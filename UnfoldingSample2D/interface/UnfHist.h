#ifndef H_UNFOLDING
#define H_UNFOLDING

#include <TString.h>

#include <vector>

#include "Core/CommonTools/interface/variables.h"

class TDirectory;
class TH1;
class TH2;
class TUnfoldBinningV17;

namespace DAS::InclusiveJet {

////////////////////////////////////////////////////////////////////////////////
/// Unfolding histograms prepared for TUnfold
struct UnfHist
{
    const TString name; //!< name corresponding to variation

    TH1 * gen,          //!< generated level (only for MC)
        * rec,          //!< reconstructed level
        * recMerged,    //!< reconstructed level with gen-level binning
        * recBG;        //!< background from fake jets (only for MC)
    TH2 * RM,    //!< response matrix (only for MC)
        * RMfine,//!< response matrix with rec binning for both axes (only for MC)
        * cov;   //!< covariance matrix

    static const std::vector<double>
        rec_pt_edges, //!< pt binning at reconstructed level
        gen_pt_edges, //!< pt binning at generated level
        rec_y_edges,  //!< eta binning at reconstructed level
        gen_y_edges;  //!< eta binning at generated level

    static const size_t
        nPtBinsGen, //!< number of bins for pt binning at reconstructed level
        nPtBinsRec, //!< number of bins for pt binning at generated level
        nYbinsGen,  //!< number of bins for eta binning at reconstructed level
        nYbinsRec;  //!< number of bins for eta binning at generated level

    static const TUnfoldBinningV17 * binsGen, //!< binning in TUnfold format at generated level
                                   * binsRec; //!< binning in TUnfold format at reconstructed level

    UnfHist (TString Name, bool MC = false);
    UnfHist (const char * prefix, TDirectory * dir);

    void ScanRecBinning ();

    void FillGen (const FourVector& jet, double weight);
    void FillRec (const FourVector& jet, double weight);
    void FillRecBG (const FourVector& jet, double weight);
    void FillCov (const FourVector& jet1, const FourVector& jet2, double weight);
    void FillRM  (const FourVector& genjet, const FourVector& recjet,
                   double weightGen, double weightRec);
    void Print ();
    void Write (TFile *);

};

}

#endif
