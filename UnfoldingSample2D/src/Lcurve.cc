#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include "InclusiveJet/UnfoldingSample2D/interface/Lcurve.h"

#include <cmath>
#include <cassert>
#include <vector>
#include <iostream>

#include <TH1.h>
#include <TFile.h>
#include <TMarker.h>

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

const int Lcurve::nsteps = 200;
const double Lcurve::mintau = 1e-4, Lcurve::maxtau = 3e-1;
const double Lcurve::tauVariation = (Lcurve::maxtau - Lcurve::mintau)/Lcurve::nsteps;

LcurveInfo::LcurveInfo () : iBest(0), tau(0.), chi2A(0.), chi2L(0.), Ndf(0) {}

LcurveInfo::LcurveInfo (TFile * file)
{
    TH1 * hInfo = dynamic_cast<TH1*>(file->Get("hInfo"));
    auto GetBinContent = [&hInfo](const char * label) {
        int i = hInfo->GetXaxis()->FindBin(label);
        return hInfo->GetBinContent(i);
    };
    iBest = GetBinContent("iBest" );
    tau   = GetBinContent("#tau"  );
    chi2A = GetBinContent("#chi2A");
    chi2L = GetBinContent("#chi2L");
    Ndf   = GetBinContent("Ndf"   );
}

void LcurveInfo::Write ()
{
    TH1 * hInfo = new TH1D("hInfo", "Unfolding results", 5, 0, 5);

    auto SetBinLabelAndContent = [&hInfo](int i, const char * label, double content) {
        hInfo->GetXaxis()->SetBinLabel(i, label);
        hInfo->SetBinContent(i, content);
    };
    SetBinLabelAndContent(1,"iBest" ,  iBest);
    SetBinLabelAndContent(2,"#tau"  ,  tau  );
    SetBinLabelAndContent(3,"#chi2A",  chi2A);
    SetBinLabelAndContent(4,"#chi2L",  chi2L);
    SetBinLabelAndContent(5,"Ndf"   ,  Ndf  );

    hInfo->GetXaxis()->CenterLabels();
    hInfo->Write();
}


Lcurve::Lcurve (Color_t color) :
    lCurve(nullptr),
    logTauX(nullptr), logTauY(nullptr)/*, * logTauCurvature(nullptr)*/,
    c(color)
{
}

Lcurve::Lcurve (TString filename, Color_t color) :
    Lcurve(color)
{
    TFile * file = TFile::Open(filename, "READ");
    lCurve  = dynamic_cast<TGraph   *>(file->Get("lCurve" ));
    logTauY = dynamic_cast<TSpline3 *>(file->Get("logTauY"));
    logTauX = dynamic_cast<TSpline3 *>(file->Get("logTauX"));
    info = LcurveInfo(file);
    // file->Close(); // TODO?

    lCurve ->SetMarkerColor(c);
    logTauY->SetMarkerColor(c);
    logTauX->SetMarkerColor(c);
    lCurve ->SetLineColor(c);
    logTauY->SetLineColor(c);
    logTauX->SetLineColor(c);

    cout << "tau=" << info.tau << endl;
}

void Lcurve::DrawLcurve ()
{
    assert(lCurve);

    for (int i = 0; i < lCurve->GetN(); ++i) {
        double x, y;
        lCurve->GetPoint(i,x,y);
        x = pow(10,x)/info.Ndf;
        y = pow(10,y)/info.Ndf;
        lCurve->SetPoint(i,x,y);
    }
    lCurve->Draw("same C");
    //lCurve->Print();

    double x,y;
    lCurve->GetPoint(info.iBest,x,y);
    cout << x << '\t' << y << endl;

    TMarker * m = new TMarker (x,y,20);
    m->SetMarkerColor(c);
    m->Draw();
}

void Lcurve::DrawTauY () // chi2L 
{
    assert(logTauY);
    
    vector<double> v_x, v_y;
    for (int i = 0; i < logTauY->GetNp(); ++i) {
        double x,y;
        logTauY->GetKnot(i,x,y);
        v_x.push_back(pow(10,x)    );
        v_y.push_back(pow(10,y)/info.Ndf);
    }
    TSpline3 * TauY = new TSpline3("TauY", v_x.data(), v_y.data(), v_x.size());
    TauY->SetLineColor(c);
    TauY->Draw("same");

    double x,y;
    TauY->GetKnot(info.iBest,x,y);
    TMarker * m = new TMarker(x,y,20);
    m->SetMarkerColor(c);
    m->Draw();
    cout << "TauY\t" << x << '\t' << y << endl;
}

void Lcurve::DrawTauX () // chi2A
{
    assert(logTauX);

    vector<double> v_x, v_y;
    for (int i = 0; i < logTauX->GetNp(); ++i) {
        double x,y;
        logTauX->GetKnot(i,x,y);
        v_x.push_back(pow(10,x));
        v_y.push_back(pow(10,y)/info.Ndf);
    }
    TSpline3 * TauX = new TSpline3("TauX", v_x.data(), v_y.data(), v_x.size());
    TauX->SetLineColor(c);
    TauX->Draw("same");

    double x,y;
    TauX->GetKnot(info.iBest,x,y);
    TMarker * tau = new TMarker(x,y,20);
    tau->SetMarkerColor(c);
    tau->Draw();
    cout << "TauX\t" << x << '\t' << y << endl;
}

#endif
