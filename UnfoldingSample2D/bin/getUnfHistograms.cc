#include <cstdlib>
#include <cassert>
#include <iostream>
#include <algorithm>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "InclusiveJet/UnfoldingSample2D/interface/UnfHist.h"

#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "variation.h"

#include "Core/JEC/bin/matching.h"

#include "Math/GenVector/GenVector_exception.h"

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// Get unfolding histograms from *n*-tuple in `UnfHist`
void getUnfHistograms 
        (TString input,  //!< name of input root file with *n*-tuples
         TString output, //!< name of output root file with histograms
         int nSplit = 1, //!< number of jobs/tasks/cores
         int nNow = 0)   //!< index of job/task/core
{
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    metainfo.Print();
    bool isMC = metainfo.isMC();

    Matching<RecJet, GenJet>::DR = metainfo.radius()/2;

    auto variations = PrepareVariations<UnfHist>(metainfo);
    //variations.at({0,0,0})->ScanRecBinning();

    TTreeReader reader(tree);
    TTreeReaderValue<Event> event = {reader, "event"};
    TTreeReaderValue<PrimaryVertex> vtx = {reader, "primaryvertex"};
    TTreeReaderValue<vector<RecJet>> recjets = {reader, "recJets"};
    TTreeReaderValue<vector<GenJet>> * genjets = nullptr;
    if (isMC)
        genjets = new TTreeReaderValue<vector<GenJet>>({reader, "genJets"}); 

    TFile *file = TFile::Open(output, "RECREATE");

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

        bool goodPV = (abs(vtx->z) < 24 /* cm */); // 0 or 1, later used as a weight
        if ((!isMC) && (!goodPV)) continue;

        double pvWgt = static_cast<double>(goodPV);

        for (auto& variation: variations) {
            VariationID  var = variation.first;
            UnfHist * unf = variation.second;

            double evWgt = event->weights[var.iEvWgt];

            // copy and apply the variation (JES + event/jet weights)
            auto recjetsV = *recjets; // V for Variation (not for Vendetta)
            for (RecJet& recjet: recjetsV) { 
                recjet.p4.Scale(recjet.JECs[var.iJEC]);
                recjet.JECs.clear(); // just to provoke an error message in case one would still try to apply the variation a second time in a later update of the code
                recjet.weights = {recjet.weights[var.iJetWgt]}; // here again, we remove all other elements to keep only one weight
            }

            // for both data and MC, get rec and cov
            for (const RecJet& recjet: recjetsV) {

                const FourVector& rec = recjet.p4;
                double w = recjet.weights.front()*pvWgt*evWgt;
                unf->FillRec(rec, w);

                for (const RecJet& recjet2: recjetsV) {

                    const FourVector& rec2 = recjet2.p4;
                    double w2 = recjet.weights[var.iJetWgt];

                    unf->FillCov(rec, rec2, w*w2);
                }
            }

            // for MC only, fill RM and gen
            if (!isMC) continue;

            Matching<RecJet, GenJet> matching(*recjets);

            // Fill matched and fake rec jets
            for (const GenJet& genjet: **genjets) {

                // gen'd jet
                const FourVector& gen = genjet.p4;
                double gw = evWgt;

                // reco'd jet
                RecJet recjet = matching.HighestPt(genjet);
                const FourVector& rec = recjet.p4;
                double rw = evWgt*pvWgt*recjet.weights.front();

                // filling
                unf->FillGen(gen,      gw    );
                unf->FillRM (gen, rec, gw, rw);
            }

            // Fill fake rec jets
            for (const RecJet& recjet: matching.mcands) {
                const FourVector& rec = recjet.p4;
                double w = evWgt*pvWgt*recjet.weights.front();
                unf->FillRecBG(rec, w);
            }
        } // loop over systematics
    } // loop over events

    for (auto& variation: variations) {
        UnfHist * unf = variation.second;
        if (unf != nullptr) unf->Write(file);
    }

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getUnfHistograms(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
