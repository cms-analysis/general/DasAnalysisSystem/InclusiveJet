#include <cassert>
#include <vector>
#include <cstdlib>
#include <cmath>

#include <TH1.h>
#include <TFile.h>
#include <TKey.h>

#include "InclusiveJet/UnfoldingSample2D/interface/UnfHist.h"

#include "common.h"

#include <TUnfold/TUnfoldBinning.h>

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// Get global systematic band from the output of the unfolding.
void getSystematics 
        (TString input,  //!< name of input root file containing the unfolded spectra
         TString output) //!< name of the output root file with 
{
    TFile * fileIn = TFile::Open(input, "READ");
    assert(fileIn);
    assert(fileIn->IsOpen());
    assert(!fileIn->IsZombie());

    TH2 * nominal = nullptr,
        * nominalbbb = nullptr;
    vector<TH2 *> variations, scans;
    TH2 * RMstat = nullptr;
    
    cout << "Looping over keys in file" << endl;
    TList* List = fileIn->GetListOfKeys() ;
    List->Print();
    TIter next(List) ;
    TKey* key = nullptr;
    while ( (key = (TKey*)next()) ) {
        TObject * obj = key->ReadObj() ;
        if (TString(obj->ClassName()) == "TGraph") continue;
        TString name = obj->GetName();

        TH2 * h = dynamic_cast<TH2*>(obj);
        assert(h);
        h->SetDirectory(0);
        if      (name.Contains("nominalbbb")) nominalbbb = dynamic_cast<TH2*>(h);
        else if (name.Contains("nominal"   )) nominal    = dynamic_cast<TH2*>(h);
        //else if (name.Contains("systau"    )) systau     = dynamic_cast<TH1*>(h);
        else if (name.Contains("SysTau"    )) continue;
        else if (name.Contains("RMstat"    )) RMstat     = dynamic_cast<TH2*>(h);
        else if (name.Contains("up") || name.Contains("down") ||
                 name.Contains("Up") || name.Contains("Down"))
            variations.push_back(dynamic_cast<TH2*>(h));
        else if (name.Contains("scan"))
            scans.push_back(dynamic_cast<TH2*>(h));
        else {
            cout << "Unexpected histogram: " << name << endl;
            exit(EXIT_FAILURE);
        }
    }
    assert(nominal);
    assert(variations.size() % 2 == 0);
    fileIn->Close();
    assert(nominal);

    cout << "Calculating data with data+MC stat uncertainties" << endl;
    TH2 * nominalExt = dynamic_cast<TH2*>(nominal->Clone("nominalExt"));
    if (RMstat != nullptr) {

        // 1) create a 1D histogram with the diagonal
        TH1 * diagonal1D = RMstat->ProjectionX("diagonal1D", 0, -1);
        diagonal1D->Reset();
        for (int i = 1; i <= diagonal1D->GetNbinsX(); ++i) {
            double content = RMstat->GetBinContent(i,i);
            diagonal1D->SetBinContent(i, sqrt(content));
        }

        // 2) use TUnfoldBinning to convert it to a 2D histogram (pt,y)
        TH2 * diagonal2D = dynamic_cast<TH2*>(UnfHist::binsGen->ExtractHistogram("diagonal2D", diagonal1D));

        // 3) add the bin content quadratically to the bin error of nominal
        for (int xbin = 1; xbin <= nominal->GetNbinsX(); ++xbin) 
        for (int ybin = 1; ybin <= nominal->GetNbinsY(); ++ybin) {
            double err1 = nominal->GetBinError(xbin,ybin),
                   err2 = diagonal2D->GetBinContent(xbin,ybin);
            double errTot = hypot(err1,err2);
            nominalExt->SetBinError(xbin, ybin, errTot);
        }
    }

    cout << "Calculating upper and lower curves" << endl;
    TH2 * upper = dynamic_cast<TH2*>(nominal->Clone("upper")),
        * lower = dynamic_cast<TH2*>(nominal->Clone("lower"));
    for (int xbin = 1; xbin <= nominal->GetNbinsX(); ++xbin) 
    for (int ybin = 1; ybin <= nominal->GetNbinsY(); ++ybin) {

        double nominal_content = nominal->GetBinContent(xbin, ybin),
               abs_tot_upper_diff = nominalExt->GetBinError(xbin, ybin),
               abs_tot_lower_diff = nominalExt->GetBinError(xbin, ybin);

        // summing up the variations quadratically
        for (auto& variation: variations) {
            double var_content = variation->GetBinContent(xbin, ybin);
            double diff = var_content - nominal_content;
            double & err = diff > 0 ? abs_tot_upper_diff : abs_tot_lower_diff;
            err = hypot(err, diff);
        }

        // keep only the very upper and lower values
        double upper_content = nominal_content + abs_tot_upper_diff,
               lower_content = nominal_content - abs_tot_lower_diff;
        upper->SetBinContent(xbin, ybin, upper_content);
        lower->SetBinContent(xbin, ybin, lower_content);
    }

    cout << "Treating scan" << endl;
    vector<TH2*> scans2D;
    for (TH2 * scan1D: scans) {
        TString n = scan1D->GetName(); n+= "2D";
        TH2 * scan2D = dynamic_cast<TH2*>(UnfHist::binsGen->ExtractHistogram(n, scan1D));
        scan2D->SetTitle(scan1D->GetTitle());
        scans2D.push_back(scan2D);
    }

    cout << "Saving into separate 1D histograms normalised to the bin width" << endl;
    TFile * fileOut = TFile::Open(output, "RECREATE");
    SaveYbins({nominal, nominalExt, nominalbbb, upper, lower});
    SaveYbins(scans2D);
    fileOut->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 3) {
        cout << argv[0] << " input output\n"
             << "\twhere\tinput = file after unfolding (contains 2D histograms)\n"
             << "\t     \toutput = file with summed variations" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    getSystematics(input, output);
    return EXIT_SUCCESS;
}
#endif
