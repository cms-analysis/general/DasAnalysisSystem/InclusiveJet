#ifndef VARIATIONS_H
#define VARIATIONS_H

#include <vector>
#include <algorithm>

#include "Core/CommonTools/interface/MetaInfo.h"

using namespace std;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// A variation is defined by variations of
///  - the event weight
///  - the jet weight
///  - the JEC factor
struct VariationID {
    int iEvWgt, iJetWgt, iJEC;
};

////////////////////////////////////////////////////////////////////////////////
/// The variations are ordered as a function of the members of `VariationID`
bool operator< (VariationID a, VariationID b)
{
    if (a.iEvWgt < b.iEvWgt) return true;
    if (a.iEvWgt > b.iEvWgt) return false;

    if (a.iJetWgt < b.iJetWgt) return true;
    if (a.iJetWgt > b.iJetWgt) return false;

    if (a.iJEC < b.iJEC) return true;
    else                 return false;
}

//typedef pair<VariationID, UnfHist *> Variation;
template <typename UH> using Variation = pair<VariationID, UH *>;

////////////////////////////////////////////////////////////////////////////////
/// Prepare the unfolding histograms from `UnfHist` for each variation.
///
/// The number of variations is deduced from the meta information.
template<typename UH> // UH for UnfHist (exact type depends on analysis)
map<VariationID, UH *> PrepareVariations 
        (MetaInfo& metainfo) //!< meta information
{
    bool isMC = metainfo.isMC();

    map<VariationID, UH *> variations;

    auto PrepareVariation = [&](VariationID v, TString name) {
        return make_pair(v, new UH(name, isMC));
    };

    // nominal
    variations.insert(PrepareVariation({0,0,0}, "nominal"));

    // event weights
    int nEvWgts = metainfo.GetNEvWgts();
    for (int i = 1; i < nEvWgts; ++i) {
        TString name = metainfo.GetEvWgt(i);
        variations.insert(PrepareVariation({i,0,0},name));
    }

    // jet weights
    int nJetWgts = metainfo.GetNJetWgts();
    for (int i = 1; i < nJetWgts; ++i) {
        TString name = metainfo.GetJetWgt(i);
        variations.insert(PrepareVariation({0,i,0},name));
    }

    // JECs
    int nJECs = metainfo.GetNJECs();
    for (int i = 1; i < nJECs; ++i) {
        TString name = metainfo.GetJEC(i);
        variations.insert(PrepareVariation({0,0,i},name));
    }

    return variations;
}

// TODO: define iterator??

}

#endif
