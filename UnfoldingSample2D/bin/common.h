#include <TH2.h>
#include <TString.h>
#include <vector>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// Transform a 2D histograms in several histograms corresponding to
/// rapidity bins and saving them.
inline void SaveYbins
        (vector<TH2*> histograms) //!< histograms corresponding to the variations
{
    for (TH2 * h2D: histograms) {

        for (int ybin = 1; ybin <= h2D->GetNbinsY(); ++ybin) {
            TString name = Form("%s_ybin%d", h2D->GetName(), ybin);
            TH1 * h = h2D->ProjectionX(name, ybin, ybin);
            h->Write();
        }
    }
}
