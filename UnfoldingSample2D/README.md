# Unfolding

 - Extract RM (with all associated uncertainties) from *n*-tuples.
 - Check purities.
 - Extract data spectrum from *n*-tuples.
 - Unfold (including all uncertainties).
 - Plot.

