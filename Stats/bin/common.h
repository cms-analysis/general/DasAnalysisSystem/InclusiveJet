#include "Core/CommonTools/interface/variables.h"
#include <vector>
#include <cassert>
#include <algorithm>

using namespace DAS;

int getbin (double pt, double absy)
{
    auto condition = [](double value) { return [value](double edge) { return value < edge; }; };
    ptrdiff_t ipt = distance(recBins.begin(), find_if(recBins.begin(), recBins.end(), condition(pt)));
    ptrdiff_t iy = distance(y_edges.begin(), find_if(y_edges.begin(), y_edges.end(), condition(absy)));
    int ibin = (iy-1)*nRecBins + ipt;
    //cout << pt << ' ' << absy << '\t' << ipt << ' ' << iy << '\t' << ibin << endl;
    return ibin;
}

std::pair<int, int> getbin (int i)
{
    assert(i > 0);
    int  ybin = (i-1) / nRecBins,
        ptbin = (i-1) % nRecBins;
    return {ybin, ptbin};
}

