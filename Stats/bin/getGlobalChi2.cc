#include <cstdlib>
#include <cassert>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Greta.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TVectorD.h>
#include <TMatrixDSym.h>
#include <TDecompBK.h>

#include "Math/VectorUtil.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "common.h"

using namespace std;
using namespace DAS;

static const double inf = numeric_limits<double>::infinity();
static const double eps = numeric_limits<double>::epsilon();

static const double ptMin = 74;
vector<double> ptMax { 3103 /*3450*/, 2787, 2238 /*2500*/, 1588 /*1784*/, 1248 /*1500*/};

////////////////////////////////////////////////////////////////////////////////
/// 2D fit function based on Chebyshev polynomials
struct Greta2D : public Greta {

    const int degree2, npars2;
    const double m2, M2;

    Greta2D (int d, int d2, double mi, double Ma, double mi2, double Ma2) :
        Greta(d,mi,Ma),
        degree2(d2), npars2(d2+1), m2(mi2), M2(Ma2)
    { }

    inline int index (int i, int j) { return this->npars*j + i; }

    double operator() (double *x, const double *p)
    {
        // normalise the variables into [-1,1]
        double nx = -1 + 2 * (log(x[0]) - log(m)) / (log(M) - log(m));
        double ny = -1 + 2 * (    x[1]  -     m2) / (    M2 -     m2);

        double result = 0;
        for (int i = 0; i <= degree ; ++i)   // runs over pt axis
        for (int j = 0; j <= degree2; ++j) { // runs over y  axis
            int I = index(i,j);
            assert(I < npars*npars2);
            result += p[I] * T(nx,i) * T(ny,j);
        }
        result = exp(result);
        return result;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// histogram with flags for bins to account for
/// as well as certain cuts that can be useful to initialise Greta
///
/// Danger: over-engineering the code....
struct Flag {

    TH1 * flag;

    int nbins; 
    double ptmin, ptmax, ymin, ymax;

    Flag (TH1 * h) : 
        flag(dynamic_cast<TH1*>(h->Clone("flag"))),
        ptmin(recBins.back()), ptmax(recBins.front()),
        ymin(y_edges.back()), ymax(y_edges.front())
    {
        cout << "Getting flag\n";
        flag->Reset();
        //cout << "      ibin   ibin2     ptbin  ptbin2      ybin   ybin2\n";
        for (int  ybin = 0;  ybin <  nYbins; ++ ybin) // start from 0 because we run over a STL vector
        for (int ptbin = 0; ptbin < nRecBins; ++ptbin) {
            int ibin = 1 + ybin*nRecBins + ptbin; // start from 1 because we run over a ROOT TH1

            double pt = recBins.at(ptbin)+1, // small shift to make sure that one is inside of the bin (not at the edge)
                   y  = y_edges.at( ybin)+0.1;

            // sanity check
            int  ibin2 = getbin(pt, y); // method used by `getCovariance`
            int ybin2, ptbin2;
            tie(ybin2, ptbin2) = getbin(ibin);

            //cout << setw(10) <<  ibin << setw(8) <<  ibin2
            //     << setw(10) << ptbin << setw(8) << ptbin2
            //     << setw(10) <<  ybin << setw(8) <<  ybin2 << '\n';

            assert( ibin ==  ibin2);
            assert(ptbin == ptbin2);
            assert( ybin ==  ybin2);

            if (pt > ptMin && pt < ptMax.at(ybin)) flag->SetBinContent(ibin, 1);
        }
        cout << flush;

        //for (int i = /*331*/ /*253*/ /*141*/ /*69*/; i <= h->GetNbinsX(); ++i) 
        //    flag->SetBinContent(i,0);

        nbins = flag->Integral();

        //////////////////////////////////

        cout << "Dumping flag\n";
        map<int,stringstream> rows;
        for (int ibin = 1; ibin <= h->GetNbinsX(); ++ibin) {
            int  ybin, ptbin;
            tie(ybin,ptbin) = getbin(ibin);
            //cout << (recBins.at(ptbin)-1) << setw(10) << (y_edges.at(ybin)+0.1) << setw(10) << getbin(recBins.at(ptbin)-1, y_edges.at(ybin)+0.1) << setw(10) << ibin << setw(10) << ybin << setw(10) << ptbin << ' ' << h->GetBinContent(ibin) << '\n';
            const char * color = flag->GetBinContent(ibin) > 0 ? "\x1B[32m" : "\x1B[31m";
            rows[ptbin] << color << setw(5) << ibin << setw(15) << h->GetBinContent(ibin);
        }
        for (int ptbin = 0; ptbin < nRecBins; ++ptbin) 
            cout << setw(8) << recBins[ptbin] << rows[ptbin].str() << "\x1B[32m\e[0m\n";
        cout << flush;
        
        //////////////////////////////////

        cout << "Getting boundaries\n";
        for (int ibin = 1; ibin <= h->GetNbinsX(); ++ibin) {
            if (flag->GetBinContent(ibin) == 0) continue;
            int  ybin, ptbin;
            tie(ybin,ptbin) = getbin(ibin);
            ptmin = min(ptmin, recBins.at(ptbin  ));
            ptmax = max(ptmax, recBins.at(ptbin+1));
            ymin  = min(ymin , y_edges.at( ybin  ));
            ymax  = max(ymax , y_edges.at( ybin+1));
        }
        cout << "ptmin = " << ptmin << '\n'
             << "ptmax = " << ptmax << '\n'
             <<  "ymin = " <<  ymin << '\n'
             <<  "ymax = " <<  ymax << endl;
    }

    bool operator() (int ibin)
    {
        static const int n = flag->GetNbinsX();
        assert(ibin > 0 && ibin <= n);
        bool pass = flag->GetBinContent(ibin) > 0;
        return pass;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// chi2 with correlations
struct Correlator {

    Flag& flag; //!< description of bins to use for fit

    vector<vector<double>> binCenters; // {pt,y}
    vector<double> binWidths;
    TVectorD v; //!< xsection value
    TMatrixDSym cov; //!< covariance matrix 

    Greta2D& greta;
    //Thunberg2D<maxdegree,maxdegree2>& greta; // TODO

    Correlator (TH1 * h, TH2 * h_cov, Flag& f,
                    Greta2D /*Thunberg2D<maxdegree,maxdegree2>*/& g) :
        flag(f),
        binCenters(flag.nbins,vector<double>(2,0.)),
        binWidths(flag.nbins,0),
        v(flag.nbins), cov(flag.nbins),
        greta(g)
    {
        cout << "Setting up v, cov and binCenters" << endl;
        for (int ibin = 1, i = 0; ibin <= h->GetNbinsX(); ++ibin) {
            if (!flag(ibin)) continue;

            int ybin, ptbin;
            tie(ybin,ptbin) = getbin(ibin);

            double ptmin = recBins.at(ptbin  );
            double ptmax = recBins.at(ptbin+1);
            double  ymin = y_edges.at( ybin  );
            double  ymax = y_edges.at( ybin+1);

            double content = h->GetBinContent(ibin);

            binCenters[i] = {(ptmin+ptmax)/2, (ymin+ymax)/2};
            binWidths[i] = ptmax - ptmin; // bin width for rapidity is 1
            v(i) = content;

            for (int ibin2 = 1, i2 = 0; ibin2 <= ibin; ++ibin2) {
                if (!flag(ibin2)) continue;
                double c = h_cov->GetBinContent(ibin, ibin2);
                cov(i,i2) = c;
                cov(i2,i) = c;
                // although the matrix is supposed to be symmetric,
                // Root is not smart enough to guess that cov(i2,i) == cov(i,i2)
                // (facepalm)
                ++i2;
                assert(i2 <= flag.nbins);
            }

            assert(cov(i,i) > 0);

            ++i;
            assert(i <= flag.nbins);
        }
        cout << flush;

        cout << "Inverting" << endl;
        double determinant = cov.Determinant();
        cout << "determinant = " << determinant << endl;
        assert(determinant > 0.);
        // The Bunch-Kaufman diagonal pivoting method decomposes a real symmetric matrix A using.  = U * D * U^T
        // TDecompBK (const TMatrixDSym &m, Double_t tol=0.0)
        TDecompBK dbk(cov);
        bool status = false;
        cov = dbk.Invert(status);
        assert(status);

        cout << "End of constructor" << endl;
    }

    double operator() (const double * p)
    {
        static TVectorD d(flag.nbins); // difference

        //// using bin center
        //for (int i = 0; i < flag.nbins; ++i) {
        //    double * x_i = binCenters[i].data();
        //    double p_i = binWidths[i] * greta(x_i, p); // count prediction in bin i 
        //    d(i) = v(i) - p_i;
        //}

        // using integral
        for (int i = 0; i < flag.nbins; ++i) {
            d(i) = v(i);
            for (double pt = /* TODO lowedge */ + 0.5 ; pt < /* TODO: up edge */; ++pt)
            //for (double  y = /* TODO lowedge */ + 0.5 ;  y < /* TODO: up edge */; ++ y)  // TODO (bin width?)
                d(i) -= greta({pt, y}, p); // bin width is one
        }

        return d * (cov * d);
    }

    TH2 * MakeHist (const char * name, const char * title, const double * p)
    {
        TH2 * h = new TH2D(name, title, nRecBins, recBins.data(), nYbins, y_edges.data());
        for (int i = 0; i < flag.nbins; ++i) {
            double * x_i = binCenters[i].data();
            double content = binWidths[i] * greta(x_i, p); // count prediction in bin i 
            int ptbin = h->GetXaxis()->FindBin(x_i[0]),
                ybin  = h->GetYaxis()->FindBin(x_i[1]);
            h->SetBinContent(ptbin, ybin, content);
        }
        return h;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Template for function
void getGlobalChi2
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              const size_t maxdegree,
              const size_t maxdegree2,
              bool autoStop = true) //!< stop automatically before maxdegree if good chi2/ndf
{
    /// Opening source and checking that it is MC
    TFile * source = TFile::Open(input, "READ");

    cout << "Getting h and h_cov" << endl;
    TH1 * h     = dynamic_cast<TH1*>(source->Get("h"  ));
    TH2 * h_cov = dynamic_cast<TH2*>(source->Get("cov"));
    assert(h     != nullptr);
    assert(h_cov != nullptr);

    h    ->SetDirectory(0);
    h_cov->SetDirectory(0);

    source->Close();

    cout << "Closing input file and opening output file" << endl;
    TFile * file = TFile::Open(output, "RECREATE");

    {
        TH2 * h2 = new TH2D("data", "data", nRecBins, recBins.data(), nYbins, y_edges.data());
        for (int i = 1; i <= h->GetNbinsX(); ++i) {
            double content = h->GetBinContent(i),
                   error   = h->GetBinError  (i);
            int ptbin, ybin;
            tie(ybin, ptbin) = getbin(i);
            h2->SetBinContent(ptbin+1, ybin+1, content);
            h2->SetBinError  (ptbin+1, ybin+1, error  );
        }
        h2->Write();
    }

    ///////////////////////////////////////

    cout << "Declaring flag and getting boundaries" << endl;
    Flag flag(h);
    flag.flag->Write();

    int ifm = 1         ; while (!flag(ifm)) ++ifm;
    int ifM = flag.nbins; while (!flag(ifM)) --ifM;

    double contentfm = h->GetBinContent(ifm),
           contentfM = h->GetBinContent(ifM);

    cout << "From bin " << ifm  << " (content = " << contentfm  << ")"
         <<  " to bin " << ifM  << " (content = " << contentfM  << ")" << endl;

    double fm = log(contentfm),
           fM = log(contentfM);

    ///////////////////////////////////////

    cout << "Declaring Greta" << endl;
    // Chebyshev polynomials in log pt and y
    Greta2D greta(maxdegree, maxdegree2, // orders
                  flag.ptmin, flag.ptmax,
                  flag.ymin , flag.ymax);
    //Thunberg2D<maxdegree,maxdegree2> greta(flag.ptmin, flag.ptmax, flag.ymin , flag.ymax); // TODO
    int npars = greta.npars * greta.npars2;
    cout << "npars = " << npars << endl;;

    ///////////////////////////////////////

    cout << "Declaring correlator" << endl;
    // transforms 1D and 2D histograms into a vector and a matrix and uses Greta to return chi2
    Correlator correlator(h, h_cov, flag, greta);
    ROOT::Math::Functor functor(correlator, npars);

    ///////////////////////////////////////

    cout << "Setting up minimiser" << endl;
    // https://root.cern.ch/doc/v608/classROOT_1_1Math_1_1Minimizer.html
    ROOT::Math::Minimizer* minimiser = ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");
    minimiser->SetMaxFunctionCalls(1e6);
    minimiser->SetFunction(functor);
    minimiser->SetTolerance(0.001);
    minimiser->SetPrintLevel(0);
    double step = 0.001;
    TH3 * parameters = new TH3D("parameters", "parameters", greta.npars , 0.5, greta.npars +0.5,
                                                            greta.npars2, 0.5, greta.npars2+0.5,
                                                            npars       , 0.5, npars       +0.5);
    TH1 * chi2ndfs = new TH1D("chi2ndfs", "#chi^{2}/NDF", npars, 0.5, npars+0.5);
    for (int j = 0; j < greta.npars2; ++j) 
    for (int i = 0; i < greta.npars ; ++i) { 
        int I = greta.index(i,j);
        const char * name = Form("p%d%d",i,j);
        // SetVariable (unsigned int ivar, const std::string &name, double val, double step)=0
        if (i == 0 && j == 0) {
            double value = (fM+fm)/2;
            minimiser->SetVariable(I,name,value,step);
            parameters->SetBinContent(i+1,j+1,1,value);
        }
        else if (i == 1 && j == 0) {
            double value = (fM-fm)/2;
            minimiser->SetVariable(I,name,value,step);
            minimiser->FixVariable(I);
            parameters->SetBinContent(i+1,j+1,1,value);
        }
        else {
            minimiser->SetVariable(I,name,0.,step);
            minimiser->FixVariable(I);
            //minimiser->SetVariableLimits(i,0.01,6); // TODO?
        }
    }

    ///////////////////////////////////////

    cout << "Global indices:\n";
    for (int i = 0; i < parameters->GetNbinsX(); ++i) {
        for (int j = 0; j < parameters->GetNbinsY(); ++j) 
            cout << setw(15) << greta.index(i,j);
        cout << '\n';
    }
    cout << flush;

    ///////////////////////////////////////

    // print and stores the values of the parameters
    auto dump = [minimiser,parameters,chi2ndfs,&greta](int npars) {
        const double * X = minimiser->X();
        for (int i = 0; i < parameters->GetNbinsX(); ++i) {
            for (int j = 0; j < parameters->GetNbinsY(); ++j) {
                const int I = greta.index(i,j);
                const double var = X[I];
                cout << setw(15) << var;
                parameters->SetBinContent(i+1, j+1, npars, var);
            }
            cout << '\n';
        }
    };

    ///////////////////////////////////////

    // make the fit and returns chi2 if successful and infinity otherwise
    auto fit = [minimiser,&correlator]() -> double {

        bool state = minimiser->Minimize();

        int status = minimiser->Status();
        if (status > 0) switch (status) {
            case 1:  cerr << "Warning: Covariance was made positive defined\n"; break;
            case 2:  cerr << "Warning: Hesse is invalid\n"; break;
            case 3:  cerr << "Warning: Expected Distance reached from the Minimum is above max (EDM = " << minimiser->Edm() << ")\n"; break;
            case 4:  cerr << "Warning: Reached call limit\n"; break;
            case 5:
            default: cerr << "Warning: Any other failure\n";
        }

        const double * X = minimiser->X();
        double chi2 = correlator(X);

        return state ? chi2 : inf;
    };

    ///////////////////////////////////////

    cout << "\e[1mInitial parameters\e[0m\n";
    dump(1);

    cout << "\e[1mRunning\e[0m" << endl;
    for (int n = 1; n <= npars; ++n) {

        cout << "\e[1mn = " << n << "\e[0m\n";

        static double last = inf;

        minimiser->ReleaseVariable(n);
        int ndf = flag.nbins - n - 1;
        double chi2 = fit();

        dump(n);
        double x2n = chi2/ndf,
               error = sqrt(2./ndf);
        if (x2n < last) {
            cout << "\x1B[32m";
            last = x2n;
        }
        else cout << "\x1B[31m";
        cout << "\e[1mChi2 & Ndf:" << setw(10) << chi2 << setw(5) << ndf << setw(10) << x2n << "\u00B1" << error << "\x1B[37m\e[0m" << endl;

        const char * name = Form("h%d", n),
                   * title = Form("#chi^{2}/ndf = %.2f #pm %.2f", x2n, error);
        TH2 * smooth = correlator.MakeHist(name, title, minimiser->X());
        smooth->Write();

        if (autoStop && abs(x2n-1) < error) {
            cout << "Chi2/Ndf ~ 1\nStopping now" << endl;
            break;
        }
    }

    //auto reset = [minimiser,parameters,&globalI,&greta](int n /* get the parameters from nth iteration  */) {
    //    for (int j = 0; j < greta.npars2; ++j) 
    //    for (int i = 0; i < greta.npars ; ++i) { 
    //        int index = globalI(i,j);
    //        double value = parameters->GetBinContent(i+1, j+1, n);
    //        //cout << index << ' ' << value << '\n';
    //        // virtual bool     SetVariableValue (unsigned int ivar, double value)
    //        minimiser->SetVariableValue(index,value);
    //    }
    //    //cout << endl;
    //};
    //cout << "Trying sophisticated appraoch for all other parameters" << endl;
    //for (int n = k; n <= npars; ++n) {
    //    int ndf = nbins-n-1;

    //    static double lastchi2ndf = inf;

    //    cout << "\e[1m\x1B[31mn = " << n << "\x1B[37m\e[0m" << endl;

    //    dump(npars);
    //    chi2ndfs->SetBinContent(n,lastchi2ndf);

    //    vector<pair<int,double>> attempts; // int = global index, double = chi2/ndf

    //    size_t Nattempts = min(3ul,maxdegree2); // in general 3, unless one parameter reached a border
    //    // TODO: better cover following situation:
    //    //       x  x  x  o  o
    //    //       x  o  o  o  o
    //    //       o  o  o  o  o
    //    cout << "Attempts:";
    //    for (int j = 0; j < greta.npars && attempts.size() < Nattempts; ++j) {
    //        if (!minimiser->IsFixedVariable(globalI(greta.npars-1,j))) continue;
    //        bool full = true;
    //        // we look for the first empty bin
    //        for (int i = 0; i < greta.npars; ++i) {
    //            int index = globalI(i,j);
    //            if (!minimiser->IsFixedVariable(index)) continue;
    //            cout << "\tp" << i << j << " (" << index << ')';
    //            attempts.push_back({index,inf});
    //            full = false;
    //            break;
    //        }
    //        if (full) --Nattempts;
    //    }
    //    cout << endl;

    //    if (Nattempts == 0) break;

    //    for (auto& attempt: attempts) {
    //        int x = attempt.first;
    //        cout << "Releasing parameter " << x << '\n';
    //        assert(minimiser->IsFixedVariable(x));
    //        minimiser->ReleaseVariable(x);
    //        attempt.second = fit()/ndf;
    //        cout << "chi2 / ndf " << attempt.second << endl;
    //        minimiser->FixVariable(x);
    //        reset(n-1);
    //    }

    //    auto best = find_if(attempts.begin(), attempts.end(), [](pair<int,double> attempt) { return attempt.second < lastchi2ndf; } );

    //    if (best != attempts.end()) 
    //        cout << "\x1B[32mBest choice is parameter " << best->first << " with chi2/ndf = " << best->second << "\x1B[37m\e[0m" << endl;
    //    else {
    //        //best = find_if(attempts.begin(), attempts.end(), [](pair<int,double> attempt) { return attempt.second < inf; } );
    //        //if (best != attempts.end()) 
    //        best = attempts.begin();
    //            cout << "\x1B[31mContinuing with parameter " << best->first << " with chi2/ndf = " << best->second << "\x1B[37m\e[0m" << endl;
    //        //else {
    //        //    cout << "None of the attempts is doing better than the former iteration. Stopping now.\n" << endl;
    //        //    cout << "\e[1m\x1B[32mFinal chi2/ndf = " << lastchi2ndf << " with " << (n+2) << " parameters.\x1B[37m\e[0m\n";
    //        //    break;
    //        //}
    //    }

    //    // resetting fit
    //    lastchi2ndf = best->second;
    //    minimiser->ReleaseVariable(best->first);
    //    fit();
    //}

    cout << "Closing file." << endl;
    parameters->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 5) {
        cout << argv[0] << " input output maxdegree maxdegree2" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    int maxdegree = atoi(argv[3]),
        maxdegree2 = atoi(argv[4]);

    getGlobalChi2(input, output, maxdegree, maxdegree2);
    return EXIT_SUCCESS;
}
#endif

