#include <cstdlib>

#include <vector>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>
#include <numeric>

#include <experimental/filesystem>

#include <TString.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TKey.h>

#include "InclusiveJet/UnfoldingSampleND/bin/PtY.h"

using namespace std;
using namespace experimental::filesystem;
using namespace DAS;
using namespace DAS::InclusiveJet;

const char * rn() { return Form("%d", rand()); }

static const auto feps = numeric_limits<float>::epsilon();

static const path DAS_WORKAREA(getenv("DAS_WORKAREA"));

inline double percentage (double numerator, double denominator) { return isnormal(denominator) ? 100*numerator/denominator : 0.; }

namespace QCDfits {

struct MuVar {
    const char * infix;
    const float MuF, MuR;
};

////////////////////////////////////////////////////////////////////////////////
/// function decorrelate
///
/// Take a "2D" input histogram (in `TUnfoldBinning` binning) and returns
/// five "1D" output histograms (still in `TUnfoldBinning` binning).
vector<TH1 *> decorrelateY (TH1 * h2, auto name)
{
    vector<TH1 *> hs;
    //h2->Print("all");
    for (int iy = 0; iy < nYbins; ++iy) {
        auto h = dynamic_cast<TH1*>(h2->Clone(name(iy)));
        h->SetDirectory(0);
        int jmin = genBinning->GetGlobalBinNumber(minpt+1, iy*0.5),
            jmax = genBinning->GetGlobalBinNumber(maxpt-1, iy*0.5);
        for (int j = 1; j <= h->GetNbinsX(); ++j) {
            if (j >= jmin && j <= jmax) continue;
            h->SetBinContent(j,0);
            h->SetBinError  (j,0);
        }
        //h->Print("all");
        hs.push_back(h);
    }
    //exit(0);
    return hs;
}

////////////////////////////////////////////////////////////////////////////////
/// struct Data
///
/// All information from the data (but nothing from any theory).
/// Unless mentioned differently, all histograms are in the TUnfoldBinning format,
/// with bin IDs instead of physical binning with `pt` and `y`.
struct Data {

    const int R,    //!< radius (e.g. 4 or 7)
              year; //!< 201?
    const pair<float,float> lumi; //!< luminosity in /fb & relative uncertainty in %

    TH1 * h; //!< nominal value
    TH2 * h2, //!< nominal value with physical binning (essentially useful to get the bin edges)
        * Corr, //!< total correlations (including MC)
        * Cov;  //!< total covariance matrix (TODO: remove?)
    vector<TH1*> systematics; //!< systematic uncertainties in data in %

    double pt, y;

private:
    static pair<float, float> getLumi
        (int R,   //!< radius (e.g. 4 or 7)
         int year //!< 201?
         )
    {
        path p = DAS_WORKAREA / "tables/luminosities";
        p /= to_string(year);
        p /= Form("ak%d", R);
        switch (year) {
            case 2016: p /= "BCDEFGH.lumi"; break;
            case 2017: p /= "BCDEF.lumi"; break;
            case 2018: p /= "ABCD.lumi"; break;
            default: cerr << "Lumi files only available for 2016, 2017 & 2018 at the moment\n"; exit(EXIT_FAILURE);
        }
        cout << p << endl;
        assert(exists(p));

        ifstream file(p.c_str());
        float trigger, lumi;
        while (file >> trigger >> lumi); // we only want the lumi of the last / unprescaled trigger
        lumi /= 1000;
        file.close();

        float relUnc;
        switch (year) {
            case 2016: relUnc = 1.2; break;
            default: cerr << "Lumi relative uncertainty only available for 2016 at the moment\n"; exit(EXIT_FAILURE);
        }

        float absUnc = lumi*relUnc/100;
        cout << "L = " << lumi << "\u00B1" << absUnc << " (" << relUnc << "%)" << endl;
        return {lumi,relUnc};
    }

public:
    ////////////////////////////////////////////////////////////////////////////////
    /// constructor
    ///
    /// Retrieving *all* information from the output root file of `unfold`.
    Data 
        (path input,       //!< for nominal value and uncertainties
         path alternative, //!< alternative unfolding for model uncertainties
         path smooth,      //!< smooth systematic uncertainties
         int radius,       //!< radius (e.g. 4 or 7)
         int Year) :       //!< 201?
        R(radius), year(Year), lumi(getLumi(R, year)),
        h(nullptr), h2(nullptr), Corr(nullptr), Cov(nullptr),
        pt(-1), y(-1)
    {
        cout << __func__ << endl;

        cout << "Input data file: " << input << endl;
        auto f = TFile::Open(input.c_str(), "READ");

        h    = dynamic_cast<TH1*>(f->Get("nominal"));
        Corr = dynamic_cast<TH2*>(f->Get("corrTotal"));
        Cov  = dynamic_cast<TH2*>(f->Get("covTotal"));
        cout << "Extracted histograms:\t" << h << '\t' << Cov << '\t' << Corr << endl;
        h->SetDirectory(0);
        Corr->SetDirectory(0);
        Cov->SetDirectory(0);
        h2 = dynamic_cast<TH2*>(genBinning->ExtractHistogram("h2", h));
        h2->SetDirectory(0);

        if (exists(smooth)) {
            f->Close();
            f = TFile::Open(smooth.c_str(), "READ");
        }

        cout << "Extracting variations\n";
        for (TString dirname: {"MC", "JES"}) {
            auto dir = f->GetDirectory(dirname);
            cout << dirname << ' ' << dir << endl;
            dir->cd();
            TIter Next(dir->GetListOfKeys()) ;
            TKey* key = nullptr;
            while ( (key = dynamic_cast<TKey*>(Next())) ) {
                TString name = key->GetName();
                cout << " - " << name << endl;
                if (name.Contains("up")) continue;
                TH1 * s = dynamic_cast<TH1 *>(key->ReadObj());
                assert(s != nullptr);
                s->SetDirectory(0);
                if (exists(smooth) || (!name.Contains("miss") && !name.Contains("fake"))) s->Add(h,-1);

                name.ReplaceAll("down", "");
                //if (name.Contains("Prefdown")) s->SetName("uncor"); // v2

                systematics.push_back(s);
            }
        }
        cout << flush;

        f->Close();

        if (!exists(alternative)) {
            cout << "**NOT** extracting model uncertainty" << endl;
            return;
        }
        
        cout << "Alternative input data file: " << input << endl;
        f = TFile::Open(alternative.c_str(), "READ");

        cout << "Extracting model uncertainty" << endl;
        auto model = dynamic_cast<TH1 *>(f->Get("nominal")); 
        model->SetName("model");
        model->SetDirectory(0);
        model->Add(h, -1);
        f->Close();
        systematics.push_back(model);
        //auto modelsup = decorrelateY(h, [](int iy) { return Form("modelY%dup", iy); });
        //auto modeldn = dynamic_cast<TH1 *>(modelup->Clone("modeldown")); 
        //modeldn->SetDirectory(0);
        //modeldn->Scale(-1);
        //systematics.push_back(modeldn);
        //auto modelsdn = decorrelateY(h, [](int iy) { return Form("modelY%ddown", iy); });
        //for (int iy = 0; iy < nYbins; ++iy) {
        //    systematics.push_back(modelsup.at(iy));
        //    systematics.push_back(modelsdn.at(iy));
        //}
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// forbid copy constructor
    Data (const Data&) = delete;

    ////////////////////////////////////////////////////////////////////////////////
    /// operator()
    ///
    /// Setting pt and y, useful to be used complentary with operator<<
    const Data& operator() (double Pt, double Y)
    {
        pt = Pt; y = Y;
        return *this;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// destructor
    ///
    /// Just releasing some memory.
    ~Data ()
    {
        delete h;
        delete h2;
        delete Corr;
        delete Cov;
        for (auto s: systematics) delete s;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// operator <<
///
/// Overloading to read the Data structure.
ofstream& operator<< (ofstream& row, const Data& CMS)
{
    assert(CMS.pt > 0 && CMS.y > 0);
    auto ibin = genBinning->GetGlobalBinNumber(CMS.pt, CMS.y);
    TAxis * X = CMS.h2->GetXaxis(),
          * Y = CMS.h2->GetYaxis();
    int ptbin = X->FindBin(CMS.pt),
         ybin = Y->FindBin( CMS.y);
    auto pTlow  = X->GetBinLowEdge(ptbin),
         pThigh = X->GetBinLowEdge(ptbin+1),
          ylow  = Y->GetBinLowEdge( ybin  ),
          yhigh = Y->GetBinLowEdge( ybin+1);
    auto content = CMS.h->GetBinContent(ibin);
    auto Sigma = content / (pThigh - pTlow), // normalise to bin width
         stat  = percentage(CMS.h->GetBinError(ibin),content);
    bool binFlag = CMS.pt >= minpt && CMS.y < 2.0 && Sigma > feps && stat > feps && stat < 33;
    row << defaultfloat << setw(20) << binFlag
                        << setw(20) << ylow
                        << setw(20) << yhigh
                        << setw(20) << pTlow
                        << setw(20) << pThigh
                        << setw(20) << (isnormal(Sigma) ? Sigma : 0)
        << fixed        << setw(20) << (isnormal(stat ) ? stat  : 0)
                        << setw(20) << CMS.lumi.second;
    for (auto& s: CMS.systematics) {
        TString name = s->GetName();
        auto v = s->GetBinContent(ibin);
        if (name.Contains("uncor")) v = abs(v);
        row << setw(20) << percentage(v, content);
    }
    return row;
}

////////////////////////////////////////////////////////////////////////////////
/// struct Term
/// 
/// Intermediate structure to cary some information about the theory information
/// in the xFitter tables, such as location, scales, etc.
struct Term {
    TString Name, Type, Source, Operator, Info;
    TString GetName   () const { return Name  ; }
    TString GetType   () const { return Type  ; }
    TString GetSource () const { return Source; }
    TString GetInfo   () const { return Info  ; }
    TString Operate   () const { return Operator + Name; }
    Term (const char * name, const char * type, const char * source, path file, const char * Op,
            MuVar& muvar, bool Units = false) :
        Name(name), Type(type), Source(source), Operator(Op),
        Info(Form("Filename=%s:%sScaleFacMuR=%.1f:ScaleFacMuF=%.1f",
                    absolute(file).c_str(), Units ? "Units=publication:" : "", muvar.MuR, muvar.MuF))
    { assert(exists(file)); }
    Term (const char * name, const char * type, const char * source, path file) :
        Name(name), Type(type), Source(source), Operator("*"),
        Info(Form("FileName=%s:FileColumn=5", absolute(file).c_str()))
    { assert(exists(file)); }
};

////////////////////////////////////////////////////////////////////////////////
/// struct Table
///
/// One instance corresponds to one output file / one rapidity bin.
/// It can be filled use operator overlaoding.
///
/// Note: `stat` and `uncor` are magic words, in the sense the xFitter
/// understands that they are not correlated among any bins, in contrast
/// to all other uncertainties, considered as correlated among all bins.
struct Table {

    static vector<TString> decorr; //!< static member describing which entries should be decorrelated among rapidity bins

    ofstream f;

    Table //!< constructor
        (path pfile, //!< destination file
         int iy, //!< rapidity bin (from 0 to 4)
         vector<Term> terms, //!< information about theory tables 
         const Data& CMS,
         int NDATA) :
        f(pfile.c_str())
    {
        cout << pfile << endl;

        // define all columns
        vector<pair<TString, TString>> columns {
            {"Flag", "binFlag"},
            {"Bin", "ylow"},
            {"Bin", "yhigh"},
            {"Bin", "pTlow"},
            {"Bin", "pThigh"},
            {"Sigma", "Sigma"},
            {"Error", "stat"},
            {"Error", "lumi"},
        };

        for (TH1 * h: CMS.systematics) { // loop over JES & MC directories
            TString title = h->GetName();
            //bool decorrelate = find_if(Table::decorr.begin(), Table::decorr.end(), [title](TString toDecorr) { return title.Contains(toDecorr); }) != Table::decorr.end();
            //TString Y = decorrelate ? Form("Y%d", iy) : "";
            //int l = title.Length();
            //     if (title(l-4, l) == "down") { title = title(0,l-4); title += '-'; }
            //else if (title(l-2, l) == "up"  ) { title = title(0,l-2); title += '+'; } 
            title.ReplaceAll("down", "");
            columns.push_back({"Error", title}); 
        }

        columns.push_back( {"Error", "stat"} ); // stand for the statistical uncertainties in the theory
        columns.push_back( {"Error", "NP"}); // from (Py+hg)/2 +/- (Py-Hg)/2
        //columns.push_back( {"Error", "NPerr1+"}); // from (Py+hg)/2 +/- (Py-Hg)/2
        //columns.push_back( {"Error", "NPerr1-"}); // 
        //columns.push_back( {"Error", "NPerr2+"}); // from (Py+Hg)/2 - Sherpa difference
        //columns.push_back( {"Error", "NPerr2-"}); // 

        // concatenate
        auto columnTypes = accumulate(next(columns.begin()), columns.end(), "'" + columns.front().first , [](TString s, pair<TString,TString> p) { return s + "', '" + p.first ; }) + "'",
             columnNames = accumulate(next(columns.begin()), columns.end(), "'" + columns.front().second, [](TString s, pair<TString,TString> p) { return s + "', '" + p.second; }) + "'";

        using TermFunc = TString (Term::*)() const;
        auto concatenate = [](TermFunc Get, TString sep = "', '") {
            return [&](TString& s, Term& t) {
                return s + sep + (t.*Get)();
            };
        };
        auto TermName   = accumulate(next(terms.begin()), terms.end(), "'" + terms.front().GetName  (), concatenate(&Term::GetName  )) + "'";
        auto TermType   = accumulate(next(terms.begin()), terms.end(), "'" + terms.front().GetType  (), concatenate(&Term::GetType  )) + "'";
        auto TermSource = accumulate(next(terms.begin()), terms.end(), "'" + terms.front().GetSource(), concatenate(&Term::GetSource)) + "'";
        auto TermInfo   = accumulate(next(terms.begin()), terms.end(), "'" + terms.front().GetInfo  (), concatenate(&Term::GetInfo  , "',\n\t\t\t'")) + "'";
        auto TheorExpr  = accumulate(terms.begin(), terms.end(), TString(""), concatenate(&Term::Operate, ""));

        assert(NDATA > 0);

        const int Percent = count_if(columns.begin(), columns.end(), [](pair<TString,TString>& s){ return s.first == "Error"; });

        f << "! CMS 13 TeV jets, " << Form("%.1f", CMS.lumi.first) << " /fb\n!\n!\n"
          << "&Data\n";
        if (iy >= 0 && iy < nYbins)
            f << "  Name = 'CMS inclusive jets 13 TeV, " << yBins.at(iy) << "'\n";
        else
            f << "  Name = 'CMS inclusive jets 13 TeV'\n";
        f << "  Reaction = 'pp jets fastNLO'\n"
          << "\n"
          << "  NDATA = " << NDATA << '\n'
          << "  NColumn = " << columns.size() << '\n'
          << "  ColumnType = " << columnTypes << '\n'
          << "  ColumnName = " << columnNames << '\n'
          << "\n"
          << "  NInfo = 4\n"
          << "  DataInfo = 13000., 1., -1., -1.\n" 
          << "  CInfo = 'sqrt(S)', 'PublicationUnits', 'MurDef', 'MufDef'\n" 
          << "\n"
          << "  IndexDataset = " << (301+iy) << '\n'
          << "  TheoryType = 'expression'\n" 
          << "  TermName = "   << TermName   << '\n'
          << "  TermType = "   << TermType   << '\n'
          << "  TermSource = " << TermSource << '\n'
          << "  TermInfo = "   << TermInfo   << '\n'
          << "  TheorExpr = '"  << TheorExpr  << "'\n"
          << "  Percent = " << Percent << "*True\n"
          << "&End\n"
          << "&PlotDesc\n"
          << "   PlotN = 5\n"
          << "   PlotDefColumn = 'ylow'\n"
          << "   PlotDefValue = -0.5, 0, 0.5, 1.0, 1.5\n" //, 2.0\n"
          << "   PlotVarColumn = 'pTlow'\n";
        for (int ybin = 1; ybin <= nYbins-1; ++ybin) 
            f << "   PlotOptions(" << ybin << ") = 'Experiment:CMS@Title: 13 TeV ak" << CMS.R << " " << yBins.at(ybin-1)
              << " @XTitle:p_{T} (GeV)@YTitle:#sigma_{jet}@Xmin:" << minpt << "@Xmax:" << maxpt << ".@Xlog@Ylog@YminR:0.9@YmaxR:1.1'\n";
        f << "&End\n"
          << "*";
        for (auto& c: columns) f << setw(20) << c.second;
        f << '\n';
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// forbid copy constructor
    Table (const Table&) = delete;

    ////////////////////////////////////////////////////////////////////////////////
    /// destructor
    ~Table ()
    {
        f.close();
    }
};
vector<TString> Table::decorr;

////////////////////////////////////////////////////////////////////////////////
/// struct Correlations
///
/// One instance corresponds to one output file / one couple of rapidity bins.
struct Correlations {

    ofstream f; //!< output text file (header + tabular format)

    ////////////////////////////////////////////////////////////////////////////////
    /// constructor
    ///
    /// Just prints the header to the output file for a given rapidity bin
    Correlations
            (path pfile, //!< output txt file
             int iy1, //!< index of rapidity bin (0-4)
             int iy2, //!< id.
             int NDATA1, //!< number of pt bins in theory predictions in bin iy1
             int NDATA2  //!< id. for iy2
             ) :
        f(pfile.c_str())
    {
        TString MatrixType = "Statistical correlations";
        f << "!\n"
          << "! " << MatrixType << '\n'
          << "!\n"
          << "&StatCorr\n";
        if (iy1 >= 0 && iy2 >= 0 && iy1 < nYbins && iy2 < nYbins)
            f << "  Name1 = 'CMS inclusive jets 13 TeV, " << yBins[iy1] << "'\n"
              << "  Name2 = 'CMS inclusive jets 13 TeV, " << yBins[iy2] << "'\n";
        else 
            f << "  Name1 = 'CMS inclusive jets 13 TeV'\n"
              << "  Name1 = 'CMS inclusive jets 13 TeV'\n";
        f << "  \n"
          << "  NIdColumns1 = 2\n"
          << "  NIdColumns2 = 2\n"
          << "  \n"
          << "  IdColumns1 = 'ylow', 'pTlow'\n"
          << "  IdColumns2 = 'ylow', 'pTlow'\n"
          << "  \n"
          << "  NCorr = " << (NDATA1 * NDATA2) <<  '\n'
          << "  \n"
          << "  MatrixType = '" << MatrixType << "'\n"
          << "&End" << endl;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// forbid copy constructor
    Correlations (const Correlations&) = delete;

    ////////////////////////////////////////////////////////////////////////////////
    /// destructor
    ~Correlations ()
    {
        f.close();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// function Write
///
/// Create 5-column text file with `ylow yup ptlow ptup content`
void Write
    (path output, //!< output txt file
     TH1 * h,     //!< 1D histogram to write with `genBins` on x-axis
     TH1 * flag,  //!< 1D histogram to know when `h` should be printed to the txt file
     int iy)      //!< bin index (starting from 0)
{
    ofstream file(output.c_str());

    auto ylow = y_edges.at(iy),
         yup  = y_edges.at(iy+1);

    for (int i = 1; i <= h->GetNbinsX(); ++i) {
        auto pt = h->GetBinCenter(i);
        auto f = flag->GetBinError(flag->FindBin(pt));
        if (abs(f) < feps) continue;
        auto ptlow = h->GetBinLowEdge(i),
             ptup  = h->GetBinLowEdge(i+1);
        file << setw( 5) <<  ylow << setw( 5) <<  yup
             << setw(10) << ptlow << setw(10) << ptup
             << setw(15) << h->GetBinContent(i) << '\n';
    }

    file.close();
}

////////////////////////////////////////////////////////////////////////////////
/// function GetThRelStat
///
/// Get relative statistical uncertainties from theory.
/// These are not directly stored in the FastNLO tables, but in the `.dat` file
/// that are available from Klaus' server.
///
/// The first line is a header; then each line corresponds to a bin.
/// The three first columns correspond to the pt bin (low, center, high),
/// the forth and fifth columns correspond to some theory cross section and its
/// statistical uncertainty.
/// All subsequent columns are here not relevant.
TH1 * GetThRelStat (TH1 * tmpl, int R, int year, const char * order)
{
    cout << "Getting some theory curve to get uncertainties" << endl;
    auto h = dynamic_cast<TH1*>(tmpl->Clone(order));
    h->Reset();

    bool increase = TString(order).Contains("NNLO");

    for (int iy = 0; iy < nYbins; ++iy) {
        double y = iy*0.5+0.25;
    
        path fTh = DAS_WORKAREA / Form("tables/FastNLO/%d/ak%d", year, R);
        fTh /= Form("1jet.%s.fnl%dh_y%d_ptjet.dat", order, R == 7 ? 5332 : 5362, iy);
        cout << fTh << endl;
        assert(exists(fTh));
    
        fstream infile(fTh.c_str());
    
        TString garbage;
        TString nextEdge = "9.70000000000E+01"; // the high edge of a bin is the low edge of the next bin
        do {
            // first, skip irrelevant information
            do infile >> garbage;
            while (garbage != nextEdge && infile.good());
    
            if (!infile.good()) break;
    
            double pt, sigma, unc;
            infile >> pt >> nextEdge >> sigma >> unc;
            double relStat = isnormal(sigma) ? unc/sigma : 0;
            if (increase) relStat *= 2;
    
            int ibin = genBinning->GetGlobalBinNumber(pt, y); 
            h->SetBinError(ibin, relStat);
        } 
        while (infile.good());
    
        infile.close();
    }

    return h;
}
} // end of namespace

using namespace QCDfits;

////////////////////////////////////////////////////////////////////////////////
/// function getxFitterTables
///
/// Produce xFitter tables for 2D inclusive jet measurements, including
/// NP, EW, NLL kFactor (if applicable), stat unc from theory calculation,
/// and all experimental uncertainties.
///
/// All tables are expected to exist in the workarea; else the program will abort.
///
/// The structure of the code is the following:
///  - in the main function, we load the data and the theory components separately;
///  - the we loop over the rapidity bins and we combine the contributions in tables.
void getxFitterTables
    (path input, //!< input root file
     path output, //!< output directory
     path alternative, //!< alternative input root file
     path smooth, //!< smooth systematic uncertainties
     int R = 7, //!< jet size
     int year = 2016) //!< 4-digit year
{
    InitBinning();

    //Table::decorr = {"Pref"}; // v1.1
    //Table::decorr = {"Pref", "PileUpPtEC1"}; // v1.2
    //Table::decorr = {"Pref", "PileUpPtEC1", "PileUpPtRef"}; // v1.3
    //Table::decorr = {"Pref", "PileUpPtEC1", "PileUpPtRef", "RelativePtBB"}; // v1.4

    assert(exists(input));
    if (exists(output)) {
        assert(is_directory(output));
        cerr << "Overwriting " << output << '\n';
    }
    create_directories(output);

    static vector<MuVar> muvars {
        { "_muf05mur05", 0.5          , 0.5               }, 
        { "_muf05mur1" , 0.5          ,        1.0        },
        { "_muf1mur05" ,      1.0     , 0.5               }, 
        { ""           ,      1.0     ,        1.0        },
        { "_muf1mur2"  ,      1.0     ,               2.0 },
        { "_muf2mur1"  ,           2.0,        1.0        },
        { "_muf2mur2"  ,           2.0,               2.0 }
    };

    // we declare a custom class as a proxy to the data points (including uncertainties)
    Data CMS(input, alternative, smooth, R, year);

    // we retrieve the theory uncertainties:
    // xFitter expects exactly the same bins in the theory *and* in the tables
    // (a flag in the table will indicate if the bin is populated in the data)
    TH1 * NLOstat = GetThRelStat(CMS.h, R, year, "NLO");
    TH2 * NLOstat2D = dynamic_cast<TH2*>(genBinning->ExtractHistogram("NLOstat2D", NLOstat));

    cout << "Getting number of points per rapidity bin" << endl;
    vector<int> NDATA(nYbins,0);
    for (int ptbin = 1; ptbin <= NLOstat2D->GetNbinsX(); ++ptbin)
    for (int  ybin = 1;  ybin <= NLOstat2D->GetNbinsY(); ++ ybin)
        if (isnormal(NLOstat2D->GetBinError(ptbin,ybin))) ++NDATA.at(ybin-1);
    auto globNDATA = accumulate(NDATA.begin(), NDATA.end(), 0);

    TH1 * NNLOstat = GetThRelStat(CMS.h, R, year, "NNLO");
    TH2 * NNLOstat2D = dynamic_cast<TH2*>(genBinning->ExtractHistogram("NNLOstat2D", NNLOstat));

    path pNP = DAS_WORKAREA / Form("tables/NPcorrections/ak%d/NP.root", R);
    assert(exists(pNP));
    auto fNP = TFile::Open(pNP.c_str(), "READ");

    path pEW = DAS_WORKAREA / Form("tables/xFitter/ak%d/EW.root", R);
    assert(exists(pEW));
    auto fEW = TFile::Open(pEW.c_str(), "READ");

    path gpF = DAS_WORKAREA / Form("tables/xFitter/ak%d/InclusiveNJets_fnl%dh_v23_fix.tab", R, R == 7 ? 5332 : 5362);

    cout << "Creating output files" << endl;
    //vector<Table> globTablesNLONLL, globTableNNLO; // indexed as `muvars`
    //for (MuVar& muvar: muvars) {
    //    Table globTableNLONLL(output / Form("Run2016_NLO+NLL%s.dat", muvar.infix), -1, { F, NLL, NP, EW }, CMS, globNDATA),
    //          globTableNNLO  (output / Form("Run2016_NNLO%s.dat"   , muvar.infix), -1, { F,      NP, EW }, CMS, globNDATA);
    //    globTablesNLONLL.push_back( globTableNLONLL );
    //    globTablesNNLO  .push_back( globTableNNLO   );
    //}
    path gp = output / "Run2016.corr";
    Correlations globCorrelations(gp, -1, -1, globNDATA, globNDATA);
    for (int iy = 0; iy < nYbins-1; ++iy) {
        cout << yBins.at(iy) << endl;

        TH1 *  NLOstat1D =  NLOstat2D->ProjectionX( "NLOstat1D", iy+1, iy+1);
        TH1 * NNLOstat1D = NNLOstat2D->ProjectionX("NNLOstat1D", iy+1, iy+1);

        auto GetNP = [fNP,iy](const char * name) {
            return dynamic_cast<TH1*>(fNP->Get(Form("%s_ybin%d", name, iy))->Clone(rn()));
        };

        auto hNP = GetNP("nominal");
        path pNP = output / Form("NP_y%d.dat", iy);
        Write(pNP, hNP, NLOstat1D, iy);

        auto hEW = dynamic_cast<TH1*>(fEW->Get(Form("ybin%d", iy)));
        path pEW = output / Form("EW_y%d.dat", iy);
        Write(pEW, hEW, NLOstat1D, iy);
        delete hEW;

        auto hNPerr = GetNP("upper");
        hNPerr->Add(hNP,-1);
        //auto hNPup  = GetNP(      "upper"),
        //     hNPdn  = GetNP(      "lower"),
        //     hNPup2 = GetNP("alternative"),
        //     hNPdn2 = GetNP("alternative");
        //for (auto h: { hNPup, hNPdn/*, hNPup2, hNPdn2*/})
        //    h->Add(hNP,-1);
        //hNPdn2->Scale(-1); // symmetrise the uncertainty

        Term NP ("NP", "reaction", "KFactor", pNP),
             EW ("EW", "reaction", "KFactor", pEW);

        path pF = DAS_WORKAREA / Form("tables/FastNLO/%d/ak%d", year, R);
        pF /= Form("1jet.NNLO.fnl%dh_y%d_ptjet.tab", R == 7 ? 5332 : 5362, iy);
        assert(exists(pF));

        cout << "Looping over scales" << endl;
        //for (MuVar& muvar: muvars) 
        for (size_t ivar = 0; ivar < muvars.size(); ++ivar) {
            MuVar& muvar = muvars.at(ivar);
            //Table& globTableNLONLL = globTablesNLONLL.at(ivar); // TODO
            //Table& globTableNNLO   = globTablesNNLO  .at(ivar); // TODO

            path pNLL = Form("/nfs/dust/cms/user/tmakela/inclusiveJets/13TeV/workarea/tables/xFitter/ak7/NNLO_compatible/kFactor_NLO+NLL%s_y%d.dat", muvar.infix, iy); // TODO: make it local...
            assert(exists(pNLL));

            auto hNLL = dynamic_cast<TH1*>(NLOstat1D->Clone("hNLL"));
            ifstream fNLL(pNLL.c_str());
            do {
                double ylow, yup, ptlow, ptup, content, errup, errdn;
                fNLL >> ylow >> yup >> ptlow >> ptup >> content >> errup >> errdn;
                if (ptlow < 97) continue;
                //cout << '\t' << ylow << '\t' << yup << '\t' << ptlow << '\t' << ptup << '\t' << content << '\t' << errup << '\t' << errdn << endl;
                if (!fNLL.good()) break;
                double pt = (ptlow+ptup)/2;
                int i = hNLL->FindBin(pt);
                hNLL->SetBinContent(i, content);
                hNLL->SetBinError  (i, abs(errup)); // careful, this is actually the error in %!!!!!
            }
            while (fNLL.good());
            fNLL.close();

            path pCI = Form("/nfs/dust/cms/user/tmakela/inclusiveJets/13TeV/workarea/tables/CI/cms13r07sindCut_y%d.tab.ci", iy); // TODO: make it local?
            assert(exists(pCI));

            Term F  ("F" , "reaction", "fastNLO", pF, "", muvar, true),
                 NLL("kF", "reaction", "KFactor", pNLL),
                 CI ("CI", "reaction", "CIJET"  , pCI, "+", muvar, false);

            Table tableNLONLL(output / Form("Run2016_NLO+NLL%s_y%d.dat", muvar.infix, iy), iy, { F, NLL, NP, EW }, CMS, NDATA.at(iy)),
                  tableNNLO  (output / Form("Run2016_NNLO%s_y%d.dat"   , muvar.infix, iy), iy, { F,      NP, EW }, CMS, NDATA.at(iy)),
                  tableNLONLLCI(output / Form("Run2016_NLO+NLL_CI%s_y%d.dat", muvar.infix, iy), iy, { F, NLL, NP, EW, CI }, CMS, NDATA.at(iy)),
                  tableNNLOCI  (output / Form("Run2016_NNLO_CI%s_y%d.dat"   , muvar.infix, iy), iy, { F,      NP, EW, CI }, CMS, NDATA.at(iy));

            for (int ptbin = 1; ptbin <= NLOstat1D->GetNbinsX(); ++ptbin) {

                // rel stat unc of theory
                double eNLO  = 100 *  NLOstat1D->GetBinError(ptbin), // %
                       eNNLO = 100 * NNLOstat1D->GetBinError(ptbin); // %
                if (abs(eNLO) < feps || abs(eNNLO) < feps) continue;

                // NP uncertainties
                double pt = NLOstat1D->GetBinCenter(ptbin),
                       y = iy*0.5 + 0.25;
                int ptbjn = hNP->FindBin(pt);
                double NP = hNP->GetBinContent(ptbjn);
                assert(isnormal(NP));
                double NPerr = percentage(hNPerr ->GetBinContent(ptbjn), NP);
                //double NPerr1up = percentage(hNPup ->GetBinContent(ptbjn), NP),
                //       NPerr1dn = percentage(hNPdn ->GetBinContent(ptbjn), NP),
                //       NPerr2up = percentage(hNPup2->GetBinContent(ptbjn), NP),
                //       NPerr2dn = percentage(hNPdn2->GetBinContent(ptbjn), NP);

                //stringstream sNP;
                //sNP << setw(20) << NPerr1up << setw(20) << NPerr1dn
                //    << setw(20) << NPerr2up << setw(20) << NPerr2dn;

                //globTableNNLO.f << CMS(pt,y) << setw(20) << eNNLO << sNP.str() << '\n';
                tableNNLO  .f << CMS(pt,y) << setw(20) << eNNLO << setw(20) << NPerr << '\n';
                tableNNLOCI.f << CMS(pt,y) << setw(20) << eNNLO << setw(20) << NPerr << '\n';

                // NLL uncertainties
                double NLLerr = hNLL->GetBinError(ptbin);
                assert(isnormal(NLLerr));
                eNLO = hypot(eNLO,NLLerr);

                //globTableNLONLL.f << CMS(pt,y) << setw(20) << eNLO << sNP.str() << '\n';
                tableNLONLL  .f << CMS(pt,y) << setw(20) << eNLO << setw(20) << NPerr << '\n';
                tableNLONLLCI.f << CMS(pt,y) << setw(20) << eNLO << setw(20) << NPerr << '\n';
            }
            delete hNLL;
        }

        delete hNP; 
        delete hNPerr; 
        //delete hNPup;
        //delete hNPdn;
        //delete hNPup2;
        //delete hNPdn2;

        cout << "Writing correlations" << endl;
        for (int iy2 = 0; iy2 <= iy; ++iy2) {
            path p = output / Form("Run2016_y%d_y%d.corr", iy, iy2);
            cout << p << endl;
            Correlations correlations(p, iy, iy2, NDATA.at(iy), NDATA.at(iy2));

            for (int ptbin  = 1; ptbin  <= NLOstat2D->GetNbinsX(); ++ptbin ) 
            for (int ptbin2 = 1; ptbin2 <= NLOstat2D->GetNbinsX(); ++ptbin2) {
                double eNLO  = NLOstat2D->GetBinError(ptbin ,iy +1),
                       eNLO2 = NLOstat2D->GetBinError(ptbin2,iy2+1);
                if (abs(eNLO) < feps || abs(eNLO2) < feps) continue;

                double ptlow  = NLOstat1D->GetBinLowEdge(ptbin ), ylow  = iy *0.5,
                       ptlow2 = NLOstat1D->GetBinLowEdge(ptbin2), ylow2 = iy2*0.5;

                int ibin  = genBinning->GetGlobalBinNumber(ptlow +1, ylow +0.1),
                    ibin2 = genBinning->GetGlobalBinNumber(ptlow2+1, ylow2+0.1);

                correlations.f << setw(10) << ylow  << setw(10) << ptlow
                               << setw(10) << ylow2 << setw(10) << ptlow2
                               << setw(20) << CMS.Corr->GetBinContent(ibin,ibin2) << '\n';
            }
        }

        cout << "Writing correlations with old style" << endl;
        for (int iy2 = 0; iy2 < nYbins-1; ++iy2) {

            for (int ptbin  = 1; ptbin  <= NLOstat2D->GetNbinsX(); ++ptbin ) 
            for (int ptbin2 = 1; ptbin2 <= NLOstat2D->GetNbinsX(); ++ptbin2) {
                double eNLO  = NLOstat2D->GetBinError(ptbin ,iy +1),
                       eNLO2 = NLOstat2D->GetBinError(ptbin2,iy2+1);
                if (abs(eNLO) < feps || abs(eNLO2) < feps) continue;

                double ptlow  = NLOstat2D->GetXaxis()->GetBinLowEdge(ptbin ), ylow  = iy *0.5,
                       ptlow2 = NLOstat2D->GetXaxis()->GetBinLowEdge(ptbin2), ylow2 = iy2*0.5;

                int ibin  = genBinning->GetGlobalBinNumber(ptlow +1, ylow +0.1),
                    ibin2 = genBinning->GetGlobalBinNumber(ptlow2+1, ylow2+0.1);

                globCorrelations.f << setw(10) << ylow  << setw(10) << ptlow
                                   << setw(10) << ylow2 << setw(10) << ptlow2
                                   << setw(20) << CMS.Corr->GetBinContent(ibin,ibin2) << '\n';
            }
        }

        delete NLOstat1D;
        delete NNLOstat1D;
    }

    fNP->Close();
    fEW->Close();

    cout << "Done" << endl;
}
                  
#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    cout << TUnfoldV17::GetTUnfoldVersion() << endl;

    assert(exists(DAS_WORKAREA));

    if (argc < 3) {
        cout << argv[0] << " input output [alternative]\n"
             << "\twhere\tinput = root file, output from `unfold`\n"
             << "\t     \toutput = new directory to host all output files\n"
             << "\t     \talternative = alternative to inputData to derive model uncertainty (not mandatory)\n"
             << "\t     \tsmooth = alternative to input used to get smooth systematic uncertainties (not mandatory)\n"
           //<< "\t     \tR = 4 or 7\n"
           //<< "\t     \tyear = 201?\n"
             << flush;
        return EXIT_SUCCESS;
    }

    path input = argv[1],
         output = argv[2],
         alternative = argc > 3 ? argv[3] : "",
         smooth = argc > 4 ? argv[4] : "";

    int R = 7;
    int year = 2016;

    getxFitterTables(input, output, alternative, smooth, R, year);
    return EXIT_SUCCESS;
}
#endif
