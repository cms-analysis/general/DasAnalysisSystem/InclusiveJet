#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include <experimental/filesystem>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TList.h>
#include <TKey.h>

#include "Math/VectorUtil.h"
#include "InclusiveJet/UnfoldingSampleND/bin/PtY.h"

using namespace std;
using namespace experimental::filesystem;
using namespace DAS;
using namespace DAS::InclusiveJet;

static const vector<double> ptMax { 3103, 2787, 2238, 1588, 1248};

void Write (TH1 * h, TDirectory * dOut)
{
    const char * name = Form("%s2D",h->GetName());
    TH2 * h2 = dynamic_cast<TH2*>(genBinning->ExtractHistogram(name, h));
    h2->SetTitle(h->GetTitle());
    dOut->cd();
    h2->SetDirectory(dOut);

    auto ptax = h2->GetXaxis();
    for (int iy = 0; iy < nYbins; ++iy)
    for (int ptbin = ptax->GetNbins(); ptax->GetBinCenter(ptbin) > ptMax.at(iy); --ptbin) {
        h2->SetBinContent(ptbin, iy+1, 0);
        h2->SetBinError  (ptbin, iy+1, 0);
    }
    //h2->Write();

    for (int ybin = 1; ybin <= nYbins; ++ybin) {
        auto name_ybin = Form("%s_ybin%d",h2->GetName(), ybin);
        auto h1 = h2->ProjectionX(name_ybin, ybin, ybin);
        h1->Scale(1,"width");
        h1->Write();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// getHEPdataInput
///
/// Extract with TUnfoldBinning proper TH2 plain-ROOT histograms
/// from the mappings to TH1 histograms 
///
/// Also calculate uncertainties for plotting
void getHEPdataInput 
             (const path& inputNom,  //!< name of input root file 
              const path& inputVar,  //!< name of input root file 
              const path& inputAlt,  //!< name of input root file 
              const path& inputNP,
              const path& inputEW,
              const path& output) //!< name of output root file
{
    for (auto& p: {inputNom, inputVar, inputAlt/*, inputNP, inputEW*/}) {
        cout << p << endl;
        assert(exists(p));
    }

    InitBinning();

    auto source = TFile::Open(inputNom.c_str(), "READ");

    auto unf       = dynamic_cast<TH1 *>(source->Get("nominal" )); 
    auto cov       = dynamic_cast<TH2 *>(source->Get("cov"     ));
    auto covTotal  = dynamic_cast<TH2 *>(source->Get("covTotal"));
    auto corrTotal = dynamic_cast<TH2 *>(source->Get("corrTotal"));
    unf      ->SetDirectory(0); 
    cov      ->SetDirectory(0);
    covTotal ->SetDirectory(0);
    corrTotal->SetDirectory(0);

    const int N = unf->GetNbinsX();

    unf->SetName("unf");

    auto unfUncorr = dynamic_cast<TH1 *>(unf->Clone("unfUncorr"));

    // STATISTICAL UNCERTAINTIES
    for (int i = 1; i <= N; ++i) {
        double content = unf->GetBinContent(i),
               err2    = cov->GetBinContent(i,i),
               errTot2 = covTotal->GetBinContent(i,i);
        if (content > 0) {
            unf      ->SetBinError(i, sqrt(err2   ));
            unfUncorr->SetBinError(i, sqrt(errTot2));
        }
    }

    unf->SetTitle("stat. unc.");
    unfUncorr->SetTitle("all uncorr. unc.");

    auto file = TFile::Open(output.c_str(), "RECREATE");
    Write(unfUncorr, file);

    auto sourceSmooth = TFile::Open(inputVar.c_str(), "READ");

    for (const char * n: {"JES", "MC"}) {
        sourceSmooth->cd();
        TDirectory * d = dynamic_cast<TDirectory *>(sourceSmooth->Get(n));
        d->cd();
        TIter Next(d->GetListOfKeys()) ;
        TKey* key = nullptr;
        while ( (key = dynamic_cast<TKey*>(Next())) ) {
            d->cd();
            TH1 * h = dynamic_cast<TH1*>(key->ReadObj());
            if (h == nullptr) continue;
            TString name = h->GetName();
            h->Add(unf, -1);

            Write(h, file);
        }
    }

    // lumi
    auto Lumi = dynamic_cast<TH1*>(unf->Clone("Lumiup"));
    Lumi->Scale(0.012);
    Write(Lumi, file);
    Lumi->SetName("Lumidn");
    Lumi->Scale(-1);
    Write(Lumi, file);

    // model
    cout << "Including model uncertainty (also made symmetric)" << endl;
    TFile * altSource = TFile::Open(inputAlt.c_str(), "READ");
    auto model = dynamic_cast<TH1 *>(altSource->Get("nominal")); 
    model->SetDirectory(0);
    altSource->Close();
    model->Add(unf, -1);
    model->SetName("modelup");
    Write(model, file);
    model->Scale(-1);
    model->SetName("modeldn");
    Write(model, file);

    // correlations
    for (int ybin1 = 1; ybin1 <= nYbins; ++ybin1) 
    for (int ybin2 = 1; ybin2 <= nYbins; ++ybin2) {
        auto name = Form("corr_y%d_y%d", ybin1, ybin2);
        auto title = "correlations for " + yBins.at(ybin1-1) + " and " + yBins.at(ybin2-1);
        TH2 * cell = new TH2D(name, title, nGenBins, genBins.data(), nGenBins, genBins.data());
        double ptmin = 74, ptmax1 = ptMax.at(ybin1-1), ptmax2 = ptMax.at(ybin2-1);
        int iminpt = cell->GetXaxis()->FindBin(ptmin),
            imaxpt1 = cell->GetXaxis()->FindBin(ptmax1),
            imaxpt2 = cell->GetXaxis()->FindBin(ptmax2);
        for (int ipt1 = iminpt; ipt1 <= imaxpt1; ++ipt1) 
        for (int ipt2 = iminpt; ipt2 <= imaxpt2; ++ipt2) {
            auto pt1 = cell->GetXaxis()->GetBinCenter(ipt1),
                 pt2 = cell->GetYaxis()->GetBinCenter(ipt2);
            auto ibin1 = genBinning->GetGlobalBinNumber(pt1,0.5*(y_edges.at(ybin1)+y_edges.at(ybin1-1))),
                 ibin2 = genBinning->GetGlobalBinNumber(pt2,0.5*(y_edges.at(ybin2)+y_edges.at(ybin2-1)));
            auto content = corrTotal->GetBinContent(ibin1, ibin2);
            assert(abs(content) < 1.00001);
            cell->SetBinContent(ipt1, ipt2, content);
            cell->SetMinimum(corrTotal->GetMinimum());
            cell->SetMaximum(corrTotal->GetMaximum());
        }
        cell->Write();
    }

    // EW corrections
    if (exists(inputEW)) {
        auto fNP = TFile::Open(inputEW.c_str(), "READ");
        for (int iy = 0; iy < nYbins; ++iy) {
            fNP->cd();
            auto h = dynamic_cast<TH1*>(fNP->Get(Form("ybin%d", iy)));
            // remove high pt bins
            for (int ptbin = h->GetNbinsX(); h->GetBinCenter(ptbin) > ptMax.at(iy); --ptbin) 
                h->SetBinContent(ptbin, 0);
            h->SetDirectory(file);
            file->cd();
            h->Write(Form("EW_ybin%d", iy+1));
        }
    }

    // NP corrections
    if (exists(inputNP)) {
        auto fNP = TFile::Open(inputNP.c_str(), "READ");
        for (int iy = 0; iy < nYbins; ++iy) {
            fNP->cd();
            auto hNom = dynamic_cast<TH1*>(fNP->Get(Form("nominal_ybin%d", iy))),
                 hVar = dynamic_cast<TH1*>(fNP->Get(Form("upper_ybin%d", iy)));
            hVar->Add(hNom,-1);
            // remove high pt bins
            for (int ptbin = hNom->GetNbinsX(); hNom->GetBinCenter(ptbin) > ptMax.at(iy); --ptbin) {
                hNom->SetBinContent(ptbin, 0);
                hVar->SetBinContent(ptbin, 0);
            }
            hNom->SetDirectory(file);
            hVar->SetDirectory(file);
            file->cd();
            hNom->Write(Form("NP_ybin%d", iy+1));
            hVar->Write(Form("NPunc_ybin%d", iy+1));
        }
    }

    source->Close();
    sourceSmooth->Close();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 7) {
        cout << argv[0] << " inputNom inputVar inputAlt inputNP inputEW output \n"
             << "where\tinputNom = root file, corresponding to output from `unfold` corresponding to inclusive jet spectrum\n"
             << "     \tinputVar = root file, corresponding to output from `getSmoothUncPtY`\n"
             << "     \tinputAlt = alternative to `inputNom` used to derive model uncertainty\n"
             << "     \tinputNP = root file with NP corrections\n"
             << "     \tinputEW = root file with EW corrections\n"
             << "     \toutput = root file with histograms formatted for `hepdata_lib` \n"
             << flush;
        return EXIT_SUCCESS;
    }

    path inputNom = argv[1],
         inputVar = argv[2],
         inputAlt = argv[3],
         inputNP = argv[4],
         inputEW = argv[5],
         output = argv[6];

    getHEPdataInput(inputNom, inputVar, inputAlt, inputNP, inputEW, output);
    return EXIT_SUCCESS;
}
#endif

