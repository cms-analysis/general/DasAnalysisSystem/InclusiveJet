#ifndef H_MYTUNFOLDDENSITY
#define H_MYTUNFOLDDENSITY

#include <TUnfold/TUnfoldDensity.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Trick to access protected members of `TUnfoldDensity` or to modify the 
/// behaviour of certain methods, e.g. background subtraction.
class MyTUnfoldDensity : public TUnfoldDensity {

    ////////////////////////////////////////////////////////////////////////////////
    /// Perform background subtraction.
    ///
    /// Copy-pasted and modified method of `TUnfoldSys`
    //void DoBackgroundSubtraction (void);

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Variadic template to access `TUnfoldDensity::AddRegularisationCondition`
    template<class ...Args> Bool_t MyAddRegularisationCondition (Args ...args)
    {
        return AddRegularisationCondition(args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Variadic template to access the constructor `TUnfoldDensity::TUnfoldDensity`
    template<class ...Args> MyTUnfoldDensity (Args ...args) :
        TUnfoldDensity(args...)
    {}

    ////////////////////////////////////////////////////////////////////////////////
    /// Specify a source of background.
    ///
    /// \param[in] bgr background distribution with uncorrelated errors
    /// \param[in] name identifier for this background source
    /// \param[in] scale normalisation factor applied to the background
    /// \param[in] scale_error normalisation uncertainty
    ///
    /// The contribution <b>scale</b>*<b>bgr</b> is subtracted from the
    /// measurement prior to unfolding. The following contributions are
    /// added to the input covarianc ematrix
    ///
    ///   - using the uncorrelated histogram errors <b>dbgr</b>, the contribution
    /// (<b>scale</b>*<b>dbgr<sub>i</sub></b>)<sup>2</sup> is added to the
    /// diagonals of the covariance
    ///   - using the histogram contents, the background normalisation uncertainty contribution
    /// <b>dscale</b>*<b>bgr<sub>i</sub></b> <b>dscale</b>*<b>bgr<sub>j</sub></b>
    /// is added to the covariance matrix
    ///
    /// Data members modified:
    ///   fBgrIn,fBgrErrUncorrInSq,fBgrErrScaleIn and those modified by DoBackgroundSubtraction()
    //void AddCorrUnc (const TH2 *bgr,const char *name,Double_t scale,Double_t scale_error)
};

}
#endif
