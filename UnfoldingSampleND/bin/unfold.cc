#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <limits>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TLegend.h>
#include <TKey.h>
#include <TMatrixD.h>
#include <TVectorD.h>
#include <TDecompSVD.h>

#include <TUnfold/TUnfoldDensity.h>
#include "InclusiveJet/UnfoldingSampleND/interface/MyTUnfoldDensity.h"

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/variables.h"

using namespace std;
using namespace DAS;

#include "correlations.h"

static const auto deps = numeric_limits<double>::epsilon();

////////////////////////////////////////////////////////////////////////////////
/// Condition of the Probability Matrix
///
/// Calculates the condition of the probability matrix with the gen binning.
///
/// Note: we do *not* include the miss entries in the normalisation since they
///       are *not* included in inverted probability matrix
double Condition (TH2 * RM)
{
    cout << __LINE__ << "\tCalculating condition" << endl;
    const int Nx = RM->GetNbinsX(),
              Ny = RM->GetNbinsY();

    TH1 * RMx = RM->ProjectionX("RMx", 0, -1);

    // normalisation & condition
    TMatrixD m(Ny,Nx); // unfortunately, we have to swap the axes...
    for (int i = 1; i <= Nx; ++i) {
        double normalisation = RMx->GetBinContent(i);
        if (normalisation > 0)
        for (int j = 1; j <= Ny; ++j) 
            m(j-1,i-1) = RM->GetBinContent(i,j) / normalisation;
    }
    TDecompSVD svd(m);
    TVectorD v = svd.GetSig();

    double Max = v[0], Min = v[0];
    for(int k = 0; k < Nx; ++k) {
        if (abs(v[k]) < 1e-5) break;
        Min = v[k];
        cout << setw(5) << k << setw(15) << v[k] << setw(15) << Max/Min << '\n';
    }
    if (Min > 0) return Max/Min;
    else         return -numeric_limits<double>::infinity();
}

////////////////////////////////////////////////////////////////////////////////
/// Bottom Line Test
///
/// Checks the agreement of data and MC before and after unfolding
void BLT (TH1 * dataDist, TH2 * dataCov, TH1 * MC, int rebin = 1)
{
    dataDist->Rebin(rebin);
    MC->Rebin(rebin);
    dataCov->Rebin2D(rebin, rebin);
    vector<int> indices;
    for (int i = 1; i <= dataCov->GetNbinsX(); ++i) 
        if (dataCov->GetBinContent(i,i) > 0) indices.push_back(i);
    int ndf = indices.size();

    TVectorD v(ndf);
    TMatrixD M(ndf,ndf);
    for (int i = 0; i < ndf; ++i) {
        int index = indices.at(i);
        double iData = dataDist->GetBinContent(index),
               iMC   = MC      ->GetBinContent(index);
        v(i) = iData-iMC;
        for (int j = 0; j < ndf; ++j) {
            int jndex = indices.at(j);
            M(i,j) = dataCov->GetBinContent(index,jndex);
        }
    }
    M.Invert();
    
    double chi2 = v*(M*v);

    cout << "chi2/ndf = " << chi2 << " / " << ndf << " = " << (chi2/ndf) << endl;
}

////////////////////////////////////////////////////////////////////////////////
/// folding
///
/// Check that the MC closes: (gen - sum of misses) * PM = (rec - sum of fakes)
void folding (TH1 * gen, TH1 * rec, TH2 * RM,
        const map<TString, TH1 *>& miss, //!< miss counts
        const map<TString, TH1 *>& fake) //!< fake counts
{
    gen = dynamic_cast<TH1 *>(gen->Clone("gen_folding"));
    rec = dynamic_cast<TH1 *>(rec->Clone("rec_folding"));
    RM  = dynamic_cast<TH2 *>(RM ->Clone( "RM_folding"));

    for (auto& m: miss) gen->Add(m.second, -1);
    for (auto& f: fake) rec->Add(f.second, -1);

    const int Nx = RM->GetNbinsX(),
              Ny = RM->GetNbinsY();

    // normalisation
    for (int i = 1; i <= Nx; ++i) {
        double normalisation = RM->Integral(i, i, 1, Ny);
        if (normalisation > 0)
        for (int j = 1; j <= Ny; ++j) {
            double content = RM->GetBinContent(i,j);
            content /= normalisation;
            RM->SetBinContent(i,j, content);
        }
    }

    // check folding
    bool Fail = false;
    for (int j = 1; j <= Ny; ++j) {
        double error = rec->GetBinError(j);
        if (abs(error) < deps) continue;

        double content = rec->GetBinContent(j);
        if (isnan(content)) Fail = true;

        for (int i = 1; i <= Nx; ++i) 
            content -= gen->GetBinContent(i) * RM->GetBinContent(i,j);

        if (abs(content) < error / 100) continue;
        Fail = true;
        cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: Folding test is failing in bin " << j << " with difference = " << content << "\x1B[30m\e[0m\n";
    }

    if (Fail) {
        cerr << __LINE__ << "\t\x1B[31m\e[1mFailure: folding test has failed\x1B[30m\e[0m\n";
        //exit(EXIT_FAILURE);
    }

    delete gen;
    delete rec;
    delete RM;
}

////////////////////////////////////////////////////////////////////////////////
/// CheckMargin
///
/// Set underflow/overflow to 0
void CheckMargin (TH1 * h, TH2 * cov, const char * name, int ibin) {
    if (h->GetBinContent(ibin) > 0 || h->GetBinError(ibin) > 0 || cov->GetBinContent(ibin,ibin) > 0) {
        cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: " << name << " is not empty! It will be set to zero, but no guarantee it works...\x1B[30m\e[0m\n";

        h->SetBinContent(ibin,0);
        h->SetBinError  (ibin,0);
        for (int i = 0; i <= h->GetNbinsX(); ++i) {
            cov->SetBinContent(ibin,i,0);
            cov->SetBinContent(i,ibin,0);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// PrepareRegularisation
///
/// Load bias and L-matrix from the output file of one of the `getLmatrix*` commands
void PrepareRegularisation (TString MCvar, TFile * fReg, MyTUnfoldDensity * density)
{
    // getting bias x_0
    auto bias = dynamic_cast<TH1*>(fReg->Get(MCvar + "/bias"));
    assert(bias != nullptr);
    density->SetBias(bias);

    // getting L matrix
    auto L = dynamic_cast<TH2*>(fReg->Get(MCvar + "/L"));

    int Nconditions = L->GetNbinsY(), // arbitrarily large number
        NtrueBins   = L->GetNbinsX(); // must be exactly the number of bins at hadron level

    for (int j = 1; j <= Nconditions; ++j) { // y-axis

        vector<int> indices;
        vector<double> conditions;

        for (int i = 1; i <= NtrueBins; ++i) { // x-axis
            double content = L->GetBinContent(i, j);
            if (abs(content) < deps) continue;
            indices.push_back(i);
            conditions.push_back(content);
        }

        // cheating with call to protected method
        assert(indices.size() == conditions.size());

        //cout << setw(3) << i;
        //for (size_t j = 0; j < indices.size(); ++j) cout << setw(5) << indices.at(j) << setw(15) << conditions.at(j);
        //cout << '\n';
        density->MyAddRegularisationCondition(indices.size(), indices.data(), conditions.data());
    }
    //cout << flush;
}

////////////////////////////////////////////////////////////////////////////////
/// SaveLcurve
///
/// Save L-curve and other curves, as well as solutions
void SaveLcurve (TGraph * lCurve, TSpline * logTauX, TSpline * logTauY,
            TSpline * logTauCurvature, int iBest, MyTUnfoldDensity * density)
{
    cout << __LINE__ << "\tSaving control plots for L-curve scan" << endl;
    Double_t t[1],x[1],y[1], c[1];
    logTauX->GetKnot(iBest,t[0],x[0]);
    logTauY->GetKnot(iBest,t[0],y[0]);
    logTauCurvature->GetKnot(iBest,t[0],c[0]);
    TGraph * bestLcurve          = new TGraph(1,x,y);
    TGraph * bestLogTauX         = new TGraph(1,t,x);
    TGraph * bestLogTauY         = new TGraph(1,t,y);
    TGraph * bestLogTauCurvature = new TGraph(1,t,c);

    double chi2a = density->GetChi2A(),
           chi2l = density->GetChi2L();
    int ndf = density->GetNdf();

    cout << __LINE__ << "\tchi^2/ndf = " << chi2a/ndf << " + " << chi2l/ndf << endl;

    double tau = density->GetTau();

    cout << __LINE__ << "\ttau = " << tau << endl;

    lCurve    ->SetNameTitle("lCurve"    , Form("#tau = %f", tau));
    bestLcurve->SetNameTitle("bestLcurve", Form(       "%f", tau));

    logTauX    ->SetNameTitle("logTauX", "log(#chi_{A}^{2})");
    bestLogTauX->SetNameTitle("bestLogTauX", Form("#chi_{A}^{2}/ndf = %f", chi2a/ndf));

    logTauY    ->SetNameTitle("logTauY", "log(#chi_{L}^{2}/#tau)");
    bestLogTauY->SetNameTitle("bestLogTauY", Form("#chi_{L}^{2}/ndf = %f", chi2l/ndf));

    logTauCurvature    ->SetNameTitle("logTauCurvature", Form("%f", tau));
    bestLogTauCurvature->SetNameTitle("bestLogTauCurvature", Form("%f", tau));

    for (TGraph * g: {lCurve, bestLcurve, bestLogTauCurvature,
                               bestLogTauX, bestLogTauY }) g->Write();
    for (TSpline * s: {logTauX, logTauY, logTauCurvature}) s->Write();

    density->GetL          ("L"          , "L-matrix"         )->Write();
    density->GetLxMinusBias("LxMinusBias", "L*(x-f_{b}*x_{0})")->Write();

    // TODO: delete?
}

////////////////////////////////////////////////////////////////////////////////
/// CorrectInefficiencies
///
/// Since miss entries are not accounted for in the migrations of the RM, they
/// must be corrected after the unfolding
void CorrectInefficiencies(TH1 * unf, const map<TString, TH1 *>& miss)
{
    cout << __LINE__ << "\tCorrecting miss entries" << endl;
    for (int i = 1; i <= unf->GetNbinsX(); ++i) {
        double content = unf->GetBinContent(i);

        // check negative entries
        // Note: you should not worry if you get a value compatible with zero
        //       BUT you should not trust the result if the value is not at all compatible with zero
        if (content < 0)  {
            double error = unf->GetBinError(i);
            cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: bin " << i << " has negative content: " << content << " +/- " << error << "\x1B[30m\e[0m\n";
        }

        // miss correction
        double factor = 1;
        for (auto& m: miss) factor += m.second->GetBinContent(i);
        content *= factor;
        unf->SetBinContent(i, content);
        // Note: we avoid explicitely the use of TH1::Multiply() 
        //       in order to have full control on the propagation
        //       of the uncertainties (done later with cov matrix)
    }
    cout << flush;
}

////////////////////////////////////////////////////////////////////////////////
/// RedoUnfold
///
/// Repeat unfolding for alternative values of tau parameter
void RedoUnfold (MyTUnfoldDensity * density, double newtau, TString name,
        TString subtitle, TDirectory * d, const map<TString, TH1 *>& miss)
{
    density->DoUnfold(newtau); // TODO?
    auto unf = dynamic_cast<TH1*>(density->GetOutput(name,
                "hadron level (" + subtitle + ")"));
    CorrectInefficiencies(unf, miss);
    unf->SetDirectory(d);
    unf->Write();
}


#define DEFAULT_NORM_UNC 0.05

////////////////////////////////////////////////////////////////////////////////
/// Unfolding output from one of the `getUnfHistograms*.C` executables (indifferently)
///
/// Note that this code does *not* require `TUnfoldBinning`
/// -> `TUnfoldBinning` is used to make the mapping from ND to 1D in other 
///    executables, but it is not necessary to make the unfolding itself
///
/// Comment on the structure of the code:
/// - nested loops, one over the variations in MC, the other over the variations in data
/// - we vary either MC or Data, but never both at the same time
/// - CT is only run for nominal entries
void unfold
             (TString inputData, //!< name of input root file (after `getUnfHistograms*`)
              TString inputMC,   //!< name of input root file (after `getUnfHistograms*`)
              TString output,    //!< name of output root file with unfolded spectra
              const float fakeNormVar = DEFAULT_NORM_UNC, //!< parameter for variation of the normalisation of fake count
              const float missNormVar = DEFAULT_NORM_UNC, //!< parameter for variation of the normalisation of miss count
              TString inputSysUncorr = '\0',          //!< root file after `getSysUnorr*`
              TString inputRegularisation = '\0')   //!< root file after `getLmatrix*`
{
    // flag for closure test (only run for the nominal value)
    bool CT = false;
    for (const char * name: {"Pythia", "MadGraph", "Herwig"})
        CT = CT || inputData.Contains(name);

    bool sysUncorr  = inputSysUncorr      != "",
         regularise = inputRegularisation != "";

    cout << __LINE__ << "\tOpening input files" << endl;
    TFile * fData = TFile::Open(inputData, "READ"),
          * fMC   = TFile::Open(inputMC  , "READ"),
          * fSys  = sysUncorr  ? TFile::Open(inputSysUncorr     , "READ") : nullptr,
          * fReg  = regularise ? TFile::Open(inputRegularisation, "READ") : nullptr;

    cout << __LINE__ << "\tPreparing output file" << endl;
    TFile * fOut = TFile::Open(output, "RECREATE");
    TDirectory * dJES    = fOut->mkdir("JES"   ),
               * dJEScov = fOut->mkdir("JEScov"),
               * dUncorr = fOut->mkdir("Uncorr"),
               * dMC     = fOut->mkdir("MC"    ),
               * dReg    = regularise ? fOut->mkdir("regularisation") : nullptr;

    fMC->cd();
    // the three next lines just correspond to a loop over the entries of a TFile / TDirectory
    TIter Next0(fMC->GetListOfKeys()) ;
    TKey* key0 = nullptr;
    while ( (key0 = dynamic_cast<TKey*>(Next0())) ) { // we are looping over directories corresponding to MC variations

        auto dirMC = dynamic_cast<TDirectory*>(key0->ReadObj());
        TString MCvar = dirMC->GetName();
        if (CT && MCvar != "nominal") continue; // CT is run only for nominal value
        dirMC->cd();

        cout << __LINE__ << "\tExtracting miss and fake counts" << endl;
        map<TString, TH1 *> miss, // only to be added by hand at the end
                            fake; // seen as backgrounds from the point of view of TUnfold 
        // reminder: in principle, one source of miss / fake is enough for the unfolding,
        // but it's good to be able to disentangle the different sources

        // the three next lines just correspond to a loop over the entries of a TFile / TDirectory
        // here, we want to retrieve only miss and fake
        TIter Next(dirMC->GetListOfKeys()) ;
        TKey* key = nullptr;
        while ( (key = dynamic_cast<TKey*>(Next())) ) { // we are looping over objects such as RM, rec, cov, miss, etc.
            auto h = dynamic_cast<TH1*>(key->ReadObj());
            cout << __LINE__ << '\t' << h << endl; // if you read 0 here, then the histograms is probably corrupt
            TString name = h->GetName();
            if (!name.Contains("miss") && !name.Contains("fake")) continue;
            if (h->GetEntries() == 0) {
               cout << __LINE__ << "\t" << name << "\tis empty.. Exiting" << endl;
               exit(EXIT_FAILURE);
            }
            name.ReplaceAll(MCvar, "");
            h->SetDirectory(0);
            if (name.Contains("fake")) {
                name.ReplaceAll("fake", "");
                cout << __LINE__ << "\tfake\t" << name << endl;
                fake.insert({name,h});
            }
            else {
                name.ReplaceAll("miss", "");
                cout << __LINE__ << "\tmiss\t" << name << endl;
                miss.insert({name,h});
            }
        } // end of retrieving of miss/fake histograms from MC

        cout << __LINE__ << "\tExtracting RM from MC" << endl;
        auto RM = dynamic_cast<TH2 *>(dirMC->Get("RM"));
        RM->SetDirectory(0);

        auto gen = dynamic_cast<TH1 *>(dirMC->Get("gen")->Clone("genNoMiss"));
        auto rec = dynamic_cast<TH1 *>(dirMC->Get("rec")->Clone("recNoFake"));

        folding(gen, rec, RM, miss, fake);

        cout << __LINE__ << "\tCalculating miss and fake rates" << endl;

        // we want a fake/miss >> RATE << (i.e. a value between 0 and 1)
        // -> we will use it later to estimate the fake/miss contribution in the data using a bin-by-bin method
        // currently: the elements of `fake`/`miss` are currently fake/miss *counts* (including model dependence)
        for (auto& f: fake) f.second->Divide(f.second, rec, 1, 1, "b");
        delete rec;
        for (auto& m: miss) gen->Add(m.second, -1);
        for (auto& m: miss) m.second->Divide(m.second, gen, 1, 1, "b");
        delete gen;
        // now: the elements of `fake`/`miss` are currently fake/miss *rates*

        // now we are done with retrieving objects from fMC, and we move to fData

        fData->cd();

        // now we estimate the fake contribution in data with bin-by-bin method
        {
            auto rec = dynamic_cast<TH1 *>(fData->Get("nominal/rec")->Clone("recData"));
            // reminder: the elements of `fake` are currently fake *rates*
            for (auto& f: fake) f.second->Multiply(rec);
            // now: the elements of `fake` are currently fake *counts* (corrected to data)
            delete rec;
        }
        // (similar thing will be done with the miss contribution *after* the unfolding)

        // Once again: now we have absolute fake count BUT miss rate

        cout << __LINE__ << "\tExtracting JES variations from variations in Data and immediately running unfolding" << endl;

        // here we loop again on the entries of a file,
        // but we recycle `key` and `Next`, which were defined earlier
        Next = TIter(fData->GetListOfKeys()) ;
        while ( (key = dynamic_cast<TKey*>(Next())) ) {

            fData->cd();
            auto dir = dynamic_cast<TDirectory*>(key->ReadObj());
            TString DATAvar = dir->GetName(); // data variation name
            if (DATAvar != "nominal") {
                if (CT) continue; // reminder: for CT, we only consider the nominal value (to reduce the output & running time)
                if (MCvar != "nominal") continue; // we never vary both data and MC in the same time
            }

            TString var = MCvar == "nominal" ? DATAvar : MCvar;
            cout << __LINE__ << '\t' << var << endl;

            dir->cd();
            auto h = dynamic_cast<TH1 *>(dir->Get("rec")->Clone("rec_" + DATAvar));
            h->SetTitle("detector level (" + var + " variation)");
            h->SetDirectory(0);
            auto cov = dynamic_cast<TH2 *>(dir->Get("cov")->Clone("recCov_" + DATAvar));
            cov->SetTitle("covariance matrix at detector level (" + var + " variation)");
            cov->SetDirectory(0);

            cout << __LINE__ << '\t' << "\e[1m" << DATAvar << ' ' << h << ' ' << cov << "\e[0m" << endl;

            if (DATAvar.Contains("nominal")) { // if data variation is nominal,
                                               // but the condition number will
                                               // be calculated for each variation of MC
                auto condition = Condition(RM);
                RM->SetTitle(Form("%.1f",condition)); // so that you keep track of the condition number
            }

            // underflow and overflow are expected to be empty (if not, will be set to 0 with a warning)
            CheckMargin(h, cov, "underflow", 0);
            CheckMargin(h, cov, "overflow", h->GetNbinsX()+1);

            // so far, still no unfolding done :) it's coming soon (but still one or two things to check...)

            cout << __LINE__ << "\tSetting up density & input (checking and comparing the bin contents of data and MC)" << endl;

            MyTUnfoldDensity * density = nullptr;
            bool badStatus = false;
            do {
                density = new MyTUnfoldDensity(RM, // reminder: this is varied by the first loop
                        TUnfold::kHistMapOutputHoriz, // truth level on x-axis of the response matrix
                        TUnfold::kRegModeNone, // we do not use any of the predefined regularisation schemes
                        TUnfold::kEConstraintNone, // no constraint on area
                        TUnfoldDensity::kDensityModeBinWidthAndUser); // no scale factors, matrix L is similar to unity matrix (?)

                // reminder: the machine epsilon is the smallest possible number
                // -> here, we set it to the epsilon of the `double` type
                density->SetEpsMatrix(deps);
                // Note: if you try with the epsilon of the `float` type, expect big problems...
                // -> indeed, if the epsilon to high, many entries of the covariance matrix 
                //    will be considered as = 0... (then problem of rank, inversion, etc.)

                int status = density->SetInput(h, // reconstructed level
                        0, // bias factor (only relevant for Tikhonov regularisation)
                        0, // for bins with zero error, this number defines 1/error
                        cov); // covariance matrix

                badStatus = status >= 10000;
                if (badStatus) { // example cases: while unfolding the inclusive jet with Pythia works fine,
                                 //                unfolding with MadGraph throws some error due to different
                                 //                populations / sampling of the bins
                    // in case of bad status, we just remove bins, where the data has entries while the simulation has none (and vice-versa)
                    delete density;
                    cout << "# bad errors (not fatal) = "     << status % 10000 << '\n'
                         << "# unconstrained bins (fatal) = " << status / 10000 << endl;

                    cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: the recovery is currently only implemented for the case of twice more gen bins than rec bins! (will abort otherwise)\x1B[30m\e[0m\n";

                    assert(RM->GetNbinsX()*2 == RM->GetNbinsY()); // TODO: check

                    for (int i = 1; i <= RM->GetNbinsX(); ++i) {
                        auto Data = h->GetBinContent(2*i) + h->GetBinContent(2*i-1);
                        auto MC = RM->Integral(i,i,0,-1);
                        cout << setw(3) << i << setw(20) << Data << setw(20) << MC << '\n';
                        if (abs(Data) < deps) {
                            cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: bin " << i << " will be removed from MC\x1B[30m\e[0m\n";
                            for (int j = 1; j <= RM->GetNbinsY(); ++j) {
                                RM->SetBinContent(i,j,0);
                                RM->SetBinError  (i,j,0);
                            }
                        }
                        if (abs(MC) < deps) {
                            cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: bin " << i << " will be removed from Data\x1B[30m\e[0m\n";
                            for (int k1: {2*i, 2*i+1}) {
                                h->SetBinContent(k1,0);
                                h->SetBinError  (k1,0);
                                for (int k2 = 1; k2 <= cov->GetNbinsX(); ++k2) {
                                    cov->SetBinContent(k1,k2,0);
                                    cov->SetBinError  (k1,k2,0);
                                    cov->SetBinContent(k2,k1,0);
                                    cov->SetBinError  (k2,k1,0);
                                }
                            }
                        }
                    }
                    cout << endl;
                }
            }
            while (badStatus);

            // now, we should have (nearly) everything settled for the unfolding...

            cout << __LINE__ << "\tSubtract background" << endl;
            for (auto& f: fake) { // remember that fake now contains esimations of the fake contributions in data
                cout << __LINE__ << '\t' << f.first << endl;
                density->SubtractBackground(f.second, // 1-D histogram in rec-level binning (with its own stat unc -- we will use later `GetEmatrixSysBackgroundUncorr()`)
                                            "fake" + f.first, // name
                                            1.0, // scale factor
                                            fakeNormVar); // factor to vary the normalisation (we will use later `GetDeltaSysBackgroundScale()`)
                // the line just above is equivalent to three things:
                //  - `h->Add(f.second, -1);`, i.e. subtracting the fake contribution from the rec level
                //  - it propagates the contribution from the limited statistics to the covariance matrix used in the unfolding
                //  - TUnfold provides a systematic variations
            }

            if (sysUncorr) {
                cout << __LINE__ << "\tAdding bin-to-bin uncertainty" << endl;

                auto hSysUncorr = dynamic_cast<TH1 *>(fSys->GetDirectory(DATAvar)->Get("sysUncorr1D"));
                density->SubtractBackground(hSysUncorr, "sysUncorr", 1.0, 0.);
            }

            cout << __LINE__ << "\tUnfolding" << endl;
            static float tau = 0.;
            if (regularise) {

                PrepareRegularisation(MCvar, fReg, density);

                if (var == "nominal") {

                    cout << __LINE__ << "\tScanning L-curve" << endl;

                    TGraph * lCurve = nullptr;
                    TSpline * logTauX = nullptr,
                            * logTauY = nullptr,
                            * logTauCurvature = nullptr;

                    int iBest = density->ScanLcurve(100, // nPoints
                            1e-6, 1e2, // tau interval (if 0-0, then automated)
                            &lCurve, &logTauX, &logTauY,
                            &logTauCurvature);

                    dReg->cd();
                    SaveLcurve(lCurve, logTauX, logTauY, logTauCurvature, iBest, density);

                    tau = density->GetTau();

                    cout << __LINE__ << "\tSaving shifts for variations of tau parameter" << endl;
                    dMC->cd();
                    RedoUnfold(density, tau*2, "Regup"  , "upper variation of regularisation parameter", dMC , miss);
                    RedoUnfold(density, tau/2, "Regdown", "lower variation of regularisation parameter", dMC , miss);
                    fOut->cd();
                    RedoUnfold(density,     0, "noReg"  , "no Tikhonov regularisation",                  fOut, miss);
                } 
            } 

            density->DoUnfold(tau);

            // now, the unfolding is done, and we have to retrieve the result and apply the correction for miss entries

            // the structure of the output file is as follows:
            //  - we want the nominal value at the root of the file (nominal for both data and MC)
            //  - we want the data variations (but nominal for MC) in `dJES`
            //  - we want the MC variations (but nominal for data) in `dMC`
            auto dOut = MCvar == "nominal" ? dynamic_cast<TDirectory *>(fOut) : dMC; 
            dOut->cd();
            auto d = (DATAvar == "nominal") ? dOut : dJES;
            d->cd();

            // from now on, we only need TUnfold to retrieve objecs, and we apply corrections by hand

            cout << __LINE__ << "\tGetting output" << endl;
            auto unf = dynamic_cast<TH1*>(density->GetOutput(var,
                        "hadron level (" + var + " variation)")); // here, we get the unfolded histogram
            CorrectInefficiencies(unf, miss);
            unf->SetDirectory(d);
            unf->Write();

            cout << __LINE__ << "\tCovariance matrix from measurement" << endl;
            TH2 * covInput = density->GetEmatrixInput("cov" + var, // histogram name
                    "covariance matrix coming from input data distribution (" + var + " variation)"); // histogram title
            covInput->SetDirectory(dJEScov);
            dJEScov->cd();
            covInput->Write(var);
            d->cd();

            if (var != "nominal") continue;

            covInput->Write("cov"); // might seem redundant to save it second time, but this is to avoid breaking compatibility with other codes

            cout << __LINE__ << "\tSaving correlation matrix from measurement (in main directory)" << endl;
            SaveCorr(covInput, "corr", "correlation matrix coming from input data distribution", d); // transform cov matrix into corr matrix

            // main reason to do it is to get the condition number (saved in the title)
            // -> otherwise, it should be identical to the input `fMC` file
            cout << __LINE__ << "\tSaving response matrix (in main directory)" << endl;
            RM->SetDirectory(d);
            RM->Write("RM"); // title was already set earlier (corresponding to condition number)

            cout << __LINE__ << "\tGetting backfolding" << endl;
            TH1 * backfolding = density->GetFoldedOutput("backfolding", // histogram name
                    "detector level (backfolded)", // histogram title
                    0 /* dist name */, 0 /* axis steering */,
                    true /* axis binning */, true /* add bg */);
            backfolding->Write();
            h->Write("rec"); // we save it only now because we only save the nominal value
            //cov->Write("recCov"); // TODO?

            if (CT) {
                auto gen = dynamic_cast<TH1 *>(dirMC->Get("gen"));
                gen->SetDirectory(d);
                gen->Write();
                delete gen;
            }

            cout << __LINE__ << "\tGetting systematic uncertainties from MC and from unfolding" << endl;

            // just initialising histogram to contain contribution from the limited statistics of the MC
            // (not only from RM, but also from fake and miss histograms)
            auto covUncor = dynamic_cast<TH2*>(covInput->Clone("covUncor"));
            covUncor->SetTitle("covariance matrix at hadron level coming from the MC (RM + miss + fake)");
            covUncor->Reset();

            dUncorr->cd();
            cout << __LINE__ << "\tCovariance matrix from RM" << endl;
            TH2 * SysUncorr = density->GetEmatrixSysUncorr("RM", // histogram name
                    "covariance matrix at hadron level coming from the RM"); // histogram title
            SysUncorr->SetDirectory(dUncorr);
            SysUncorr->Write();
            covUncor->Add(SysUncorr);
            cout << __LINE__ << "\tCovariance matrices from backgrounds" << endl;
            for (auto& f: fake) {
                TH2 * SysBackgroundUncorr = density->GetEmatrixSysBackgroundUncorr("fake" + f.first, // source
                        "fake" + f.first, // histogram name
                        Form("covariance matrix coming from %s", f.second->GetTitle()));
                SysBackgroundUncorr->SetDirectory(dUncorr);
                SysBackgroundUncorr->Write();
                covUncor->Add(SysBackgroundUncorr);
                delete SysBackgroundUncorr;
            }

            if (sysUncorr) {
                cout << __LINE__ << "\tCovariance matrices from trigger and prefiring uncertainties" << endl;
                TH2 * SysBackgroundUncorr = density->GetEmatrixSysBackgroundUncorr("sysUncorr",
                        "sysUncorr", "covariance matrix from trigger and prefiring uncertainties");
                SysBackgroundUncorr->SetDirectory(dUncorr);
                SysBackgroundUncorr->Write();
                covUncor->Add(SysBackgroundUncorr);
                delete SysBackgroundUncorr;
            }
            cout << __LINE__ << "\tCovariance matrices from inefficiencies" << endl;
            for (const auto& m: miss) { // only using miss for the names
                auto SysIneffUncorr = dynamic_cast<TH2 *>(SysUncorr->Clone("miss" + m.first));
                SysIneffUncorr->Reset();
                for (int i = 1; i <= m.second->GetNbinsX(); ++i) {
                    double error = m.second->GetBinError(i) * abs(unf->GetBinContent(i));
                    SysIneffUncorr->SetBinContent(i, i, pow(error, 2));
                }
                SysIneffUncorr->SetDirectory(dUncorr);
                SysIneffUncorr->Write();
                covUncor->Add(SysIneffUncorr);
                delete SysIneffUncorr;
            }
            dOut->cd();
            covUncor->SetDirectory(dOut);
            covUncor->Write();

            auto covTotal = dynamic_cast<TH2*>(covInput->Clone("covTotal"));
            covTotal->Add(covUncor);
            covTotal->SetTitle("covariance matrix at hadron level including contributions from data and MC");
            covTotal->SetDirectory(dOut);
            covTotal->Write("covTotal");
            SaveCorr(covTotal, "corrTotal", "correlation matrix at hadron level including contributions from data and MC", d);

            dMC->cd();

            cout << __LINE__ << "\tSaving estimates of fake counts (in main directory) and shifts from background variations (in `MC` directory)" << endl;
            for (auto& f: fake) {
                TString name = "fake" + f.first,
                        title = f.second->GetTitle();
                cout << __LINE__ << '\t' << name << endl;

                // count
                d->cd();
                f.second->SetDirectory(d);
                f.second->SetTitle(title + " (corrected)");
                f.second->Write(name);

                dMC->cd();

                // upper shift
                {
                    TH1 * shift = density->GetDeltaSysBackgroundScale(name, // name of the source given in `SubtractBackground()`
                                                                      name + "up", // name of new histogram
                                                                      "upper shift of " + title); // title of new histogram
                    shift->SetDirectory(dMC);
                    shift->Write();
                    delete shift;
                }

                // lower shift
                {
                    TH1 * shift = density->GetDeltaSysBackgroundScale(name, // name of the source given in `SubtractBackground()`
                                                                      name + "down", // name of new histogram
                                                                      "lower shift of " + title); // title of new histogram
                    shift->Scale(-1);
                    shift->SetDirectory(dMC);
                    shift->Write();
                    delete shift;
                }
            }

            cout << __LINE__ << "\tDeleting density" << endl;
            delete density;

            cout << __LINE__ << "\tSaving estimates of miss counts (in main directory) and shifts from inefficiency variations (in `MC` directory)" << endl;
            for (auto& m: miss) {
                TString name = "miss" + m.first,
                        title = m.second->GetTitle();
                cout << __LINE__ << '\t' << name << endl;

                auto M = dynamic_cast<TH1*>(m.second->Clone(name));

                // count
                fOut->cd();
                for (int i = 1; i <= unf->GetNbinsX(); ++i) {
                    double u = unf->GetBinContent(i),
                           content = m.second->GetBinContent(i) *     u ,
                           error   = m.second->GetBinError  (i) * abs(u);
                    M->SetBinContent(i, content);
                    M->SetBinError  (i, error  );
                }
                M->SetDirectory(fOut);
                M->Write();

                dMC->cd();

                // upper shifts
                {
                    auto shift = dynamic_cast<TH1 *>(M->Clone(name + "up"));
                    shift->Scale(missNormVar);
                    shift->SetDirectory(dMC);
                    shift->SetTitle("upper shift of " + title);
                    shift->Write();
                    delete shift;
                }

                // lower shifts
                {
                    auto shift = dynamic_cast<TH1 *>(M->Clone(name + "down"));
                    shift->Scale(-1);
                    shift->Scale(missNormVar);
                    shift->SetDirectory(dMC);
                    shift->SetTitle("lower shift of " + title);
                    shift->Write();
                    delete shift;
                }
            }

            // basically, everything has now been retrieved
            // -> now we just perform some last test

            cout << __LINE__ << "\tBottom line test" << endl;
            {
                auto recMC = dynamic_cast<TH1 *>(dirMC->Get("rec")->Clone("recBLT")),
                     genMC = dynamic_cast<TH1 *>(dirMC->Get("gen")->Clone("genBLT"));

                int rebin = recMC->GetNbinsX()/genMC->GetNbinsX();

                cout << "Detector level (`rebin` = " << rebin << "): ";
                BLT(h, cov, recMC, rebin);

                cout << "Hadron level (with data unc. only): ";
                BLT(unf, covInput, genMC);

                cout << "Hadron level (with data + MC unc.): ";
                BLT(unf, covTotal, genMC);

                delete recMC;
                delete genMC;
            }

            cout << __LINE__ << "\tDeleting various object before moving to next variation" << endl;
            delete h;
            delete cov;
            delete unf;
            delete covInput;
            delete covUncor;
            delete covTotal;
        } // end of data variations 
    } // end of MC variations

    cout << "Done" << endl;

    fOut->cd();
    fOut->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    cout << TUnfoldV17::GetTUnfoldVersion() << endl;

    if (argc < 4) {
        cout << argv[0] << " inputData inputMC output fakeNormVar missNormVar bias\n"
             << "\twhere\tinputData = root file from any of the `getUnfHistogram*` command containing (pseudo-)data to be unfolded\n"
             << "\t     \tinputMC = root file from the same `getUnfHistogram*` command containing the simulation used to perform the unfolding\n"
             << "\t     \toutput = root file with unfolded cross section & all uncertainties\n"
             << "\t     \tfakeNormVar = uncertainty on normalisation of fake entries (default: " << DEFAULT_NORM_UNC << ", expecting value between 0 and 1))\n"
             << "\t     \tmissNormVar = uncertainty on normalisation of miss entries (default: " << DEFAULT_NORM_UNC << ", expecting value between 0 and 1))\n"
             << "\t     \tinputSysUncorr = root file from the corresponding `getSysUncorr*` command containing the bin-to-bin uncorrelated uncertainties for trigger and/or prefiring (default: "")\n"
             << "\t     \tinputRegularisation = root file from the corresponding `getLmatrix*` command containing the bias and the L-matrix (default: "")\n"
             << "Notes:\n"
             << " - Only the three first parameters are mandatory.\n"
             << " - Ignore regularisation option UNLESS you are SURE of what you are doing!\n"
             << " - If the name of the input contains Pythia or MadGraph or Herwig (i.e. for closure test), only the nominal variation is unfolded.\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString inputData = argv[1],
            inputMC   = argv[2],
            output    = argv[3];

    float fakeNormVar = argc > 4 ? atof(argv[4]) : DEFAULT_NORM_UNC,
          missNormVar = argc > 5 ? atof(argv[5]) : DEFAULT_NORM_UNC;

    TString inputSysUncorr      = argc > 6 ? argv[6] : "",
            inputRegularisation = argc > 7 ? argv[7] : "";

    unfold(inputData, inputMC, output, fakeNormVar, missNormVar, inputSysUncorr, inputRegularisation);
    return EXIT_SUCCESS;
}
#endif

