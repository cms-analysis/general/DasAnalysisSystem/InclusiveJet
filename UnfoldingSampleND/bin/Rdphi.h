#include <TUnfold/TUnfoldBinning.h>

#include <map>

static TUnfoldBinning * recBinning = new TUnfoldBinning("recBinning");
static TUnfoldBinning * genBinning = new TUnfoldBinning("genBinning");

namespace DAS {
namespace Rdphi {
static const vector<double> recBins { /*50, 58, 66, 76, 88, 100,*/ 115, 130, 150, 175, 200, 225, 250, 275, 300, 330, 360, 390, 430, 470, 510, 550, 600, 650, 700, 750, 800, 860, 920, 980, 1050, 1120, 1190, 1260, 1340, 1420, 1500, 1590, 1680, 1770, 1870, 1970, 2070, 2180, 2300, 2430, 2560, 2700, 3000},
                            genBins { /*50,     66,     88,     */ 115,      150,      200,      250,      300,      360,      430,      510,      600,      700,      800,      920,      1050,       1190,       1340,       1500,       1680,       1870,       2070,       2300,       2560,       3000},
                            n_edges{-0.5,0.5,1.5,2.5,3.5/*,4.5*/};

static const size_t nGenBins = genBins.size()-1,
                    nRecBins = recBins.size()-1;

static const float minpt = genBins.front(), minptN = 100, maxpt = genBins.back(), maxy = 2.5;
//static const std::map<int,float> minpt = {{2016, genBins.front()}, {2017, genBins.at(1)}, {2018, genBins.at(1)}};
static const int maxN = DAS::Rdphi::n_edges.back();
}
}

void InitBinning (bool multiplicity = true, bool years = false) 
{
    recBinning->AddAxis("recpt",DAS::Rdphi::nRecBins,DAS::Rdphi::recBins.data(),false,false);
    genBinning->AddAxis("genpt",DAS::Rdphi::nGenBins,DAS::Rdphi::genBins.data(),false,false);
    if (multiplicity) {
        recBinning->AddAxis("recn" ,1+DAS::Rdphi::maxN,DAS::Rdphi::n_edges.data(),false,false);
        genBinning->AddAxis("genn" ,1+DAS::Rdphi::maxN,DAS::Rdphi::n_edges.data(),false,false);
    }
    if (years) {
        std::vector<double> years {2015.5, 2016.5, 2017.5, 2018.5};
        int nYears = years.size()-1;
        recBinning->AddAxis("year", nYears, years.data(), false, false);
    }
}

