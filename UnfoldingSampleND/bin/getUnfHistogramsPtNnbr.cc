#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <utility>

#include <experimental/filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/stream.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/JEC/interface/JESreader.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include "Core/JEC/bin/matching.h"

#include "Rdphi.h"

using namespace std;
using namespace DAS;
using namespace experimental::filesystem;

template<typename Jet> inline bool GoodAngleSep (const Jet& Ref, const Jet& Test) 
{
    using ROOT::Math::VectorUtil::DeltaPhi;
    float dphi = abs(DeltaPhi(Ref.p4, Test.p4));
    return ((dphi > 2.*M_PI/3.) && (dphi < 7.*M_PI/8.));
}

struct Variation {
    const TString name;
    static bool isMC;
    const size_t iJEC, iJetWgt, iEvtWgt;
    TH1 * rec, * tmp, * gen,
        * missNoMatch, * missOutN, * missOutY, * missOutLow, * missOutHigh,
        * fakeNoMatch, * fakeOutN, * fakeOutY, * fakeOutLow, * fakeOutHigh;    
    TH2 * cov, * RM;

    Variation (TString Name, size_t IJEC = 0 , size_t IJetWgt = 0 , size_t IEvtWgt = 0) :
        name(Name), iJEC(IJEC), iJetWgt(IJetWgt), iEvtWgt(IEvtWgt),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatch(nullptr), missOutN(nullptr), missOutY(nullptr), missOutLow(nullptr), missOutHigh(nullptr),
        fakeNoMatch(nullptr), fakeOutN(nullptr), fakeOutY(nullptr), fakeOutLow(nullptr), fakeOutHigh(nullptr),
        cov(nullptr), RM(nullptr)
    {
        rec = recBinning->CreateHistogram(Name + "rec"),
        tmp = recBinning->CreateHistogram(Name + "tmp");
        cov = recBinning->CreateErrorMatrixHistogram(Name + "cov", false);
        if (!isMC) return;
        gen           = genBinning->CreateHistogram(Name + "gen"      ),
        missNoMatch = genBinning->CreateHistogram(Name + "missNoMatch"),
        missOutY    = genBinning->CreateHistogram(Name + "missOutY"   ),
        missOutN    = genBinning->CreateHistogram(Name + "missOutN"   ),
        missOutHigh = genBinning->CreateHistogram(Name + "missOutHigh"),
        missOutLow  = genBinning->CreateHistogram(Name + "missOutLow" ),
        fakeNoMatch = recBinning->CreateHistogram(Name + "fakeNoMatch"),
        fakeOutY    = recBinning->CreateHistogram(Name + "fakeOutY"   ),
        fakeOutN    = recBinning->CreateHistogram(Name + "fakeOutN"   ),
        fakeOutHigh = recBinning->CreateHistogram(Name + "fakeOutHigh"),
        fakeOutLow  = recBinning->CreateHistogram(Name + "fakeOutLow" );
        RM          = TUnfoldBinning::CreateHistogramOfMigrations(genBinning, recBinning, Name + "RM");
    }

    void Write (TFile * file)
    {
        cout << "Writing " << name << endl;
        file->cd();
        TDirectory * dir = file->mkdir(name);
        dir->cd();
#define PAIR(name) make_pair(dynamic_cast<TH1*>(name), #name)
        for (auto p: { PAIR(gen), PAIR(missNoMatch), PAIR(missOutN), PAIR(missOutY), PAIR(missOutHigh), PAIR(missOutLow),
                       PAIR(rec), PAIR(fakeNoMatch), PAIR(fakeOutN), PAIR(fakeOutY), PAIR(fakeOutHigh), PAIR(fakeOutLow),
                       PAIR(cov), PAIR(RM) }) {
#undef PAIR
            if (p.first == nullptr) continue;
            p.first->SetDirectory(dir);
            p.first->Write(p.second);
        }
    }
};
bool Variation::isMC = false; // just fort initialisation

double GetLumiReweight (int thisYear, int R)
{
    double thisLumi = 0, totalLumi = 0;
    for (int year: {2016,2017,2018}) {
        ifstream file;
        const char * DAS_WORKAREA = getenv("DAS_WORKAREA");
        path fname = Form("%s/tables/luminosities/%d/ak%d/Run%d.lumi", DAS_WORKAREA, year, R, year);
        assert(exists(fname));
        file.open(fname.c_str());
        float lumi = 0;
        while (file.good()) file >> lumi;
        file.close();
        cout << year << setw(10) << lumi << '\n';
        totalLumi += lumi;
        if (year == thisYear) thisLumi = lumi;
    }
    cout << flush;
    assert(thisLumi > 0);
    assert(totalLumi > 0);

    float lumiWgt = thisLumi / totalLumi; 
    cout << "lumiWgt = " << lumiWgt << endl;
    return lumiWgt;
}

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding for both pt and y (in d=2), 
/// using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHistogramsPtNnbr 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              bool combine,   //!< flag whether the output will be combined for full-Run-2 or not (-> different normalisations)
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    InitBinning();

    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    cout << "Setting up branches" << endl;

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();
    int year = metainfo.year();
    float radius = metainfo.radius();

    assert(year > 2015 && year < 2019);
    auto lumiWgt = combine ? GetLumiReweight(year, 10*radius) : 1.;

    TTreeReader reader(tree);
    TTreeReaderValue<Event> ev = {reader, "event"};
    TTreeReaderValue<vector<RecJet>> recjets = {reader, "recJets"};
    TTreeReaderValue<vector<GenJet>> * genjets = nullptr;
    if (isMC)
        genjets = new TTreeReaderValue<vector<GenJet>>({reader, "genJets"}); 

    TFile * file = TFile::Open(output, "RECREATE");

    cout << "Setting variations" << endl;

    Variation::isMC = isMC;
    vector<Variation> variations { Variation("nominal") };
    if (isMC) {
        variations.push_back( Variation("JERup"  , 1, 0, 0) );
        variations.push_back( Variation("JERdown", 2, 0, 0) );
        variations.push_back( Variation("PUup"  , 0, 0, 1) );
        variations.push_back( Variation("PUdown", 0, 0, 2) );
        //variations.push_back( Variation("ModelPtup"  , 0, 0, 3) );
        //variations.push_back( Variation("ModelPtdown", 0, 0, 4) );
        //variations.push_back( Variation("ModelNnbrup"  , 0, 0, 5) ); // TODO
        //variations.push_back( Variation("ModelNnbrdown", 0, 0, 6) );
    }
    else {
        variations.push_back( Variation("Prefup"     , 0, 0, year == 2018 ? 0 : 1) );
        variations.push_back( Variation("Prefdown"   , 0, 0, year == 2018 ? 0 : 2) );
        int i = 1;
        for (TString& JES: JESreader::getJECUncNames()) {
            variations.push_back( Variation(JES + "up"  , i  , 0, 0) );
            variations.push_back( Variation(JES + "down", i+1, 0, 0) );
            i += 2;
        }
    }

    cout << "maxN = " << Rdphi::maxN << endl;

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

        if (isMC) {
            assert(ev->weights.size() == 3);
            //Model::Pt(*ev, *recjets, **genjets);
            //Model::Nnbr(*ev, *recjets, **genjets); // TODO
        }

        for (Variation& v: variations) {

            v.tmp->Reset();

            auto evWgt = lumiWgt * ev->weights.at(v.iEvtWgt);

            vector<int> binIDs; // we save the indices of the filled bins in order to avoid many multiplications by 0
            for (RecJet& recjet: *recjets) {
                auto y  = abs(recjet.Rapidity());
                if (y >= Rdphi::maxy) continue;
                auto pt = recjet.CorrPt(v.iJEC);
                if (pt < Rdphi::minpt/*.at(year)*/ || pt > Rdphi::maxpt) continue;

                int Nnbr = 0;
                for (const RecJet& candidate: *recjets) {
                    auto ptc = candidate.CorrPt(v.iJEC), yc = abs(candidate.Rapidity());
                    if (abs(ptc - pt) < feps) continue; // if it's the same jet, continue
                    if (ptc < Rdphi::minptN || ptc > Rdphi::maxpt || yc > Rdphi::maxy) continue;
                    if (GoodAngleSep(recjet, candidate)) ++Nnbr;
                }
                if (Nnbr > Rdphi::maxN) continue;

                auto irecbin = recBinning->GetGlobalBinNumber(pt,Nnbr/*,year*/);
                if (irecbin == 0)
                    cout << pt << ' ' << Nnbr << endl;
                assert(irecbin > 0);
                if (find(binIDs.begin(), binIDs.end(), irecbin) == binIDs.end()) binIDs.push_back(irecbin);

                auto jWgt = recjet.weights.at(v.iJetWgt);

                v.rec->Fill(irecbin, evWgt * jWgt);
                v.tmp->Fill(irecbin, evWgt * jWgt); // for cov
            }

            for (auto x: binIDs) for (auto y: binIDs) {
                auto cCov = v.cov->GetBinContent(x,y),
                     cTmp = v.tmp->GetBinContent(x)*v.tmp->GetBinContent(y);
                v.cov->SetBinContent(x,y,cCov+cTmp);
            }
        }

        if (!isMC) continue;

        Matching<RecJet, GenJet> matching(*recjets);
        for (GenJet& genjet: **genjets) {
            auto genpt = genjet.p4.Pt(),
                 geny = abs(genjet.Rapidity());
            
            int genNnbr = 0;
            for (const GenJet& candidate: **genjets) {
                auto ptc = candidate.p4.Pt(), yc = abs(candidate.Rapidity());
                if (ptc < Rdphi::minptN || ptc > Rdphi::maxpt || yc > Rdphi::maxy) continue;
                if (GoodAngleSep(genjet, candidate)) ++genNnbr;
            }

            bool LowGenPt  = genpt   <  Rdphi::minpt/*.at(year)*/,
                 HighGenPt = genpt   >= Rdphi::maxpt,
                 HighGenY  = geny    >= Rdphi::maxy,
                 HighGenN  = genNnbr >  Rdphi::maxN;
            bool goodGen = (!LowGenPt) && (!HighGenPt) && (!HighGenY) && (!HighGenN);

            auto igenbin = genBinning->GetGlobalBinNumber(genpt,genNnbr);
            if (goodGen) assert(igenbin > 0);

            RecJet recjet = matching.HighestPt(genjet);
            bool noMatch = recjet.p4.Pt() > 13e3 /* 13 TeV */;

            for (Variation& v: variations) {

                auto evWgt = lumiWgt * ev->weights.at(v.iEvtWgt);

                if (goodGen) v.gen->Fill(igenbin, evWgt);

                if (noMatch) {
                    if (goodGen) v.missNoMatch->Fill(igenbin, evWgt);
                    continue;
                }

                auto jWgt = recjet.weights.at(v.iJetWgt);

                auto recpt = recjet.CorrPt(v.iJEC),
                     recy  = abs(recjet.Rapidity());

                int recNnbr = 0;
                for (const RecJet& candidate: *recjets) {
                    auto ptc = candidate.CorrPt(v.iJEC), yc = abs(candidate.Rapidity());
                    if (ptc < Rdphi::minptN || ptc > Rdphi::maxpt || yc >= Rdphi::maxy) continue;
                    if (GoodAngleSep(recjet, candidate)) ++recNnbr;
                }

                bool LowRecPt  = recpt   <  Rdphi::minpt/*.at(year)*/,
                     HighRecPt = recpt   >= Rdphi::maxpt,
                     HighRecY  = recy    >= Rdphi::maxy,
                     HighRecN  = recNnbr >  Rdphi::maxN;
                bool goodRec = (!LowRecPt) && (!HighRecPt) && (!HighRecY) && (!HighRecN);

                auto irecbin = recBinning->GetGlobalBinNumber(recpt,recNnbr/*,year*/);
                if (goodRec) assert(irecbin > 0);

                if      ( goodRec &&  goodGen) { v.RM         ->Fill(igenbin, irecbin, evWgt *      jWgt ); // TODO: how to consider the weight of the neighbours...?
                                                 v.missNoMatch->Fill(igenbin,          evWgt * (1 - jWgt)); } // for jet weights != 1 
                else if (!goodRec &&  goodGen) { if (  LowRecPt  && (!HighRecPt) && (!HighRecY) && (!HighRecN)) v.missOutLow ->Fill(igenbin, evWgt       );
                                            else if ((!LowRecPt) &&   HighRecPt  && (!HighRecY) && (!HighRecN)) v.missOutHigh->Fill(igenbin, evWgt       );
                                            else if ((!LowRecPt) && (!HighRecPt) &&   HighRecY  && (!HighRecN)) v.missOutY   ->Fill(igenbin, evWgt       );  
                                            else if ((!LowRecPt) && (!HighRecPt) && (!HighRecY) &&   HighRecN ) v.missOutN   ->Fill(igenbin, evWgt       ); 
                                            else                                                                v.missNoMatch->Fill(igenbin, evWgt       ); }
                else if ( goodRec && !goodGen) { if (  LowGenPt  && (!HighGenPt) && (!HighGenY) && (!HighGenN)) v.fakeOutLow ->Fill(irecbin, evWgt * jWgt);
                                            else if ((!LowGenPt) &&   HighGenPt  && (!HighGenY) && (!HighGenN)) v.fakeOutHigh->Fill(irecbin, evWgt * jWgt);
                                            else if ((!LowGenPt) && (!HighGenPt) &&   HighGenY  && (!HighGenN)) v.fakeOutY   ->Fill(irecbin, evWgt * jWgt);  
                                            else if ((!LowGenPt) && (!HighGenPt) && (!HighGenY) &&   HighGenN ) v.fakeOutN   ->Fill(irecbin, evWgt * jWgt); 
                                            else                                                                v.fakeNoMatch->Fill(irecbin, evWgt * jWgt); }
            }
            // TODO: try to use the underflow and overflow for matching out of the phase space...
        }

        for (RecJet& recjet: matching.mcands) {
            for (Variation& v: variations) {

                auto y  = abs(recjet.Rapidity());
                if (y >= Rdphi::maxy) continue;
                auto pt = recjet.CorrPt(v.iJEC);
                if (pt < Rdphi::minpt/*.at(year)*/ || pt > Rdphi::maxpt) continue;

                int Nnbr = 0;
                for (const RecJet& candidate: *recjets) {
                    auto ptc = candidate.CorrPt(v.iJEC), yc = abs(candidate.Rapidity());
                    if (ptc < Rdphi::minptN || ptc > Rdphi::maxpt || yc >= Rdphi::maxy) continue;
                    if (GoodAngleSep(recjet, candidate)) ++Nnbr;
                }

                if (Nnbr > Rdphi::maxN) continue;

                auto evWgt = lumiWgt * ev->weights.at(v.iEvtWgt);
                auto jWgt = recjet.weights.at(v.iJetWgt);

                auto irecbin = recBinning->GetGlobalBinNumber(pt,Nnbr/*,year*/);
                assert(irecbin > 0);
                v.fakeNoMatch->Fill(irecbin, evWgt * jWgt);
            }
        }
    }

    for (Variation& v: variations)
        v.Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output combineFullRun2 [nSplit [nNow]]\n"
             << "\twhere\tinput = any n-tuple\n"
             << "\t     \toutput = a root file with format compatible with `unfold` executable\n"
             << "\t     \tcombineFullRun2 = flag whether the output will be combined into global distribution for full-Run-2 unfolding (yes/no, true/false, 1/0)\n"
             << "NB: with `combineFullRun2`, the luminosity in $DAS_WORKAREA/tables/luminosities is automatically retrieved.\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    TString combineFullRun2 = argv[3];
    combineFullRun2.ToLower();
    bool combine = combineFullRun2.Contains("true") || combineFullRun2.Contains("yes") || (combineFullRun2.Atoi() > 0);
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    getUnfHistogramsPtNnbr(input, output, combine, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
