#include "Core/CommonTools/interface/variables.h"

#include <vector>

#include <TUnfold/TUnfoldBinning.h>

namespace DAS {
namespace InclusiveJet {

static const auto minpt = DAS::recBins.at(2) /* 74 */, maxpt = DAS::recBins.back(), maxy = DAS::y_edges.back();
//static const std::vector<float> maxpts { 3103, 2787, 2500, 1784, 1101};

static TUnfoldBinning * recBinning = new TUnfoldBinning("recInclJet");
static TUnfoldBinning * genBinning = new TUnfoldBinning("genInclJet");

void InitBinning () 
{
    recBinning->AddAxis("recpt",nRecBins,recBins.data(),false,false);
    recBinning->AddAxis("recy" ,  nYbins,y_edges.data(),false,false);
    genBinning->AddAxis("genpt",nGenBins,genBins.data(),false,false);
    genBinning->AddAxis("geny" ,  nYbins,y_edges.data(),false,false);
}

}
}

