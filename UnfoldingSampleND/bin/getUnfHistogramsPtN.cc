#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <utility>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/stream.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/JEC/interface/JESreader.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include "Core/JEC/bin/matching.h"

#include "PtN.h"

using namespace std;

using namespace DAS;
using namespace DAS::JetMultiplicity;

struct Variation {
    const TString name;
    static bool isMC;
    const size_t iJEC, iJetWgt, iEvtWgt;
    TH1 * rec, * tmp, * gen,
        * missNoMatch, * missOutN, * missOutY, * missOutLow, * missOutHigh,
        * fakeNoMatch, * fakeOutN, * fakeOutY, * fakeOutLow, * fakeOutHigh;    
    TH2 * cov, * RM;

    Variation (TString Name, size_t IJEC = 0 , size_t IJetWgt = 0 , size_t IEvtWgt = 0) :
        name(Name), iJEC(IJEC), iJetWgt(IJetWgt), iEvtWgt(IEvtWgt),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatch(nullptr), missOutN(nullptr), missOutY(nullptr), missOutLow(nullptr), missOutHigh(nullptr),
        fakeNoMatch(nullptr), fakeOutN(nullptr), fakeOutY(nullptr), fakeOutLow(nullptr), fakeOutHigh(nullptr),
        cov(nullptr), RM(nullptr)
    {
        rec = recBinning->CreateHistogram(Name + "rec"),
        tmp = recBinning->CreateHistogram(Name + "tmp");
        cov = recBinning->CreateErrorMatrixHistogram(Name + "cov", false);
        if (!isMC) return;
        gen         = genBinning->CreateHistogram(Name + "gen"        ),
        missNoMatch = genBinning->CreateHistogram(Name + "missNoMatch"),
        missOutY    = genBinning->CreateHistogram(Name + "missOutY"   ),
        missOutN    = genBinning->CreateHistogram(Name + "missOutN"   ),
        missOutHigh = genBinning->CreateHistogram(Name + "missOutHigh"),
        missOutLow  = genBinning->CreateHistogram(Name + "missOutLow" ),
        fakeNoMatch = recBinning->CreateHistogram(Name + "fakeNoMatch"),
        fakeOutY    = recBinning->CreateHistogram(Name + "fakeOutY"   ),
        fakeOutN    = recBinning->CreateHistogram(Name + "fakeOutN"   ),
        fakeOutHigh = recBinning->CreateHistogram(Name + "fakeOutHigh"),
        fakeOutLow  = recBinning->CreateHistogram(Name + "fakeOutLow" );
        RM          = TUnfoldBinning::CreateHistogramOfMigrations(genBinning, recBinning, Name + "RM");
    }

    void Write (TFile * file)
    {
        cout << "Writing " << name << endl;
        file->cd();
        TDirectory * dir = file->mkdir(name);
        dir->cd();
#define PAIR(name) make_pair(dynamic_cast<TH1*>(name), #name)
        for (auto p: { PAIR(gen), PAIR(missNoMatch), PAIR(missOutN), PAIR(missOutY), PAIR(missOutHigh), PAIR(missOutLow),
                       PAIR(rec), PAIR(fakeNoMatch), PAIR(fakeOutN), PAIR(fakeOutY), PAIR(fakeOutHigh), PAIR(fakeOutLow),
                       PAIR(cov), PAIR(RM) }) {
#undef PAIR
            if (p.first == nullptr) continue;
            p.first->SetDirectory(dir);
            p.first->Write(p.second);
        }
    }
};
bool Variation::isMC = false; // just fort initialisation

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding for both pt and y (in d=2), 
/// using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHistogramsPtN 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    InitBinning();

    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    cout << "Setting up branches" << endl;

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    Event * ev = nullptr;
    tree->SetBranchAddress("event", &ev);
    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);
    if (isMC) 
        tree->SetBranchAddress("genJets", &genJets);

    TFile * file = TFile::Open(output, "RECREATE");

    cout << "Setting variations" << endl;

    Variation::isMC = isMC;
    vector<Variation> variations { Variation("nominal") };
    if (isMC) {
        variations.push_back( Variation("JERup"      , 1, 0, 0) );
        variations.push_back( Variation("JERdown"    , 2, 0, 0) );
        variations.push_back( Variation("Prefup"     , 0, 1, 0) );
        variations.push_back( Variation("Prefdown"   , 0, 2, 0) );
        variations.push_back( Variation("PUup"       , 0, 0, 1) );
        variations.push_back( Variation("PUdown"     , 0, 0, 2) );
    }
    else {
        int i = 1;
        for (TString& JES: JESreader::getJECUncNames()) {
            variations.push_back( Variation(JES + "up"  , i  , 0, 0) );
            variations.push_back( Variation(JES + "down", i+1, 0, 0) );
            i += 2;
        }
    }

    cout << "Looping" << endl;
    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        for (Variation& v: variations) {

            v.tmp->Reset();

            auto evWgt = ev->weights.at(v.iEvtWgt);

            vector<int> binIDs; // we save the indices of the filled bins in order to avoid many multiplications by 0
            int N = 0;
            for (RecJet& recJet: *recJets) {

                ++N;
                if (N >= maxN) break;

                auto y  = abs(recJet.Rapidity());
                if (y >= maxy) continue;

                auto pt = recJet.CorrPt(v.iJEC);
                if (pt < minpt || pt >= maxpt) continue;

                auto ibin = recBinning->GetGlobalBinNumber(pt,N);
                assert(ibin > 0);
                if (find(binIDs.begin(), binIDs.end(), ibin) == binIDs.end()) binIDs.push_back(ibin);

                auto jWgt = recJet.weights.at(v.iJetWgt);

                v.rec->Fill(ibin, evWgt * jWgt);
                v.tmp->Fill(ibin, evWgt * jWgt); // for cov
            }

            for (auto x: binIDs) for (auto y: binIDs) {
                auto cCov = v.cov->GetBinContent(x,y),
                     cTmp = v.tmp->GetBinContent(x)*v.tmp->GetBinContent(y);
                v.cov->SetBinContent(x,y,cCov+cTmp);
            }
        }

        if (!isMC) continue;

        for (Variation& v: variations) {

            sort(recJets->begin(), recJets->end(),
                    [&v](const RecJet& j1, const RecJet& j2)
                    { return j1.CorrPt(v.iJEC) > j2.CorrPt(v.iJEC); });

            Matching<RecJet, GenJet> matching(*recJets);

            int genN = 0;
            for (GenJet& genJet: *genJets) {
                ++genN;

                auto genpt = genJet.p4.Pt(),
                     geny = abs(genJet.Rapidity());

                bool LowGenPt  = genpt < minpt,
                     HighGenPt = genpt >= maxpt,
                     HighGenY  =  geny >= maxy,
                     HighGenN  =  genN >  maxN;
                bool goodGen = (!LowGenPt) && (!HighGenPt) && (!HighGenY) && (!HighGenN);
                
                auto igenbin = genBinning->GetGlobalBinNumber(genpt,genN);
                if (goodGen) assert(igenbin > 0);

                RecJet recJet = matching.HighestPt(genJet);
                bool noMatch = recJet.p4.Pt() > 13e3 /* 13 TeV */;

                auto evWgt = ev->weights.at(v.iEvtWgt);

                if (goodGen) v.gen->Fill(igenbin, evWgt);

                if (noMatch) {
                    if (goodGen) v.missNoMatch->Fill(igenbin, evWgt);
                    continue;
                }

                auto jWgt = recJet.weights.at(v.iJetWgt); 

                auto recpt = recJet.CorrPt(v.iJEC),
                     recy  = abs(recJet.Rapidity());

                int recN = 0;
                for (size_t iRec = 0; iRec < recJets->size(); ++iRec) {
                    auto testpt = recJets->at(iRec).CorrPt(v.iJEC);
                    if (abs(recpt - testpt) < feps) {
                        recN = iRec+1;
                        break;
                    }
                }
                assert(recN != 0);

                bool LowRecPt  = recpt < minpt,
                     HighRecPt = recpt >= maxpt/*s.at(recybin)*/,
                     HighRecY  =  recy >= maxy,
                     HighRecN  =  recN >  maxN;
                bool goodRec = (!LowRecPt) && (!HighRecPt) && (!HighRecY) && (!HighRecN);

                auto irecbin = recBinning->GetGlobalBinNumber(recpt,recN);
                if (goodRec) assert(irecbin > 0);

                if      ( goodRec &&  goodGen) { v.RM         ->Fill(igenbin, irecbin, evWgt *      jWgt );
                                                 v.missNoMatch->Fill(igenbin,          evWgt * (1 - jWgt)); }   // for jet weights != 1 
                else if (!goodRec &&  goodGen) { if (  LowRecPt  && (!HighRecPt) && (!HighRecY) && (!HighRecY)) v.missOutLow ->Fill(igenbin, evWgt       );
                                            else if ((!LowRecPt) &&   HighRecPt  && (!HighRecY) && (!HighRecY)) v.missOutHigh->Fill(igenbin, evWgt       );
                                            else if ((!LowRecPt) && (!HighRecPt) && (!HighRecY) && (!HighRecY)) v.missOutY   ->Fill(igenbin, evWgt       );
                                            else if ((!LowRecPt) && (!HighRecPt) &&   HighRecY  &&   HighRecY ) v.missOutN   ->Fill(igenbin, evWgt       );
                                            else                                                                v.missNoMatch->Fill(igenbin, evWgt       ); }
                else if ( goodRec && !goodGen) { if (  LowGenPt  && (!HighGenPt) && (!HighGenY) && (!HighGenY)) v.fakeOutLow ->Fill(irecbin, evWgt * jWgt);
                                            else if ((!LowGenPt) &&   HighGenPt  && (!HighGenY) && (!HighGenY)) v.fakeOutHigh->Fill(irecbin, evWgt * jWgt);
                                            else if ((!LowGenPt) && (!HighGenPt) &&   HighGenY  && (!HighGenY)) v.fakeOutY   ->Fill(irecbin, evWgt * jWgt);
                                            else if ((!LowGenPt) && (!HighGenPt) && (!HighGenY) &&   HighGenY ) v.fakeOutN   ->Fill(irecbin, evWgt * jWgt); 
                                            else                                                                v.fakeNoMatch->Fill(irecbin, evWgt * jWgt); }
            } // match loop

            for (RecJet& recJet: matching.mcands) {

                auto y  = abs(recJet.Rapidity());
                if (y >= maxy) continue;
                auto pt = recJet.CorrPt(v.iJEC);
                if (pt < minpt || pt >= maxpt) continue;

                int N = 0;
                for (size_t iRec = 0; iRec < recJets->size(); ++iRec) {
                    auto testpt = recJets->at(iRec).CorrPt(v.iJEC);
                    if (abs(pt - testpt) < feps) {
                        N = iRec+1;
                        break;
                    }
                }
                assert(N != 0);

                if (N >= maxN) break;

                auto evWgt = ev->weights.at(v.iEvtWgt);
                auto jWgt = recJet.weights.at(v.iJetWgt);

                auto ibin = recBinning->GetGlobalBinNumber(pt,N);
                assert(ibin > 0);
                v.fakeNoMatch->Fill(ibin, evWgt * jWgt);
            } // fake loop
        } // variation loop
    } // event loop

    for (Variation& v: variations)
        v.Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output model [nSplit [nNow]]\n"
             << "\twhere\tinput = any n-tuple\n"
             << "\t     \toutput = a root file with format compatible with `unfold` executable\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getUnfHistogramsPtN(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
