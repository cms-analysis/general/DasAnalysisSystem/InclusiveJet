#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <numeric>
#include <limits>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toyEigen.h"
#include "Core/CommonTools/interface/toyRoot.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

#include "Rdphi.h"
#include "getsystematics.h"

using namespace std;
using namespace DAS;
using namespace Eigen;

#include "correlations.h"

static const auto deps = numeric_limits<double>::epsilon();

////////////////////////////////////////////////////////////////////////////////
/// RdphiFunc 
///
/// In a given pt bin, get Rdphi from a vector of number of neighbours
class RdphiFunc {

    const vector<vector<int>> Index; // double indexed vector [pt][Nnbr], containing the ids of the nonzero bins
    const size_t nPtBins, //!< nb of nonzero ptbins of 1D distribution, i.e. of Rdphi distribution
                 nTotBins; //!< nb of nonzero bins of original 2D distribution, i.e. in (pt,Nnbr) distribution

    static vector<vector<int>> GetMaxNnbrs (TH1 * h, TUnfoldBinning * binning)
    {
        vector<vector<int>> vv;
        for (size_t ipt = 0; ipt < Rdphi::nGenBins; ++ipt) {
            double pt = Rdphi::genBins.at(ipt)+1;

            // 1) get the max number of neighbours in that particular pt bin
            double maxNnbr = Rdphi::maxN-1;
            for (int Nnbr = 0; Nnbr <= Rdphi::maxN; ++Nnbr) {
                int i = binning->GetGlobalBinNumber(pt, Nnbr);
                auto element = h->GetBinContent(i);
                //cout << pt << ' ' << Nnbr << ' ' << i << ' ' << element << endl;
                if (abs(element) < numeric_limits<double>::epsilon()) {
                    maxNnbr = Nnbr;
                    break;
                }
            }

            if (maxNnbr < 1) {
                cout << "First empty pt bin = " << pt << "\nBreaking here." << endl;
                break;
            }

            // 2) get the list of indices for the different numbers of neighbours
            vector<int> v;
            for (int Nnbr=0; Nnbr <= maxNnbr; ++Nnbr) {
                int i = binning->GetGlobalBinNumber(pt, Nnbr);
                v.push_back(i);
            }
            vv.push_back(v);
        }
        return vv;
    }

public:

    RdphiFunc (TH1 * h, TUnfoldBinning * binning) :
        Index(GetMaxNnbrs(h, binning)),
        nPtBins(Index.size()),
        nTotBins(accumulate(Index.begin(), Index.end(), 0, [](double sum, vector<int> v) { return sum += v.size(); }))
    {
        cout << "Max number of neighbours per pt bin:\n";
        for (auto& v: Index)
            cout << v.size() << ' ';
        cout << '\t' << nTotBins << endl;
    }

    pair<TH1 *, TH2 *> RemoveEmptyBins (TH1 * h, TH2 * cov) const
    {
        TString suffix = "_nz"; // nz = no zero
        TH1 * h_nz   = new TH1D(h  ->GetName() + suffix, h->GetTitle(), nTotBins, 0.5, nTotBins+0.5);
        TH2 * cov_nz = new TH2D(cov->GetName() + suffix, h->GetTitle(), nTotBins, 0.5, nTotBins+0.5,
                                                                        nTotBins, 0.5, nTotBins+0.5);

        // fill "non-zero" histogram & cov matrix according to the values stored in Index 
        for (size_t pt = 0, inew = 1; pt < this->nPtBins; ++pt) 
        for (size_t Nnbr = 0; Nnbr < Index.at(pt).size(); ++Nnbr, ++inew) {
            assert(inew <= nTotBins);

            // input distribution
            int iold = Index.at(pt).at(Nnbr);
            double content = h->GetBinContent(iold); assert(abs(content) > 0); // sanity check
            h_nz->SetBinContent(inew, content);

            // covariance matrix
            for (size_t pt2 = 0, inew2 = 1; pt2 < this->nPtBins; ++pt2) 
            for (size_t Nnbr2 = 0; Nnbr2 < Index.at(pt2).size(); ++Nnbr2, ++inew2) {
                assert(inew2 <= nTotBins);

                int iold2 = Index.at(pt2).at(Nnbr2);
                double content2 = cov->GetBinContent(iold, iold2);
                cov_nz->SetBinContent(inew, inew2, content2);
            }
        }
        return {h_nz,cov_nz};
    }

    TVectorD operator() (const TVectorD& x)
    {
        //cout << x.GetNrows()  << ' ' << nTotBins << endl;
        assert(x.GetNrows() == static_cast<int>(nTotBins));

        TVectorD rdphi(nPtBins), denominator(nPtBins);
        for (size_t pt = 0, I = 0; pt < this->nPtBins; I += Index.at(pt).size(), ++pt) {
            double numerator = 0, denominator = 0;
            for (size_t Nnbr = 0, i = I; Nnbr < Index.at(pt).size(); ++Nnbr, ++i) {
                double element = x(i);
                numerator += Nnbr*element;
                denominator += element;
            }
            if (denominator != 0)
                rdphi(pt) = numerator/denominator;
        }

        return rdphi;
    }

    VectorXd operator() (const VectorXd& x)
    {
        assert(x.size() == static_cast<int>(nTotBins));

        VectorXd rdphi(nPtBins), denominator(nPtBins);
        for (size_t pt = 0, I = 0; pt < this->nPtBins; I += Index.at(pt).size(), ++pt) {
            double numerator = 0, denominator = 0;
            for (size_t Nnbr = 0, i = I; Nnbr < Index.at(pt).size(); ++Nnbr, ++i) {
                double element = x(i);
                numerator += Nnbr*element;
                denominator += element;
            }
            if (denominator != 0)
                rdphi(pt) = numerator/denominator;
        }

        return rdphi;
    }
};

TH1 * NaiveApproach (TH1 * hIn, TUnfoldBinning * binning)
{
    TString name = hIn->GetName();
    TH2 * hIn2 = dynamic_cast<TH2*>(binning->ExtractHistogram(name + "2", hIn));
    TH1 * hOut = hIn2->ProjectionX(name + "naive", 0, -1);
    hOut->Reset();
    for (int ipt = 1; ipt <= hOut->GetNbinsX(); ++ipt) {
        double numerator = 0, denominator = 0;
        for (int iNnbr = 1; iNnbr <= hIn2->GetNbinsY(); ++iNnbr) {
            double element = hIn2->GetBinContent(ipt, iNnbr);
            int Nnbr = iNnbr - 1;
            numerator += Nnbr*element;
            denominator += element;
        }
        if (denominator != 0)
            hOut->SetBinContent(ipt, numerator/denominator);
    }
    return hOut;
}

////////////////////////////////////////////////////////////////////////////////
/// getRdphiRatio
///
/// Takes the output of the unfolding to calculate the Rdphi observable.
/// The output file keeps (as much as possible) the same structure as the input.
void getRdphiRatio 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              const long N)   //!< number of entries
{
    InitBinning();
    ToyEigen::verbose = false;
    ToyRoot::verbose = false;

    TFile * source = TFile::Open(input, "READ");

    vector<TH1 *> nom { dynamic_cast<TH1*>(source->Get("nominal")) };
    nom.front()->SetName("nominal_hIn");
    nom.front()->SetDirectory(0);
    nom.push_back( dynamic_cast<TH1*>(nom.front()->Clone("nominalTotal")) );
    nom.back()->SetName("nominalTotal_hIn");
    nom.back()->SetDirectory(0);
    TH2 * covIn  = dynamic_cast<TH2*>(source->Get("cov"));
    TH2 * covTotalIn  = dynamic_cast<TH2*>(source->Get("covTotal"));
    covIn->SetName("covIn");
    covTotalIn->SetName("covTotalIn");
    covIn->SetDirectory(0);
    covTotalIn->SetDirectory(0);

    TDirectory * dJES = dynamic_cast<TDirectory *>(source->Get("JES")); // variations
    vector<TH1 *> JES = GetSystematics(dJES);

    TDirectory * dMC = dynamic_cast<TDirectory *>(source->Get("MC")); // shifts
    vector<TH1 *> MC = GetSystematics(dMC);
    for (TH1 * hIn: MC) {
        TString name = hIn->GetName();
        if (name.Contains("miss") || name.Contains("fake"))
            hIn->Add(nom.front());
    }
    source->Close();

    TFile * file = TFile::Open(output, "RECREATE");
    dJES = file->mkdir("JES");
    dMC = file->mkdir("MC");

    vector<pair<TDirectory *, vector<TH1*>>> all_shifts;
    all_shifts.push_back({ file, nom });
    all_shifts.push_back({ dJES, JES });
    all_shifts.push_back({ dMC , MC  });

    TH1 * hOutNom = nullptr;
    for (auto& shifts: all_shifts)
    for (TH1 * hIn: shifts.second) {

        cout << hIn << endl;
        TString name = hIn->GetName();

        //TH2 * covInHere = name.Contains("nominalTotal") ? covIn : covTotalIn; // TODO?

        //if (s == "nominal") { // TODO
        //    TH1 * hRecIn   = dynamic_cast<TH1*>(source->Get("rec"));
        //    TH1 * naiveRec = NaiveApproach(hRecIn, recBinning);
        //    naiveRec->Write("recNaive");
        //    TH1 * naive = NaiveApproach(hIn, genBinning);
        //    naive->Write("genNaive");
        //}

        cout << __LINE__ << "\tDeclaring RpdphiFunc functor" << endl;
        RdphiFunc rdphifunc(hIn, genBinning);

        cout << __LINE__ << "\tRemoving empty bins (if any)" << endl;
        tie(hIn, covIn) = rdphifunc.RemoveEmptyBins(hIn, covTotalIn); // TODO: check "Total"

        cout << __LINE__ << '\t' << "\e[1m" << name << ' ' << hIn << ' ' << covIn << "\e[0m" << endl;

        TDirectory * dir = shifts.first;
        dir->cd();

        cout << __LINE__ << "\tDeclaring toy" << endl;
        ToyRoot toy(hIn, covIn, rdphifunc, 42 /* seed */);

        cout << __LINE__ << "\tPlaying" << endl;
        for (long i = 0; i < N; ++i)
            toy.play();

        cout << __LINE__ << "\tGetting output" << endl;
        name.ReplaceAll("_hIn", "_hOut");
        TH1 *   hOut = new TH1D(name, name, DAS::Rdphi::nGenBins, DAS::Rdphi::genBins.data());
        name.ReplaceAll("_hOut", "_covOut");
        TH2 * covOut = new TH2D(name, name, DAS::Rdphi::nGenBins, DAS::Rdphi::genBins.data(),
                                            DAS::Rdphi::nGenBins, DAS::Rdphi::genBins.data());
        name.ReplaceAll("_covOut", "");
        tie(hOut, covOut) = toy.buy(hOut, covOut);

        cout << __LINE__ << "\tChecking that cov matrix is symmetric" << endl;
        for (int i = 1; i <= hOut->GetNbinsX(); ++i)
        for (int j = 1; j <= hOut->GetNbinsX(); ++j) {
            double normalisation = hOut->GetBinContent(i) * hOut->GetBinContent(j);
            if (abs(normalisation) < deps) continue;
            double a_ij = covOut->GetBinContent(i,j) / normalisation,
                   a_ji = covOut->GetBinContent(j,i) / normalisation;
            if (abs(a_ij-a_ji) > deps)
                cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: asymmetry found for i = " << i << ", j = " << j << ", getting a_ij = " << a_ij << " and a_ji = " << a_ji << "\x1B[30m\e[0m\n";
        }

        cout << __LINE__ << "\tLooking for abnormal entries" << endl;
        for (int i = 1; i <= hOut->GetNbinsX(); ++i) {
            double content = hOut->GetBinContent(i);
            if (isnormal(content)) continue;
            cerr << __LINE__ << "\t\x1B[31m\e[1mWarning: bin " << i << " has abnormal content: " << content << "\x1B[30m\e[0m\n";
            hOut->SetBinContent(i, 0);
            hOut->SetBinError  (i, 0);
            for (int j = 0; j <= hOut->GetNbinsX(); ++j) {
                covOut->SetBinContent(i,j,0);
                covOut->SetBinContent(j,i,0);
            }
        }

        //if (shifts.first == dMC) {
        //    cout << __LINE__ << "\tIf MC, subtracting nominal value" << endl;
        //    assert(hOutNom != nullptr);
        //    hOut->Add(hOutNom, -1);
        //}

        cout << __LINE__ << "\tWriting output" << endl;
        hOut->SetDirectory(dir);
        //shifts.first->ls();
        hOut->Write(name);
        if (name == "nominal") {
            cout << __LINE__ << "\tWriting covariance" << endl;
            hOutNom = dynamic_cast<TH1*>(hOut->Clone("clone"));
            hOutNom->SetDirectory(0);
            covOut->SetDirectory(dir);
            covOut->Write("cov");
            SaveCorr(covOut, "corr", "correlations at hadron level (data only)", file);
        }
        else if (name == "nominalTotal") {
            cout << __LINE__ << "\tWriting total covariance" << endl;
            covOut->SetDirectory(dir);
            covOut->Write("covTotal");
            SaveCorr(covOut, "corrTotal", "correlations at hadron level (data + MC)", file);
        }
    }

    cout << __LINE__ << "\tClosing" << endl;
    file->Close();
    cout << __LINE__ << "\tDone" << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output N" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    long N = atol(argv[3]);

    getRdphiRatio(input, output, N);
    return EXIT_SUCCESS;
}
#endif
