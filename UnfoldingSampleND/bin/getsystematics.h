#include <vector>

#include <TH1.h>
#include <TDirectory.h>
#include <TList.h>
#include <TKey.h>

using std::vector;

vector<TH1 *> GetSystematics (TDirectory * dir, const char * suffix = "_hIn")
{
    dir->cd();
    vector<TH1 *> v;
    TIter Next(dir->GetListOfKeys()) ;
    TKey* key = nullptr;
    while ( (key = dynamic_cast<TKey*>(Next())) ) {
        TH1 * h = dynamic_cast<TH1*>(key->ReadObj());
        if (h == nullptr) continue;
        TString name = h->GetName();
        name.ReplaceAll("Output", "");
        name += suffix;
        h->SetName(name);
        h->SetDirectory(0);
        v.push_back(h);
    }
    return v;
}

