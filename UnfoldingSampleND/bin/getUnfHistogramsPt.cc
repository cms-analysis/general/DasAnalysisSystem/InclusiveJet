#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/JEC/interface/JESreader.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include "Core/JEC/bin/matching.h"

#include "model.h"

using namespace std;
using namespace DAS;

static const double minpt = recBins.front(), maxpt = recBins.back(), maxy = y_edges.back();

struct Variation {
    const TString name;
    static bool isMC;
    const size_t iJEC, iJetWgt, iEvtWgt;
    TH1 * rec, * tmp, * gen,
        * missNoMatch, * missOutY, * missOutLow, * missOutHigh,
        * fakeNoMatch, * fakeOutY, * fakeOutLow, * fakeOutHigh;    
    TH2 * cov, * RM;

    Variation (TString Name, size_t IJEC = 0 , size_t IJetWgt = 0 , size_t IEvtWgt = 0) :
        name(Name), iJEC(IJEC), iJetWgt(IJetWgt), iEvtWgt(IEvtWgt),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatch(nullptr), missOutY(nullptr), missOutLow(nullptr), missOutHigh(nullptr),
        fakeNoMatch(nullptr), fakeOutY(nullptr), fakeOutLow(nullptr), fakeOutHigh(nullptr),
        cov(nullptr), RM(nullptr)
    {
        rec = new TH1D(Name + "rec", "detector level", nRecBins, recBins.data()),
        tmp = new TH1D(Name + "tmp", "", nRecBins, recBins.data());
        cov = new TH2D(Name + "cov", "covariance matrix of detector level", nRecBins, recBins.data(), nRecBins, recBins.data());
        if (!isMC) return;
        gen         = new TH1D(Name + "gen"        , "hadron level", nGenBins, genBins.data()),
        missNoMatch = new TH1D(Name + "missNoMatch", "real inefficiency"                                               , nGenBins, genBins.data()),
        missOutY    = new TH1D(Name + "missOutY"   , "good gen jets matched to jets reconstructed at too high rapidity", nGenBins, genBins.data()),
        missOutHigh = new TH1D(Name + "missOutHigh", "good gen jets matched to jets reconstructed at too high pt"      , nGenBins, genBins.data()),
        missOutLow  = new TH1D(Name + "missOutLow" , "good gen jets matched to jets reconstructed at too low pt"       , nGenBins, genBins.data()),
        fakeNoMatch = new TH1D(Name + "fakeNoMatch", "real background"                                             , nRecBins, recBins.data()),
        fakeOutY    = new TH1D(Name + "fakeOutY"   , "good rec jets matched to jets generated at too high rapidity", nRecBins, recBins.data()),
        fakeOutHigh = new TH1D(Name + "fakeOutHigh", "good rec jets matched to jets generated at too high pt"      , nRecBins, recBins.data()),
        fakeOutLow  = new TH1D(Name + "fakeOutLow" , "good rec jets matched to jets generated at too low pt"       , nRecBins, recBins.data());
        RM          = new TH2D(Name + "RM", "migrations of jets in phase space at both levels", nGenBins, genBins.data(), nRecBins, recBins.data());
    }

    void Write (TFile * file)
    {
        cout << "Writing " << name << endl;
        file->cd();
        TDirectory * dir = file->mkdir(name);
        dir->cd();
#define PAIR(name) make_pair(dynamic_cast<TH1*>(name), #name)
        for (auto p: { PAIR(gen), PAIR(missNoMatch), PAIR(missOutY), PAIR(missOutHigh), PAIR(missOutLow),
                       PAIR(rec), PAIR(fakeNoMatch), PAIR(fakeOutY), PAIR(fakeOutHigh), PAIR(fakeOutLow),
                       PAIR(cov), PAIR(RM) }) {
#undef PAIR
            if (p.first == nullptr) continue;
            p.first->SetDirectory(dir);
            p.first->Write(p.second);
        }
    }
};
bool Variation::isMC = false; // just fort initialisation
////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding only for pt (in d=1), 
/// without using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHistogramsPt
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    cout << "Setting up branches" << endl;

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    TTreeReader reader(tree);
    TTreeReaderValue<Event> ev = {reader, "event"};
    TTreeReaderValue<vector<RecJet>> recjets = {reader, "recJets"};
    TTreeReaderValue<vector<GenJet>> * genjets = nullptr;
    if (isMC)
        genjets = new TTreeReaderValue<vector<GenJet>>({reader, "genJets"}); 

    TFile * file = TFile::Open(output, "RECREATE");

    cout << "Setting variations" << endl;

    Variation::isMC = isMC;
    vector<Variation> variations { Variation("nominal") };
    if (isMC) {
        variations.push_back( Variation("JERup"  , 1, 0, 0) );
        variations.push_back( Variation("JERdown", 2, 0, 0) );
        variations.push_back( Variation("PUup"  , 0, 0, 1) );
        variations.push_back( Variation("PUdown", 0, 0, 2) );
        variations.push_back( Variation("Prefup"  , 0, 1, 0) );
        variations.push_back( Variation("Prefdown", 0, 2, 0) );
        variations.push_back( Variation("Modelup"  , 0, 0, 3) );
        variations.push_back( Variation("Modeldown", 0, 0, 4) );
    }
    else {
        int i = 1;
        for (TString& JES: JESreader::getJECUncNames()) {
            variations.push_back( Variation(JES + "up"  , i  , 0, 0) );
            variations.push_back( Variation(JES + "down", i+1, 0, 0) );
            i += 2;
        }
    }

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

        for (Variation& v: variations) {

            v.tmp->Reset();

            auto evWgt = ev->weights.at(v.iEvtWgt);

            vector<int> binIDs; // we save the indices of the filled bins in order to avoid many multiplications by 0
            for (RecJet& recjet: *recjets) {
                auto y  = recjet.AbsRap();
                if (y >= maxy) continue;
                auto pt = recjet.CorrPt(v.iJEC);
                if (pt < minpt || pt > maxpt) continue;

                auto irecbin = v.rec->FindBin(pt);
                if (find(binIDs.begin(), binIDs.end(), irecbin) == binIDs.end()) binIDs.push_back(irecbin);

                auto jWgt = recjet.weights.at(v.iJetWgt);

                v.rec->Fill(pt, evWgt * jWgt);
                v.tmp->Fill(pt, evWgt * jWgt); // for cov
            }

            for (auto x: binIDs) for (auto y: binIDs) {
                double cCov = v.cov->GetBinContent(x,y),
                       cTmp = v.tmp->GetBinContent(x)*v.tmp->GetBinContent(y);
                v.cov->SetBinContent(x,y,cCov+cTmp);
            }
        }

        if (!isMC) continue;

        Matching<RecJet, GenJet> matching(*recjets);
        for (GenJet& genjet: **genjets) {
            auto genpt = genjet.p4.Pt(),
                 geny = abs(genjet.Rapidity());

            bool LowGenPt  = genpt < minpt,
                 HighGenPt = genpt >= maxpt,
                 HighGenY  =  geny >= maxy;
            bool goodGen = (!LowGenPt) && (!HighGenPt) && (!HighGenY);

            RecJet recjet = matching.HighestPt(genjet);
            bool noMatch = recjet.p4.Pt() > 13e3 /* 13 TeV */;

            for (Variation& v: variations) {

                auto evWgt = ev->weights.at(v.iEvtWgt);

                if (goodGen) v.gen->Fill(genpt, evWgt);

                if (noMatch) {
                    if (goodGen) v.missNoMatch->Fill(genpt, evWgt);
                    continue;
                }

                auto jWgt = recjet.weights.at(v.iJetWgt);

                auto recpt = recjet.CorrPt(v.iJEC),
                     recy  = abs(recjet.Rapidity());

                bool LowRecPt  = recpt < minpt,
                     HighRecPt = recpt >= maxpt,
                     HighRecY  =  recy >= maxy;
                bool goodRec = (!LowRecPt) && (!HighRecPt) && (!HighRecY);

                if      ( goodRec &&  goodGen) { v.RM         ->Fill(genpt, recpt, evWgt *      jWgt );
                                                 v.missNoMatch->Fill(genpt,        evWgt * (1 - jWgt)); } // for jet weights != 1 
                else if (!goodRec &&  goodGen) { if (  LowRecPt  && (!HighRecPt) && (!HighRecY)) v.missOutLow ->Fill(genpt, evWgt       );
                                            else if ((!LowRecPt) &&   HighRecPt  && (!HighRecY)) v.missOutHigh->Fill(genpt, evWgt       );
                                            else if ((!LowRecPt) && (!HighRecPt) &&   HighRecY ) v.missOutY   ->Fill(genpt, evWgt       );
                                            else                                                 v.missNoMatch->Fill(genpt, evWgt       ); }
                else if ( goodRec && !goodGen) { if (  LowGenPt  && (!HighGenPt) && (!HighGenY)) v.fakeOutLow ->Fill(recpt, evWgt * jWgt);
                                            else if ((!LowGenPt) &&   HighGenPt  && (!HighGenY)) v.fakeOutHigh->Fill(recpt, evWgt * jWgt);
                                            else if ((!LowGenPt) && (!HighGenPt) &&   HighGenY ) v.fakeOutY   ->Fill(recpt, evWgt * jWgt);  
                                            else                                                 v.fakeNoMatch->Fill(recpt, evWgt * jWgt); }
            }
            // TODO: try to use the underflow and overflow for matching out of the phase space...
        }

        for (RecJet& recjet: matching.mcands) {
            for (Variation& v: variations) {

                double y  = abs(recjet.Rapidity());
                if (y >= maxy) continue;
                double pt = recjet.CorrPt(v.iJEC);
                if (pt < minpt || pt > maxpt) continue;

                auto evWgt = ev->weights.at(v.iEvtWgt);
                auto jWgt = recjet.weights.at(v.iJetWgt);

                v.fakeNoMatch->Fill(pt, evWgt * jWgt);
            }
        }
    }

    for (Variation& v: variations)
        v.Write(file);

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = any n-tuple\n"
             << "\t     \toutput = a root file with format compatible with `unfold` executable" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getUnfHistogramsPt(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
