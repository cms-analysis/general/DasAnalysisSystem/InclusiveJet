#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TKey.h>

#include "Math/VectorUtil.h"

#include "PtY.h"

using namespace std;
using namespace DAS;
using namespace DAS::InclusiveJet;

////////////////////////////////////////////////////////////////////////////////
/// getLmatrixPtY
///
/// Prepare bias and L-matrix for regularisation.
///
/// TODO: use smooth fit of unregularised unfolding as bias?
void getLmatrixPtY
             (TString input,  //!< name of input root file (MC after `getUnfHistogramsPtY`)
              TString output, //!< name of output root file (one of the optional inputs of `unfold`)
              float fb = 0.)  //!< bias factor
{
    InitBinning();

    TFile * source = TFile::Open(input, "READ");
    TFile * file = TFile::Open(output, "RECREATE");

    TIter Next(source->GetListOfKeys()) ;
    TKey* key = nullptr;
    while ( (key = dynamic_cast<TKey*>(Next())) ) {

        source->cd();
        auto dirIn = dynamic_cast<TDirectory*>(key->ReadObj());
        TString var = dirIn->GetName(); // data variation name
        cerr << "\e[1m" << var << "\e[0m\n";
        dirIn->cd();

        TH2 * RM = dynamic_cast<TH2*>(dirIn->Get("RM"));
        assert(RM != nullptr);
        RM->SetDirectory(0);

        file->cd();
        auto dirOut = file->mkdir(var);
        dirOut->cd();

        // bias
        auto bias = RM->ProjectionX("bias", 0, -1); // this is like "gen - miss"
        bias->SetTitle("f_{b} * #bf{x_{0}}");
        bias->Print("all");

        // L-matrix
        TH2 * Lmatrix = TUnfoldBinning::CreateHistogramOfMigrations(genBinning, genBinning, "L");
        // NOTE:
        //  - the x-axis MUST correspond to the axis of the TUnfoldBinning binning
        //  - the y-axis only happens to correspond to it as well, but primarily corresponds 
        //    to the number of conditions, which is *arbitrary*

        for (int iy = 1; iy <= nYbins; ++iy)
        for (int ipt = 1; ipt <= nGenBins; ++ipt) {
            double y = (y_edges.at(iy-1) + y_edges.at(iy)) / 2,
                   pt = (genBins.at(ipt-1) + genBins.at(ipt)) / 2;
            int i = genBinning->GetGlobalBinNumber(pt, y);

            // b(in)
            int                 bUp     = i-nGenBins,
                 bLeft   = i-1, bCenter = i         , bRight  = i+1,
                                bDown   = i+nGenBins;

            // values (curvature regularisation)
            auto get = [bias](int i) {
                double content = bias->GetBinContent(i);
                assert(content >= 0);
                return content > 0 ? 1./content : 0;
            };
            double cUp     = get(bUp   ),
                   cLeft   = get(bLeft ),
                   cRight  = get(bRight),
                   cDown   = get(bDown );

            cout << setw(3) << iy << setw(3) << ipt
                 << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
                 << setw(5) << bUp     << setw(15) << -cUp                   
                 << setw(5) << bLeft   << setw(15) <<     -cLeft             
                 << setw(5) << bRight  << setw(15) <<           -cRight      
                 << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

            // filling L-matrix
                            Lmatrix->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
            if (cUp    > 0) Lmatrix->SetBinContent(i, bUp    , -cUp                    );
            if (cLeft  > 0) Lmatrix->SetBinContent(i, bLeft  ,     -cLeft              );
            if (cRight > 0) Lmatrix->SetBinContent(i, bRight ,           -cRight       );
            if (cDown  > 0) Lmatrix->SetBinContent(i, bDown  ,                  -cDown );
        }
        cout << flush;
        Lmatrix->SetDirectory(dirOut);
        Lmatrix->Write();

        bias->Scale(fb);
        bias->SetDirectory(dirOut);
        bias->Write();
    }

    source->Close();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [fb]\n"
             << "\twhere\tinput = MC file from `getUnfHistogramsPtY`\n"
             << "\t     \toutput = root file to be used as last argument of `unfold`\n"
             << "\t     \tfb = strength of the regularisation (see `TUnfold` documentation)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    float fb = argc > 3 ? atof(argv[3]) : 0.;

    getLmatrixPtY(input, output, fb);
    return EXIT_SUCCESS;
}
#endif

