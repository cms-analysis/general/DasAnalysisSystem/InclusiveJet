#include "Core/CommonTools/interface/variables.h"

using namespace DAS;

static const double minpt = 74, maxpt = 3103, maxy = 2.5;
static TUnfoldBinning * recBinning = new TUnfoldBinning("recBinning");
static TUnfoldBinning * genBinning = new TUnfoldBinning("genBinning");

void InitBinning () 
{
    assert(recBins[2] == minpt);
    assert(recBins[nRecBins-2] == maxpt);

    const int N = nRecBins-4;

    recBinning->AddAxis("recpt",     N,   &recBins[2],false,false);
    recBinning->AddAxis("recy" ,nYbins,y_edges.data(),false,false);
    genBinning->AddAxis("genpt",   N/2,   &genBins[1],false,false);
    genBinning->AddAxis("geny" ,nYbins,y_edges.data(),false,false);
}
