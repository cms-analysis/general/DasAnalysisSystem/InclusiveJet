#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TF2.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Greta.h"

#include "Core/JEC/bin/common.h"
#include "Core/JEC/interface/resolution.h"
#include "Core/JEC/interface/DoubleCrystalBall.h"

//#include "InclusiveJet/UnfoldingToy2D/interface/Spectrum.h"

using namespace std;
using namespace DAS;

typedef ThunbergDoubleCrystalBall<nmu, nkL, nnL, nkR, nnR, DoubleCrystalBall::Integral> Resolution2D;

////////////////////////////////////////////////////////////////////////////////
/// Response functor
///
/// It just mimicks the behaviour of a response matrix.
/// Its purpose is for calculations only: it does not care where the parameters come from
///
/// Notes:
///  - it does not include any weight for the rapidity migrations (TODO? probably yes)
///  - it does not include any factor for the background (TODO? probably not)
/// ATM, there is no explicit dependence on yrec, and only implicit dependence on ygen
struct Response {

    Thunberg<nN, log, exp> gen;
    Thunberg<4, log, exp> missOut, missNoMatch;
    Resolution2D resolution2D;
    const size_t igen, imissOut, imissNoMatch, iresolution2D, N;

    Response (pair<double,double> genRange,
              pair<double,double> missOutRange,
              pair<double,double> missNoMatchRange,
              pair<double,double> resolution2DRange) :
        gen         (genRange         .first, genRange         .second),
        missOut     (missOutRange     .first, missOutRange     .second),
        missNoMatch (missNoMatchRange .first, missNoMatchRange .second),
        resolution2D(resolution2DRange.first, resolution2DRange.second),
        igen(2),
        imissOut(igen+gen.npars),
        imissNoMatch(imissOut+missOut.npars), 
        iresolution2D(imissNoMatch+missNoMatch.npars),
        N(iresolution2D + resolution2D.N)
    { }

    double operator() (double * x, double * p)
    {
        //cout << "------" << endl;
        double genpt = x[0], recptUp = p[0], recptDn = p[1];
        //cout << "pt: " << genpt << ' ' << recptUp << ' ' << recptDn << '\n';
        double deltaUp = (recptUp-genpt)/genpt,
               deltaDn = (recptDn-genpt)/genpt;
        //cout << "delta: " << deltaUp << ' ' << deltaDn << '\n';
        if (abs(deltaUp) > w || abs(deltaDn) > w) return 0.;
        // intf is defined from -0.8 to 0.8;

        double yUp[] = {genpt, deltaUp},
               yDn[] = {genpt, deltaDn};
        double CBup = resolution2D(yUp, &p[iresolution2D]),
               CBdn = resolution2D(yDn, &p[iresolution2D]);
        //cout << "CB: " << CBup << ' ' << CBdn << '\n';
        //if (CBup < CBdn) return 0; // TODO ?
        assert(CBup >= CBdn);

        double fGen         = gen        (&genpt, &p[igen        ]),
               fMissOut     = missOut    (&genpt, &p[imissOut    ]),
               fMissNoMatch = missNoMatch(&genpt, &p[imissNoMatch]);
        ////cout << "f: " << fGen << ' ' << fMissOut << ' ' << fMissNoMatch << '\n';
        fMissOut = min(1.,max(0., fMissOut)); // TODO ?
        assert(fMissOut > 0 && fMissOut < 1);
        //fMissNoMatch = min(1.,max(0., fMissNoMatch)); // TODO ?
        assert(fMissNoMatch > 0 && fMissNoMatch < 1);

        double result = (1-fMissOut-fMissNoMatch)*fGen*(CBup-CBdn);
        //cout << "result: " << result << '\n';
        return result;
    }
};

//typedef function<double(double,int)> Correction;

struct SpectrumFunc {
    vector<TF1 *> fs;

    SpectrumFunc (TFile * file, function<const char*(int)> path, double scale = 1.) :
        fs(nYbins, nullptr), s(scale)
    {
        cout << "Loading smooth spectrum from " << file->GetName() << endl;
        for (int iy = 1; iy <= nYbins; ++iy) {
            TString p = path(iy);
            cout << p << endl;
            fs[iy-1] = dynamic_cast<TF1 *>(file->Get(p));
            assert(fs[iy-1] != nullptr);
        }
    }

    double operator() (double pt, int iy)
    {
        assert(iy > 0 && iy <= nYbins);
        //if (fs[iy-1] == nullptr) return 0; // TODO: remove!!
        return s*fs[iy-1]->Eval(pt);
    }
};

struct CorrectionFunc {
    vector<TF1 *> fs;
    double s;

    CorrectionFunc (TFile * file, function<const char*(int)> path, double scale = 1.) :
        fs(nYbins, nullptr), s(scale)
    {
        cout << "New correction (function) from " << file->GetName() << endl;
        for (int iy = 1; iy <= nYbins; ++iy) {
            TString p = path(iy);
            cout << p << endl;
            fs[iy-1] = dynamic_cast<TF1 *>(file->Get(p));
            assert(fs[iy-1] != nullptr);
        }
    }

    double operator() (double pt, int iy)
    {
        assert(iy > 0 && iy <= nYbins);
        //if (fs[iy-1] == nullptr) return 0; // TODO: remove!!
        return s*fs[iy-1]->Eval(pt);
    }
};

struct CorrectionHist {
    vector<TH1 *> hs;

    CorrectionHist (TFile * file, function<const char*(int)> path, double scale = 1.) :
        hs(nYbins, nullptr)
    { 
        cout << "New correction (histogram)" << endl;
        for (int iy = 1; iy <= nYbins; ++iy) {
            TString p = path(iy);
            cout << p << endl;
            hs[iy-1] = dynamic_cast<TH1 *>(file->Get(path(iy)));
            hs[iy-1]->Scale(scale);
            hs[iy-1]->SetDirectory(0);
            assert(hs[iy-1] != nullptr);
        }
    }

    double operator() (double pt, int iy)
    {
        assert(iy > 0 && iy <= nYbins);
        int ipt = hs[iy-1]->GetXaxis()->FindBin(pt);
        return hs[iy-1]->GetBinContent(ipt);
    }

    double operator() (int ipt, int iy)
    {
        assert(iy > 0 && iy <= nYbins);
        return hs[iy-1]->GetBinContent(ipt);
    }
};


////////////////////////////////////////////////////////////////////////////////
/// Toy functor
///
/// This functor handles several `Response` functors to simulate the migrations
/// among pt & y bins, according to variations or hyper-parameters
template<typename GenSpectrum> struct Toy {

    // this contains 5 rap bins at gen level
    vector<TF1 *> fs;

    TH3 * RMy;

    // 5 rap bins at rec level
    vector<TH1 *> hs;

    // 5x5 RMs
    vector<vector<TH2 *>> RMs;

    // use the objects directly rather than TF1s
    // we have 5 rap bins at gen level
    vector<TH1 *> parameters;
    TH1 * minima, * maxima;

    //Toy (GenSpectrum& genlevel,
    Toy (GenSpectrum& genlevel,
            TFile * file,
            const char * variation) :
        genSpectrum(genlevel),
        RMy(dynamic_cast<TH3*>(file->Get(Form("%s/RMy", variation)))),
        hs(nYbins), RMs(nYbins, vector<TH2*>(nYbins, nullptr)),
        minima(dynamic_cast<TH1 *>(file->Get(Form("%s/minima", variation)))),
        maxima(dynamic_cast<TH1 *>(file->Get(Form("%s/maxima", variation))))
    {
        for (int igeny = 1; igeny <= nYbins; ++igeny) {

            TH1 * parameter = dynamic_cast<TH1 *>(file->Get(Form("%s/ybin%d/hParams", variation, igeny)));
            cout << igeny << '\t' << parameter << endl;
            parameters.push_back(parameter);
        }
    }

    Spectrum operator() (CorrectionFunc& cMissOut, CorrectionFunc& cMissNoMatch,
                         CorrectionHist& cFakeOut, CorrectionHist& cFakeNoMatch)
    {
        cout << "Smearing" << endl;

        for (int igeny = 1; igeny <= nYbins; ++igeny) {

            double m = minima->GetBinContent(igeny),
                   M = maxima->GetBinContent(igeny);

            //double m = genSpectrum.ptRanges[igeny-1].first,
            //       M = genSpectrum.ptRanges[igeny-1].second;
            //cout << m << ' ' << M << endl;

            TString suffix = Form("_ybin%d", igeny);

            TH1 * h = new TH1D("toy" + suffix, yBins[igeny-1], nRecBins, recBins.data()); // here, igeny actually stands for irecy...

            TF1 * gen         = genSpectrum .fs[igeny-1],
                * missOut     = cMissOut    .fs[igeny-1],
                * missNoMatch = cMissNoMatch.fs[igeny-1];
            TH1 * parameter = parameters[igeny-1];

            pair<double,double> genRange,
                                missOutRange,
                                missNoMatchRange,
                                resolution2DRange;

            gen        ->GetRange(genRange        .first, genRange        .second);
            missOut    ->GetRange(missOutRange    .first, missOutRange    .second);
            missNoMatch->GetRange(missNoMatchRange.first, missNoMatchRange.second);
            resolution2DRange = {m, M};

            vector<double> p(2,0); // the two first elements are kept for the rec pt edges
            for (int i = 0; i < gen         ->GetNpar(); ++i) p.push_back(gen         ->GetParameter(i));
            for (int i = 0; i < missOut     ->GetNpar(); ++i) p.push_back(missOut     ->GetParameter(i));
            for (int i = 0; i < missNoMatch ->GetNpar(); ++i) p.push_back(missNoMatch ->GetParameter(i));
            for (int i = 1; i <= parameter->GetNbinsX(); ++i) p.push_back(parameter->GetBinContent(i));

            Response response( genRange, missOutRange, missNoMatchRange, resolution2DRange );
            TF1 * f_response = new TF1("response" + suffix, response, m, M, response.N);
            f_response->SetParameters(p.data());

            for (int irecy = max(igeny-1,1); irecy <= min(igeny+1,nYbins); ++irecy) {

                TH2 * RM = new TH2D("RM" + suffix + Form("_yrecbin%d", irecy), "", nGenBins, genBins.data(), nRecBins, recBins.data());

                TH1 * yWgt = RMy->ProjectionX("RMy"    + suffix, igeny, igeny, irecy,  irecy); // TODO
                yWgt->Divide(RMy->ProjectionX("RMyDen" + suffix, igeny, igeny,     1, nYbins));

                int firstrecpt = RM->GetYaxis()->FindBin(m+deps),
                    lastrecpt  = RM->GetYaxis()->FindBin(M-deps);
                int firstgenpt = RM->GetXaxis()->FindBin(m+deps),
                    lastgenpt  = RM->GetXaxis()->FindBin(M-deps);
                for (int irecpt = firstrecpt; irecpt <= lastrecpt; ++irecpt) {
                    double mr = RM->GetYaxis()->GetBinLowEdge(irecpt);
                    double Mr = RM->GetYaxis()->GetBinUpEdge(irecpt);

                    double content =     h->GetBinContent(irecpt)   ,
                           error2  = pow(h->GetBinError  (irecpt),2);

                    for (int igenpt = firstgenpt; igenpt <= lastgenpt; ++igenpt) {
                        double mg = RM->GetXaxis()->GetBinLowEdge(igenpt);
                        double Mg = RM->GetXaxis()->GetBinUpEdge(igenpt);

                        double err = 0;
                        f_response->SetParameter(0,Mr);
                        f_response->SetParameter(1,mr);
                        double integral = f_response->IntegralOneDim(mg, Mg, feps, feps, err); // TODO: this integral could be done only once instead of two or three times
 
                        double w = yWgt->GetBinContent(igenpt); // TODO: smooth & derive uncertainties
                        //if (integral > 0)
                        //    cout << mr << ' ' << Mr << ' ' << mg << ' ' << Mg << ' ' << w << ' ' << integral << endl;
                        integral *= w;

                        content +=     integral;
                        error2  += pow(integral*err,2);
                        RM->SetBinContent(igenpt, irecpt, integral    );
                        RM->SetBinError  (igenpt, irecpt, integral*err);
                    }

                    double fake = 1.+cFakeOut(irecpt, irecy)+cFakeNoMatch(irecpt, irecy);
                    content *= fake;

                    h->SetBinContent(irecpt,      content );
                    h->SetBinError  (irecpt, sqrt(error2 ));
                }
                RMs[igeny-1][irecy-1] = RM;
            } // loop over irecy
            h->Scale(1,"width");
            hs[igeny-1] = h; // igeny actually stands for irecy

            // normalising // TODO?
            for (int irecy = max(igeny-1,1); irecy <= min(igeny+1,nYbins); ++irecy) {
                TH2 * RM = RMs[igeny-1][irecy-1];
                assert(RM->GetNbinsY() == h->GetNbinsX());
                for (int igenpt = 1; igenpt <= RM->GetNbinsX(); ++igenpt) {
                    double ptmin = RM->GetXaxis()->GetBinLowEdge(igenpt),
                           ptmax = RM->GetXaxis()->GetBinUpEdge(igenpt);
                    double normalisation = genSpectrum.fs[igeny-1]->Integral(ptmin, ptmax);
                    if (normalisation < feps) continue;
                    for (int irecpt = 1; irecpt <= h->GetNbinsX(); ++irecpt) {
                        double content = RM->GetBinContent(igenpt, irecpt),
                               error   = RM->GetBinError  (igenpt, irecpt);
                        content /= normalisation;
                        error   /= normalisation; // is this way correct? shouldn't one use binomial errors? TODO
                        RM->SetBinContent(igenpt, irecpt, content),
                        RM->SetBinError  (igenpt, irecpt, error  );
                    }
                }
            }
        }

        cout << "Toy calculation has finished" << endl;
        Spectrum spectrum(hs);
        return spectrum;
    }

    void Write (TDirectory * d)
    {
        d->cd();
        for (auto& vRM: RMs)
        for (TH2 * RM: vRM) {
            if (RM == nullptr) continue;
            RM->SetDirectory(d);
            RM->Write(); // TODO: name?
        }
    }
};

struct Scenario {
    TString name;
    TString JERvariation;
    double effScale, bckScale;
    // TODO: tail?
    // TODO: rapidity?
};

//struct GenSpectrum {
//    vector<TF1*> fs;
//    vector<double> maxima;
//
//    GenSpectrum (const char * file, vector<double> Ms) :
//        fs(nYbins, nullptr), maxima(Ms)
//    {
//        assert(file != nullptr);
//
//        for (int iy = 1; iy <= nYbins; ++iy) {
//
//            const char * name = Form("nominal/ybin%d/N", iy);
//            cout << name << endl;
//            TF1 * f = dynamic_cast<TF1 *>(file->Get(name));
//            assert(f != nullptr);
//            f->GetRange(ptRanges[iy-1].first, ptRanges[iy-1].second);
//            fs[iy-1] = f;
//        }
//        
//    }
//
//    double operator() ()
//    {
//    }
//};

////////////////////////////////////////////////////////////////////////////////
/// Build toy RM including migrations among rapidity bins
void get2DToyRM 
             (TString input,  //!< name of input root file obtained after `fitJERcurves`
              TString output) //!< name of output root file with RMs
{
    TFile * source = TFile::Open(input, "READ");

    //Spectrum gen(source, "gen");
    SmoothSpectrum<nN> gen(source/*, "gen"*/);

    TFile * file = TFile::Open(output, "RECREATE");
    gen.Write(file, "hGen");
    gen.WriteSmooth(file, "fGen");

            //double m = genSpectrum.ptRanges[igeny-1].first,
            //       M = genSpectrum.ptRanges[igeny-1].second;

    Scenario nominal {"nominal", "nominal", 1, 1},
             JERup {"JERup", "upper", 1, 1},
             JERdn {"JERdn", "lower", 1, 1},
             missUp {"missUp", "nominal", 1.5, 1},
             missDn {"missDn", "nominal", 0.5, 1},
             fakeUp {"fakeUp", "nominal", 1, 1.5},
             fakeDn {"fakeDn", "nominal", 1, 0.5};

    for (const Scenario& scenario: {nominal, JERup, JERdn, missUp, missDn, fakeUp, fakeDn}) {

        cout << "\e[1m" << scenario.name << "\e[0m" << endl;

        file->cd();
        TDirectory * d = file->mkdir(scenario.name);

        CorrectionFunc missNoMatch(source, [&](int iy) { return Form(   "missNoMatch/ybin%d/f",                               iy); }, scenario.effScale ),
                       missOut    (source, [&](int iy) { return Form("%s/missOut/ybin%d/f"    , scenario.JERvariation.Data(), iy); }, scenario.effScale );
                       //fakeNoMatch(source, [&](int iy) { return Form("%s/fakeNoMatch/ybin%d/f", scenario.JERvariation.Data(), iy); }, scenario.bckScale ),
                       //fakeOut    (source, [&](int iy) { return Form("%s/fakeOut/ybin%d/f"    , scenario.JERvariation.Data(), iy); }, scenario.bckScale );

        CorrectionHist //missNoMatch(source, [&](int iy) { return Form(   "missNoMatch/ybin%d/h",                               iy); }, scenario.effScale),
                       //missOut    (source, [&](int iy) { return Form("%s/missOut/ybin%d/h"    , scenario.JERvariation.Data(), iy); }, scenario.effScale ),
                       fakeNoMatch(source, [&](int iy) { return Form("%s/fakeNoMatch/ybin%d/h", scenario.JERvariation.Data(), iy); }, scenario.bckScale ),
                       fakeOut    (source, [&](int iy) { return Form("%s/fakeOut/ybin%d/h"    , scenario.JERvariation.Data(), iy); }, scenario.bckScale );

        Spectrum rec(source, scenario.JERvariation + "/rec");
        rec.Write(d, "hRec");

        Toy<SmoothSpectrum<nN>> toy(gen, source, scenario.JERvariation);
        auto hs = toy(missOut, missNoMatch, fakeOut, fakeNoMatch);

        cout << "Writing" << endl;
        d->cd();
        hs.Write(d, "hRecToy");
        toy.Write(d);
    }
    cout << "Closing" << endl;

    source->cd();
    source->Close();
    file->cd();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    assert(__cplusplus > 201400);
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output variation\n"
             << "\twhere\tinput = output of `fitJERcurves`\n"
             << "\t     \toutput = contains RMs and toy spectra\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    get2DToyRM(input, output);
    return EXIT_SUCCESS;
}
#endif

