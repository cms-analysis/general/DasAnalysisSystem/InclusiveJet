# Unfolding with the toy

## Principles & status of the implementation

- Extract the 2D resolution as a function of the spectrum at hadron level in each rapidity bin from the MC samples (see `Core/JEC/bin/fitJERcurves.cc`).
- Convolve the 2D resolution with (a Chebyshev parameterisation of) the hadron-level spectrum to obtain a Response Matrix and a detector-level spectrum (implemented in `getToyRM.cc`, but solution is not yet satisfactory).
- Fit the Chebyshev parameterisation of the spectrum at hadron level to match the measurement at detector level when smeared (to be implemented in `getToyRM.cc`).

## A few notes

### Observations

- The deviations from a pure Gaussian curve must be taken into account, at least when fitting.
- The mean of the resolution cannot be assumed to be zero (in other words, the RM is not perfectly diagonal).
- The behaviour at low pt is difficult to handle, and it seems related to the very high value for the exponent.

### Claim

The tails must be taken into account in the smearing, especially at |y| > 1.5.

### Disclaimer

Only tested with 2016 data.

### To be investigated

- Impact of the definition of the phase space on the shape of the resolution curve at low pt.
- Retry to change the matching definition (in `Core/JEC/bin/matching.h`) to improve the shape of the resolution at low pt.
- Check with sample without pile-up to see if they can explain these tails.
