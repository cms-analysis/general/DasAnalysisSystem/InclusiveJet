#include <vector>
#include <algorithm>

#include <TH1.h>
#include <TF1.h>
#include <TFile.h>
#include <TDirectory.h>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/SaveParameters.h"

////////////////////////////////////////////////////////////////////////////////
/// Functor to extract the xsect directly from a histogram
///
/// If given from file, the input should be a count.
/// If given inside of the code, the input is not modified.
struct Spectrum {

    vector<TH1 *> hs;
    vector<pair<double, double>> ptRanges;

private:

    static vector<TH1 *> GetHists (TFile * f, const char * name)
    { 
        assert(f != nullptr);
        f->cd();
        TH2 * h2 = dynamic_cast<TH2*>(f->Get(name));
        assert(h2 != nullptr);

        vector<TH1 *> hs(nYbins);
        for (int iy = 1; iy <= nYbins; ++iy) {
            hs[iy-1] = h2->ProjectionX(Form("h_%s_ybin%d", name, iy), iy, iy);
            hs[iy-1]->Scale(1, "width");
            hs[iy-1]->SetDirectory(0);
        }
        return hs;
    }

public:

    Spectrum (vector<TH1*> Hs) :
        hs(Hs), ptRanges(nYbins)
    {
        cout << __func__ << endl;

        for (int iy = 1; iy <= nYbins; ++iy) {

            TH1 * h = hs[iy-1];
            assert(h != nullptr);
            //h->Print("all");

            //int im = 1, iM = h->GetNbinsX();
            //while (abs(h->GetBinContent(im)) <= feps) { ++im; assert(im < iM); }
            //while (abs(h->GetBinContent(iM)) <= feps) { --iM; assert(im < iM); }

            //double mg = h->GetXaxis()->GetBinLowEdge(im),
            //       Mg = h->GetXaxis()->GetBinLowEdge(iM);

            //ptRanges[iy-1] = {mg, Mg};
        }
    }

    Spectrum (TFile * file) :
        hs(nYbins, nullptr), ptRanges(nYbins)
    {
        assert(file != nullptr);

        for (int iy = 1; iy <= nYbins; ++iy) {

            TH1 * h = dynamic_cast<TH1 *>(file->Get(Form("nominal/ybin%d/h_N", iy)));
            assert(h != nullptr);
            hs[iy-1] = h;
        }
    }

    //template<typename... Args> Spectrum (Args... args) :
    //    Spectrum(GetHists(args...))
    Spectrum (TFile * f, const char * name) : 
        Spectrum(GetHists(f, name))
    {
        cout << __func__ << endl;
    }

    double operator() (double pt, int iy)
    {
        assert(iy > 0 && iy <= nYbins);
        TAxis * x = hs[iy-1]->GetXaxis();
        int ipt = x->FindBin(pt);
        return hs[iy-1]->GetBinContent(ipt, iy);
    }

    double operator() (double pt, double y)
    {
        int iy = abs(y)*2.;
        ++iy; // convention is to start from 1, not from 0
        return operator()(pt, iy);
    }

    void operator/= (const Spectrum& spectrum)
    {
        for (int iy = 1; iy <= nYbins; ++iy) {
            assert(hs[iy-1]->GetNbinsX() == spectrum.hs[iy-1]->GetNbinsX());
            //for (int ipt = 1; ipt <= hs[iy-1]->GetNbinsX(); ++ipt) 
            //    cout << iy << '\t' << hs[iy-1]->GetBinContent(ipt) << '\t' << spectrum.hs[iy-1]->GetBinContent(ipt) << '\n';
            hs[iy-1]->Divide(spectrum.hs[iy-1]);
        }
        //cout << flush;
    }

    void Write (TDirectory * d, const char * name = "")
    {
        d->cd();
        for (int iy = 1; iy <= nYbins; ++iy) {
            TH1 * h = hs[iy-1];
            h->SetDirectory(d);
            const char * hname = Form("%s_ybin%d", name, iy);
            h->Write(hname);
        }
    }
};

//Spectrum operator/ (const Spectrum& num, const Spectrum& den)
//{
//    vector<TH1*> clone(nYbins, nullptr);
//    for (int y = 1; y <= nYbins; ++y) {
//        TH1 * h = num.hs[y-1];
//        const char * name = Form("clone_%s", h->GetName());
//        clone[y-1] = dynamic_cast<TH1*>(h->Clone(name));
//    }
//    Spectrum spectrum(clone);
//    spectrum /= den;
//    return spectrum;
//}

////////////////////////////////////////////////////////////////////////////////
/// Functor to extract the xsect from a fit of the histogram
///
/// TODO: 2D Chebyshev fit
template<int N> struct SmoothSpectrum : public Spectrum {
    
    vector<TF1 *> fs;

    template<typename... Args> SmoothSpectrum (Args... args) :
        Spectrum(args...), fs(nYbins, nullptr)
    {
        for (int iy = 1; iy <= nYbins; ++iy) {

            double m = ptRanges[iy-1].first,
                   M = ptRanges[iy-1].second;
            TF1 * f = GetSmoothFit<N>(hs[iy-1], m, M, true, "Q0SNREI");
            fs[iy-1] = f;
        }
    }

    SmoothSpectrum (TFile * file) :
        Spectrum(file), fs(nYbins, nullptr)
    {
        assert(file != nullptr);

        for (int iy = 1; iy <= nYbins; ++iy) {

            const char * name = Form("nominal/ybin%d/p_fGen", iy);
            cout << name << endl;
            TF1 * f = GetTF1<Thunberg<N>>(file,name);
            assert(f != nullptr);
            f->GetRange(ptRanges[iy-1].first, ptRanges[iy-1].second);
            cout << iy << ' ' << ptRanges[iy-1].first << ' ' << ptRanges[iy-1].second << endl;
            fs[iy-1] = f;
        }
    }

    double operator() (double pt, int iy)
    {
        assert(iy > 0 && iy <= nYbins);
        return fs[iy-1]->Eval(pt);
    }

    void WriteSmooth (TDirectory * d, const char * name = "")
    {
        d->cd();
        for (int iy = 1; iy <= nYbins; ++iy) {
            TF1 * f = fs[iy-1];
            const char * fname = Form("%s_ybin%d", name, iy);
            f->Write(fname);
        }
    }
};
