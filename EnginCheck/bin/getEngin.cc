#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
////////////////////////////////////////////////////////////////////////////////
/// Contains 1D and 2D histograms to describe the jet kinematics.
struct JetPlots {
    
    TH1 * pt, *ptUnf, * y, * eta, * phi, * area;
    TH2 * pt_y, *ptUnf_y, * pt_eta;

    JetPlots (TString name) : //!< Constructor, initialises all histograms
        pt(new TH1D(name + "pt", "pt", nPtBins, pt_edges.data())),
        ptUnf(new TH1D(name + "ptUnf", "ptUnf", genBins.size()-1, genBins.data())),
        y(new TH1D(name + "y" , "y ", nYbins , y_edges.data())),
        eta(new TH1D(name + "eta" , "eta ", nYbins , y_edges.data())),
        phi(new TH1D(name + "phi", "phi", 314, -M_PI, M_PI)),
        area(new TH1D(name + "area", "area", 100, 0, 2)),
        pt_y(new TH2D(name + "pt_y", "pt_y", nPtBins, pt_edges.data(), nYbins, y_edges.data())),
        ptUnf_y(new TH2D(name + "ptUnf_y", "ptUnf_y", genBins.size()-1, genBins.data(), nYbins, y_edges.data())),
        pt_eta(new TH2D(name + "pt_eta", "pt_eta", nPtBins, pt_edges.data(), nYbins, y_edges.data()))
    { }
    void Fill //!< Fill all histograms directly
           (const FourVector& p4, //!< four-momentum of the jet
            float Area,           //!< jet area
            double weight)        //!< entry weight (jet*event)
    {
        pt->Fill(p4.Pt(), weight);
        ptUnf->Fill(p4.Pt(), weight);
        y->Fill(abs(Rapidity(p4)), weight);
        eta->Fill(abs(p4.Eta()), weight);
        phi->Fill(p4.Phi(), weight);
        area->Fill(Area, weight);
        pt_eta->Fill(p4.Pt(), abs(p4.Eta()), weight);
        pt_y->Fill(p4.Pt(), abs(Rapidity(p4)), weight);
        ptUnf_y->Fill(p4.Pt(), abs(Rapidity(p4)), weight);
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Contains 1D and 2D histograms to describe the jet kinematics.
struct EventPlots {

    // from Event
    TH1D * hard_scale;

    // from MET
    TH1D * metEt, * metSumEt, * metFraction, * metPt, * metPhi;

    // from PileUp
    TH1D * puRho, * nVtx, * trpu, * intpu, * pthatMax;

    // from PV
    TH1D * pvRho, * pvZ, * pvChi2ndof;

    EventPlots () : //!< Constructor, initialises all histograms
        hard_scale(new TH1D("hard_scale", "hard_scale", 6500, 0, 6500)), 
        metEt      (new TH1D("metEt", "metEt", 100, 1, 1000)),
        metSumEt   (new TH1D("metSumEt", "metSumEt", 650, 1, 6500)),
        metFraction(new TH1D("metFraction", "metFraction", 100, 0, 1)),
        metPt      (new TH1D("metPt", "metPt", 100, 1, 1000)),
        metPhi     (new TH1D("metPhi", "metPhi", 314, -M_PI, M_PI)),
        puRho      (new TH1D("puRho",   "puRho", 100, 0, 100)),
        nVtx       (new TH1D("nVtx",  "nVtx", 100, 0, 100)),
        trpu       (new TH1D("trpu",  "trpu", 100, 0, 100)),
        intpu      (new TH1D("intpu", "intpu", 100, 0, 100)),
        pthatMax   (new TH1D("pthatMax", "pthatMax", 6500, 0, 6500)),
        pvRho      (new TH1D("pvRho", "pvRho", 150, 0, 1.5)),
        pvZ        (new TH1D("pvZ", "pvZ", 48, -24, 24)),
        pvChi2ndof (new TH1D("pvChi2ndof", "pvChi2ndof", 50, 0, 5))
    {}
    void Fill //!< Fill all histograms directly
        (Event* ev,             //!< event info (run number, etc, but also weight)
         MET* met,              //!< MET info
         PileUp* pu,            //!< PU info
         PrimaryVertex * pv)    //!< PV info
    {
        double w = ev->weights.front();

        // from Event
        hard_scale->Fill(ev->hard_scale, w);

        // from MET
        metEt->Fill(met->Et, w);
        metSumEt->Fill(met->SumEt, w);
        metFraction->Fill(met->Et/met->SumEt, w);
        metPt->Fill(met->Pt, w);
        metPhi->Fill(met->Phi, w);

        // from PileUp
        puRho->Fill(pu->rho, w);
        nVtx ->Fill(pu->nVtx, w);
        trpu ->Fill(pu->trpu, w);
        intpu->Fill(pu->intpu, w);
        pthatMax->Fill(pu->pthatMax, w);

        // from PrimaryVertex
        pvRho     ->Fill(pv->Rho, w);
        pvZ       ->Fill(pv->z, w);
        pvChi2ndof->Fill(pv->chi2/pv->ndof, w);
    }
};
#endif

struct Engin {

};












////////////////////////////////////////////////////////////////////////////////
/// Projects many branches of the *n*-tuple onto histograms (jet and event variables).
void getSpectrum 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    /// Opening source and checking that it is MC
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("ak4/ProcessedTree"));

    TTreeReader reader(tree);
    TTreeReaderValue<Long64_t> ev = {reader, "EvtHdr_.mEvent"};
    TTreeReaderValue<Int_t> run  = {reader, "EvtHdr_.mRun"};
    TTreeReaderValue<Int_t> lumi = {reader, "EvtHdr_.mLumi"};
    TTreeReaderArray<Double_t> px = {reader, "PFJetsCHS_.P4_.fCoordinates.fX"};
    TTreeReaderArray<Double_t> py = {reader, "PFJetsCHS_.P4_.fCoordinates.fY"};
    TTreeReaderArray<Double_t> pz = {reader, "PFJetsCHS_.P4_.fCoordinates.fZ"};
    TTreeReaderArray<Float_t> corr = {reader, "PFJetsCHS_.cor_"};

    TTreeReaderArray<Float_t> NHF = {reader, "PFJetsCHS_.nhf_"};
    TTreeReaderArray<Float_t> NEMF = {reader, "PFJetsCHS_.nemf_"};
    TTreeReaderArray<Int_t> nhm = {reader, "PFJetsCHS_.nhm_"};
    TTreeReaderArray<Int_t> phm = {reader, "PFJetsCHS_.phm_"};
    TTreeReaderArray<Int_t> elm = {reader, "PFJetsCHS_.elm_"};
    TTreeReaderArray<Int_t> mum = {reader, "PFJetsCHS_.mum_"};
    TTreeReaderArray<Int_t> CHM = {reader, "PFJetsCHS_.chm_"};
    TTreeReaderArray<Float_t> CEMF = {reader, "PFJetsCHS_.cemf_"};
    TTreeReaderArray<Float_t> CHF = {reader, "PFJetsCHS_.chf_"};
    TTreeReaderArray<Float_t> MUF = {reader, "PFJetsCHS_.muf_"};

    TFile *file = TFile::Open(output, "RECREATE");

    JetPlots jetplots(""), genjetplots("gen"), bjetplots("b_"), genbjetplots("genb_"), cjetplots("c_"), gencjetplots("genc_"), lightjetplots("light_"), genlightjetplots("genlight_");
    EventPlots eventplots;

    Looper looper(__func__, &reader, nSplit, nNow);
    while (looper.Next()) {

       //cout << *run <<" "<< *ev << endl;
       //cout << px.GetSize() << endl;
       assert(px.GetSize()== corr.GetSize());
       for(unsigned i = 0; i < px.GetSize(); ++i) {
         double pt = hypot(px.At(i),py.At(i)) / corr.At(i);

         if(pt > 2000) {
            double phi = atan2(py.At(i), px.At(i));
            double p = sqrt(pow(px.At(i),2) + pow(py.At(i),2) + pow(pz.At(i),2));
            //double eta = -log(acos(pz.At(i)/p)/2);
            double eta = 0.5*log((p+pz.At(i))/(p-pz.At(i)));

            // tight veto lep ID
            bool tightLepVetoJetID = true;

            auto NumConst = CHM.At(i) + nhm.At(i) + phm.At(i) + elm.At(i) + mum.At(i); //OK
            if(abs(eta)<=2.7)
               tightLepVetoJetID = (NHF.At(i)<0.90 && NEMF.At(i)<0.90 && NumConst>1 && MUF.At(i)<0.8) && ((abs(eta)<=2.4 && CHF.At(i)>0 && CHM.At(i)>0 && CEMF.At(i)<0.90) || abs(eta)>2.4);

            if (!tightLepVetoJetID) continue;

            cout << *run <<" "<<*lumi<<" "<< Int_t(*ev) <<" "<< pt << " " << eta << " "<< phi << endl;
         }
       }
        // jets
           //cout << recjet.p4.Pt() << endl;
       /*
        for (const auto& x: *px) 
           cout << x << endl;
        for (const auto& y: *py) 
           cout << y << endl;
           */

        /*
            double pt = hypot(*px,*py);
            if (pt > 1297)  {
               cout << *run <<" "<<  *ev <<" : "<< pt << endl;
            }
            continue;
            */


    }
    file->Write();
    file->Close();
}









#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 3) {
        cout << "getSpectrum input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getSpectrum(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
