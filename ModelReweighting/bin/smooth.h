#include <cstdlib>
#include <vector>
#include <utility>

#include "Core/CommonTools/interface/variables.h"

#include <TFile.h>
#include <TF1.h>

#include <experimental/filesystem>

using namespace std;
using namespace experimental::filesystem;
using namespace DAS;

struct Smoothing {
    const bool FileExists;
    vector<vector<vector<TH1*>>> hs;

    Smoothing (TString filename, bool verbose = true) :
        FileExists(exists(path(filename.Data())))
    {
        if (!FileExists) assert(filename == "none");
        TFile * file = TFile::Open(filename, "READ");
        for (int N = 1; N <= maxMult; ++N) {
            vector<vector<TH1 *>> vv;
            for (int y = 1; y <= nYbins ; ++y) {
                vector<TH1 *> v;
                vector<pair<float, float>> p;
                for (int n = 1; n <= N; ++n) {
                    TH1 * h = dynamic_cast<TH1*>(file->Get(Form("Nbin%d/ybin%d/nbin%d/weight", N, y, n)));
                    if (h == nullptr) {
                        if (verbose) cerr << "\x1B[31m\e[1mNo smoothing factor found for N = " << N << ", y = " << y << ", n = " << n << "\x1B[30m\e[0m\n";
                        h = new TH1F(Form("%d", rand()), "", 1, 74, 3450);
                        h->SetBinContent(1,1);
                    }
                    v.push_back(h);
                }
                vv.push_back(v);
            }
            hs.push_back(vv);
        }
    }

    float operator()
        (float pt, //!< corrected transverse momentum
         float y, //!< rapidity
         int n, //!< n-th jet
         int N) //!< jet multiplicity
    {
        //assert(n <= N);
        if (n > N) {
            return 1.;
            //cerr << "n = " << n << ", N = " << N << '\n';
            //exit(EXIT_FAILURE);
        }
        if (n > maxMult) return 1.;
        if (N > maxMult) return 1.;
        if (N == 0) return 1.;
        if (n == 0) return 1.;

        if (abs(y) > y_edges.back()) return 1.;
        int ybin = abs(y)*2;
        ybin = min(nYbins-1, ybin);
        //cout << __LINE__ << '\t' << "n = " << n << endl;
		//cout << __LINE__ << '\t' << "y = " << y << endl;
		//cout << __LINE__ << '\t' << "N = " << N << endl;
        //cout << __LINE__ << '\t' << "hs.size() = " << hs.size()                               << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").size() = " << hs.at(N-1).size()                       << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").at(" << ybin << ").size() = " << hs.at(N-1).at(ybin).size()              << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").at(" << ybin << ").at(" << n-1 << ") = " << hs.at(N-1).at(ybin).at(n-1)             << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").at(" << ybin << ").at(" << n-1 << ")->GetTitle() = " << hs.at(N-1).at(ybin).at(n-1)->GetTitle() << endl;
        pt = max(pt, static_cast<float>(74));
        pt = min(pt, static_cast<float>(3450));
        auto h = hs.at(N-1).at(ybin).at(n-1);
        auto result = h->GetBinContent(h->FindBin(pt));
        //cout << pt << ' ' << ybin << ' ' << n << ' ' << N << ' ' << result << endl;
        return result;
    }
};


