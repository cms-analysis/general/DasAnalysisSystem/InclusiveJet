#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>

#include "Math/VectorUtil.h"

#include "Core/JEC/bin/matching.h"

#include "model.h"

template<typename T> void MetaInfo::SetVal (TString name, T value)
{
    TObject * flag = Flags->FindObject(name);
    if (flag == nullptr) {
        cout << "Flag " << name << " has not been found." << endl;
        exit(EXIT_FAILURE);
    }
    auto parameter = dynamic_cast<TParameter<T>*>(flag);
    assert(parameter);
    parameter->SetVal(value);
}

////////////////////////////////////////////////////////////////////////////////
/// applyModelReweighting
///
/// Apply smooth reweighting factor obtained from `getModelReweighting`
/// in each rapidity bin separately
void applyModelReweighting 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              TString model,  //!< name of root file with reweighting function
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input);

    Model m(model, nNow == 0);

    TFile * file = TFile::Open(output, "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("applyModelReweighting");
    bool isMC = metainfo.isMC();
    assert(isMC);
    metainfo.SetVal("isMC", false);
    assert(!metainfo.isMC());

    Event * event = nullptr;
    oldchain->SetBranchAddress("event", &event);
    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);
    oldchain->SetBranchAddress("genJets", &genJets);

    TH2 * genBefore = new TH2D("genBefore", "genBefore", nRecBins, recBins.data(), nYbins, y_edges.data()),
        * recBefore = new TH2D("recBefore", "recBefore", nRecBins, recBins.data(), nYbins, y_edges.data()),
        * genAfter  = new TH2D("genAfter" , "genAfter" , nRecBins, recBins.data(), nYbins, y_edges.data()),
        * recAfter  = new TH2D("recAfter" , "recAfter" , nRecBins, recBins.data(), nYbins, y_edges.data());

    vector<TH3 *> pt_y_ns;
    for (int N = 1; N <= maxMult; ++N) {
        TString name = Form("pt_y_nth_Nbin%d", N); // n-th jet for jet multiplicity N
        TString title = Form("3D inclusive jet distribution with N_{jets} = %d", N);
        TH3 * h = new TH3D(name, title, nPtBins, pt_edges.data(),
                                         nYbins,  y_edges.data(),
                                        maxMult,  n_edges.data());
        pt_y_ns.push_back(h);
    }

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        //int NRec = recJets->size(),
        //    NGen = genJets->size();
        int NRec = count_if(recJets->begin(), recJets->end(), [](RecJet& recJet) { return recJet.CorrPt() >= 74 && abs(recJet.Rapidity()) < 2.5;})/*,
            NGen = count_if(genJets->begin(), genJets->end(), [](GenJet& genJet) { return genJet.p4.Pt() >= 74 && abs(genJet.Rapidity()) < 2.5;})*/;

        auto evWgt = event->genWgts.front() * event->recWgts.front();

        auto get_nRec = [recJets](const RecJet& recjet) {
            auto thispt = recjet.CorrPt(0)/*,
                 thisy = abs(recjet.Rapidity())*/;
            int nRec = 0;
            for (auto thatRecJet: *recJets) {
                auto thatpt = thatRecJet.CorrPt(0),
                     thaty = abs(thatRecJet.Rapidity());
                if (thatpt >= 74 && thaty < 2.5) ++nRec;
                if (abs(thispt - thatpt) < feps)
                    return nRec;
            }
            cerr << "Rec jet was not found!\n";
            exit(EXIT_FAILURE);
        };

        Matching<RecJet, GenJet> matching(*recJets);
        for (size_t iGen = 0; iGen < genJets->size(); ++iGen) {
            GenJet& genJet = genJets->at(iGen);
            auto gpt = genJet.p4.Pt(),
                 gy  = abs(genJet.Rapidity());

            const RecJet& recJet = matching.HighestPt(genJet);
            bool noMatch = recJet.p4.Pt() > 13e3 /* 13 TeV */;
            if (noMatch) {
                //auto nGen = iGen + 1;
                auto jWgt = 1.; //m(gpt, gy, nGen, NGen); // TODO?
                genBefore->Fill(gpt, gy, evWgt       );
                genAfter ->Fill(gpt, gy, evWgt * jWgt);
            }
            else {
                auto rpt = recJet.CorrPt(0),
                     ry  = abs(recJet.Rapidity()),
                     rjWgt = recJet.weights.front(); // only rec jet weight
                auto nRec = get_nRec(recJet);
                auto gjWgt = m(rpt, ry, nRec, NRec);
                genBefore->Fill(gpt, gy, evWgt                );
                recBefore->Fill(rpt, ry, evWgt         * rjWgt);
                genAfter ->Fill(gpt, gy, evWgt * gjWgt        );
                recAfter ->Fill(rpt, ry, evWgt * gjWgt * rjWgt);

                for (auto& w: genJet.weights) w *= gjWgt;

                if (NRec > maxMult || NRec == 0) continue;
                pt_y_ns.at(NRec-1)->Fill(recJet.CorrPt(), recJet.AbsRap(), nRec, evWgt * gjWgt * rjWgt);
            }
        }

        for (RecJet& recJet: matching.mcands) {
            auto rpt = recJet.CorrPt(0),
                 ry  = recJet.AbsRap(),
                 rjWgt = recJet.weights.front(); // only rec jet weight
            auto nRec = get_nRec(recJet);
            auto gjWgt = m(rpt, ry, nRec, NRec);
            recBefore->Fill(rpt, ry, evWgt         * rjWgt);
            recAfter ->Fill(rpt, ry, evWgt * gjWgt * rjWgt);

            for (auto& w: recJet.weights) w *= gjWgt;

            if (NRec > maxMult || NRec == 0) continue;
            pt_y_ns.at(NRec-1)->Fill(recJet.CorrPt(), recJet.AbsRap(), nRec, evWgt * gjWgt * rjWgt);
        }

        newtree->Fill();
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output model [nSplit [nNow]]\n"
             << "\twhere\tinput = input MC n-tuple to reweight\n"
             << "\t     \toutput = reweighted MC n-tuple\n"
             << "\t     \tmodel = root file contaning smooth fits to data (obtained from `getModelReweighting`)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2],
            model = argv[3];
    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    applyModelReweighting(input, output, model, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
