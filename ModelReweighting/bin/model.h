#include <cstdlib>
#include <vector>
#include <utility>

#include "Core/CommonTools/interface/variables.h"

#include <TFile.h>
#include <TF1.h>

#include <experimental/filesystem>

using namespace std;
using namespace experimental::filesystem;
using namespace DAS;

struct Model {
    const bool FileExists;
    vector<vector<vector<TF1*>>> fs;
    vector<vector<vector<pair<float, float>>>> intervals;

    Model (TString filename, bool verbose = true) :
        FileExists(exists(path(filename.Data())))
    {
        if (!FileExists) {
            assert(filename == "none"); 
            return;
        }
        TFile * file = TFile::Open(filename, "READ");
        for (int N = 1; N <= maxMult; ++N) {
            vector<vector<TF1 *>> vv;
            vector<vector<pair<float, float>>> pp;
            for (int y = 1; y <= nYbins ; ++y) {
                vector<TF1 *> v;
                vector<pair<float, float>> p;
                for (int n = 1; n <= N; ++n) {
                    TF1 * f = dynamic_cast<TF1*>(file->Get(Form("Nbin%d/ybin%d/nbin%d/f", N, y, n)));
                    if (f == nullptr) {
                        if (verbose) cerr << "\x1B[31m\e[1mNo reweighting function found for N = " << N << ", y = " << y << ", n = " << n << "\x1B[30m\e[0m\n";
                        f = new TF1(Form("%d", rand()), "1", 74, 3450);
                    }

                    double minpt, maxpt;
                    f->GetRange(minpt, maxpt);

                    v.push_back(f);
                    p.push_back( { minpt, maxpt } );
                }
                vv.push_back(v);
                pp.push_back(p);
            }
            fs.push_back(vv);
            intervals.push_back(pp);
        }
    }

    float operator()
        (float pt, //!< corrected transverse momentum
         float y, //!< rapidity
         int n, //!< n-th jet
         int N) //!< jet multiplicity
    {
        if (!FileExists) return 1.;

        //assert(n <= N);
        if (n > N) {
            return 1.;
            //cerr << "n = " << n << ", N = " << N << '\n';
            //exit(EXIT_FAILURE);
        }
        if (n > maxMult) return 1.;
        if (N > maxMult) return 1.;
        if (N == 0) return 1.;
        if (n == 0) return 1.;

        if (abs(y) > y_edges.back()) return 1.;
        int ybin = abs(y)*2;
        ybin = min(nYbins-1, ybin);
        //cout << __LINE__ << '\t' << "n = " << n << endl;
		//cout << __LINE__ << '\t' << "y = " << y << endl;
		//cout << __LINE__ << '\t' << "N = " << N << endl;
        //cout << __LINE__ << '\t' << "hs.size() = " << hs.size()                               << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").size() = " << hs.at(N-1).size()                       << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").at(" << ybin << ").size() = " << hs.at(N-1).at(ybin).size()              << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").at(" << ybin << ").at(" << n-1 << ") = " << hs.at(N-1).at(ybin).at(n-1)             << endl;
		//cout << __LINE__ << '\t' << "hs.at(" << N-1 << ").at(" << ybin << ").at(" << n-1 << ")->GetTitle() = " << hs.at(N-1).at(ybin).at(n-1)->GetTitle() << endl;
        auto minpt = intervals.at(N-1).at(ybin).at(n-1).first,
             maxpt = intervals.at(N-1).at(ybin).at(n-1).second;
        pt = max(pt, minpt + feps);
        pt = min(pt, maxpt - feps);
        auto result = fs.at(N-1).at(ybin).at(n-1)->Eval(pt);
        //cout << pt << ' ' << ybin << ' ' << n << ' ' << N << ' ' << result << endl;
        return result;
    }
};

