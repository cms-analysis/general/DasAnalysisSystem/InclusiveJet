#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/Greta.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/terminal.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// getModelReweighting
///
/// Obtain reweighting function from Greta fit of ratio of jet spectrum
/// in Data and MC as a function of pt, y, and cumulative N.
void getModelReweighting 
             (TString inputMC,    //!< name of simulation input root file 
              TString inputData,  //!< name of data input root file 
              TString output)     //!< name of output root file
{
    TFile * sourceMC   = TFile::Open(inputMC  , "READ");
    TFile * sourceData = TFile::Open(inputData, "READ");
    TFile * file       = TFile::Open(output   , "RECREATE");

    for (int N = 1; N <= maxMult; ++N) { // N = jet mutiplicity
        TString hname = Form("pt_y_nth_Nbin%d", N);
        cout << hname << endl;

        sourceMC->cd();
        TH3 * hMC3   = dynamic_cast<TH3*>(sourceMC->Get(hname));
        assert(hMC3 != nullptr);
        hMC3->SetDirectory(0);

        sourceData->cd();
        TH3 * hData3 = dynamic_cast<TH3*>(sourceData->Get(hname));
        assert(hData3 != nullptr);
        hData3->SetDirectory(0);

        hData3->Divide(hMC3);

        file->cd();
        TDirectory * d = file->mkdir(Form("Nbin%d", N));
        d->cd();
        hData3->SetDirectory(d);
        hData3->Write("h3");

        for (int y = 1; y <= nYbins; ++y) {

            TDirectory * dd = d->mkdir(Form("ybin%d", y));

            for (int n = 1; n <= N; ++n) { // n-th jet
                dd->cd();
                TDirectory * ddd = dd->mkdir(Form("nbin%d", n));
                ddd->cd();

                TH1 * h = hData3->ProjectionX("h", y, y, n, n);
                h->SetDirectory(ddd);
                h->SetTitle("Data/MC");
                h->Write("h");

                //static const vector<float> maxpts { 3103, 2787, 2500, 1784, 1101};
                int minpt = h->FindBin(75),
                    maxpt = h->FindBin(3400 /*maxpts.at(y-1)*/);
                const int degree = 4;
                while (h->GetBinContent(minpt) == 0 && minpt + degree + 1 < maxpt) ++minpt;
                while (h->GetBinContent(maxpt) == 0 && minpt + degree + 1 < maxpt) --maxpt;
                auto f = GetSmoothFit<degree>(h, minpt, maxpt, 2, "Q0SNRE", false);

                // output
                if (f == nullptr) cout << red << "Failed: ";
                else              cout << green << "Success: ";
                cout << N << ' ' << y << ' ' << n << '\n' << normal << endl;

                if (f == nullptr) continue;
                TString fname = Form("pt_ybin%d_nth%d_Nbin%d", y, n, N);
                f->SetTitle(fname);
                f->Write("f");
            }
        }
    }
    sourceMC->Close();
    sourceData->Close();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output\n"
             << "where\tinputMC = simulation root file from `getSpectrum`\n" 
             << "     \tinputData = data root file from `getSpectrum`\n" 
             << "     \toutput = root file with reweighting function in each rapidity bin (to be applied on MC)" << endl;
        return EXIT_SUCCESS;
    }

    TString inputMC = argv[1],
            inputData = argv[2],
            output = argv[3];

    getModelReweighting(inputMC, inputData, output);
    return EXIT_SUCCESS;
}
#endif

