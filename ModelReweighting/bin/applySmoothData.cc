#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include "smooth.h"

using namespace std;
using namespace DAS;

static const auto feps = numeric_limits<float>::epsilon();

////////////////////////////////////////////////////////////////////////////////
/// Template for function
[[ deprecated ]]
void applySmoothData 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              TString weights,  //!< name of input root file with weights (from `getSmoothData`)
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input);

    Smoothing m(weights, nNow == 0);

    TFile * file = TFile::Open(output, "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("smoothing");
    bool isMC = metainfo.isMC();
    assert(!isMC);

    Event * event = nullptr;
    oldchain->SetBranchAddress("event", &event);
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);

    TH2 * recBefore = new TH2D("recBefore", "recBefore", nRecBins, recBins.data(), nYbins, y_edges.data()),
        * recAfter  = new TH2D("recAfter" , "recAfter" , nRecBins, recBins.data(), nYbins, y_edges.data());

    vector<TH3 *> pt_y_ns;
    for (int N = 1; N <= maxMult; ++N) {
        TString name = Form("pt_y_nth_Nbin%d", N); // n-th jet for jet multiplicity N
        TString title = Form("3D inclusive jet distribution with N_{jets} = %d", N);
        TH3 * h = new TH3D(name, title, nPtBins, pt_edges.data(),
                                         nYbins,  y_edges.data(),
                                        maxMult,  n_edges.data());
        pt_y_ns.push_back(h);
    }

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        int NRec = count_if(recJets->begin(), recJets->end(), [](RecJet& recJet) { return recJet.CorrPt() >= 74 && abs(recJet.Rapidity()) < 2.5;});

        auto evWgt = event->weights.front();

        auto get_nRec = [recJets](const RecJet& recjet) {
            auto thispt = recjet.CorrPt(0)/*,
                 thisy = abs(recjet.Rapidity())*/;
            int nRec = 0;
            for (auto thatRecJet: *recJets) {
                auto thatpt = thatRecJet.CorrPt(0),
                     thaty = abs(thatRecJet.Rapidity());
                if (thatpt >= 74 && thaty < 2.5) ++nRec;
                if (abs(thispt - thatpt) < feps)
                    return nRec;
            }
            cerr << "Rec jet was not found!\n";
            exit(EXIT_FAILURE);
        };

        for (RecJet& recJet: *recJets) {
            auto rpt = recJet.CorrPt(0),
                 ry  = abs(recJet.Rapidity()),
                 rjWgt = recJet.weights.front(); // only rec jet weight
            auto nRec = get_nRec(recJet);
            auto jWgt = m(rpt, ry, nRec, NRec);
            recBefore->Fill(rpt, ry, evWgt * rjWgt       );
            recAfter ->Fill(rpt, ry, evWgt * rjWgt * jWgt);
            if (NRec > maxMult || NRec == 0) continue;
            pt_y_ns.at(NRec-1)->Fill(recJet.CorrPt(), abs(recJet.Rapidity()), nRec, evWgt * jWgt * rjWgt);

            for (auto& w: recJet.weights) w *= jWgt;
        }

        newtree->Fill();
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output weights [nSplit [nNow]]\n"
             << "\twhere\tinput = normalised data n-tuple\n"
             << "\t     \toutput = modified n-tuple\n"
             << "\t     \tweights = output from `getSmoothData`\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2],
            weights = argv[3];
    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    applySmoothData(input, output, weights, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
